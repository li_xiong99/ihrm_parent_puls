package com.kun.ihrm.employee.client;

import com.kun.ihrm.common.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 声明接口,通过feign调用其他微服务
 * @author: hyl
 * @date: 2020/02/11
 **/
@FeignClient(name = "ihrm-system")
public interface SystemFeignClient {

    /**
     * 调用微服务的接口
     */

    @RequestMapping(value = "/sys/user/{id}",method = RequestMethod.GET)
    public Result findByUserId(@PathVariable("id") String id);

}

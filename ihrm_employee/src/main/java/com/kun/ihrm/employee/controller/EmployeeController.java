package com.kun.ihrm.employee.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kun.ihrm.common.controller.BaseController;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import com.kun.ihrm.employee.client.SystemFeignClient;
import com.kun.ihrm.employee.entity.*;
import com.kun.ihrm.employee.service.*;
import com.kun.ihrm.model.employee.EmployeeArchive;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/employees")
public class EmployeeController extends BaseController {
    //存档
    @Resource
    private AtteArchiveMonthlyService atteArchiveMonthlyService;
    //存档详情
    @Resource
    private AtteArchiveMonthlyInfoService atteArchiveMonthlyInfoService;
    //岗位
    @Resource
    private EmUserCompanyJobsService emuserCompanyJobsService;
    //转正信息
    @Resource
    private EmPositiveService emPositiveService;
    //辞职
    @Resource
    private EmResignationService emResignationService;
    //员工详情
    @Resource
    private EmUserCompanyPersonalService emuserCompanyPersonalService;
//    //用户
//    @Resource
//    private BsUserService bsUserService;
    @Resource
    private SystemFeignClient systemFeignClient;

    @Resource
    private  EmArchiveService archiveService;



    /**
     * 完成员工历史归档详情
     *
     * GET /employees/archives/
     * 接口ID：104783477
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104783477
     */
    @RequestMapping(value = "/archives", method = RequestMethod.GET)
    public Result findArchives( @RequestParam(defaultValue = "1") Integer page,
                                @RequestParam(defaultValue = "10") Integer pagesize,
                               String year) {
        PageResult searchPage = atteArchiveMonthlyService.getbyPageList(page, pagesize,year);
        return new Result(ResultCode.SUCCESS,searchPage);
    }
/**
 * 员工个人详情更新
 * employees/1063705989926227968/personalInfo
 * PUT
 */
    @PutMapping("/{id}/personalInfo")
    public Result updateToPersonalInfo(@PathVariable("id") String id,
                                       @RequestBody EmUserCompanyPersonal personal){
        boolean update = emuserCompanyPersonalService.updateById(personal);
        if(update){
            return new Result(ResultCode.SUCCESS,update);
        }
        return new Result(ResultCode.FAIL);
    }
    /**
     * 完成员工岗位详情
     *
     * GET /employees/{id}/jobs
     * 接口ID：105078920
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105078920
     */
    @GetMapping("/{id}/jobs")
    public Result JobsAll(@PathVariable("id") String id) {
        EmUserCompanyJobs emuserCompanyJobsId = emuserCompanyJobsService.getById(id);
        return new Result(ResultCode.SUCCESS, emuserCompanyJobsId);
    }

    /**
     * 历史档案修改完成
     * employees/1063705989926227968/jobs
     * PUT
     * @param id
     * @return
     */
    @RequestMapping(value = "{id}/jobs",method = RequestMethod.PUT)
    public Result updateToJobs(@PathVariable("id") String id,@RequestBody EmUserCompanyJobs userCompanyJobs){
        boolean rows = emuserCompanyJobsService.updateById(userCompanyJobs);
        if(rows){
            return new Result(ResultCode.SUCCESS,rows);
        }
        return new Result(ResultCode.FAIL);
    }

    /**
     * 完成个人详情
     *
     */
    @GetMapping("/{id}/personalInfo")
    public Result personalInfoId(@PathVariable("id") String id) {
        EmUserCompanyPersonal serviceById = emuserCompanyPersonalService.selectByInfo(id);
        return new Result(ResultCode.SUCCESS, serviceById);
    }

    /**
     * 月份报表 详情
     *
     * @param month
     * @param type
     * @return
     */
//    TODO 月份报表 详情
    @GetMapping("/archives/{yearAndMonth}")
    public Result findAllPage(
            @PathVariable("yearAndMonth") String yearAndMonth,
            @RequestParam(defaultValue = "1") String type,
            @RequestParam(defaultValue = "201908") String month
    ) {

        return new Result(ResultCode.SUCCESS, "");
    }

    /**
     * 完成转正表单详情
     *
     * GET /employees/{id}/positive
     * 接口ID：104903662
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104903662
     */
    @GetMapping("/{id}/positive")
    public Result positiveAll(@PathVariable("id") String id) {
        EmPositive positive = emPositiveService.getById(id);
        if(positive==null){
            positive=new EmPositive();
            positive.setUserId(id);
            positive.setCreateTime(new Date());
            positive.setEstatus(1);
        }
        return new Result(ResultCode.SUCCESS, positive);
    }
    /**
     * 表单保存
     * PUT /employees/{id}/positive
     * 接口ID：104903850
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104903850
     */
    @RequestMapping(value ={ "/{id}/positive"},method = RequestMethod.PUT)
    public Result addOrUpdate(@RequestBody EmPositive emPositive,@PathVariable("id") String userId){
        emPositive.setUserId(userId);
        emPositiveService.saveOrUpdate(emPositive);
        return new Result(ResultCode.SUCCESS);
    }

    /**
     * 完成离职表单读取
     *
     * GET /employees/{id}/leave
     * 接口ID：104903971
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104903971
     */

    @GetMapping("/{id}/leave")
    public Result findLeave(@PathVariable("id") String id) {
        EmResignation byId = emResignationService.getById(id);
        if (byId != null) {
            return new Result(ResultCode.SUCCESS, byId);
        }
        return new Result(ResultCode.SUCCESS, "离职失败");
    }

    /**
     * 离职表单保存
     * PUT /employees/{id}/leave
     * 接口ID：104963558
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104963558
     *employees/1063705989926227968/leave
     * @return
     */
    @RequestMapping(value = "/{id}/leave",method = RequestMethod.PUT)
    public Result updateToLeave(@PathVariable("id") String id,@RequestBody EmResignation emResignation){
        boolean rows = emResignationService.updateById(emResignation);
        if(rows){
            return new Result(ResultCode.SUCCESS,rows);
        }
        return new Result(ResultCode.FAIL);
    }
}

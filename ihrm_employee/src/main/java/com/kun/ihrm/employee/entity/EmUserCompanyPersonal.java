package com.kun.ihrm.employee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * EmuserCompanyPersonal实体类
 * @packageName .main.java
 * @fileName EmuserCompanyPersonal.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */

@TableName("em_user_company_personal")
public class EmUserCompanyPersonal {

  /** 
   * 用户ID
   */
  @TableId(value = "user_id", type = IdType.AUTO)
  private String userId;

  @TableField("username")
  private String username;

  @TableField("mobile")
  private String mobile;

  @TableField("time_of_entry")
  private String timeOfEntry;

  @TableField("department_name")
  private String departmentName;
  /** 
   * 公司ID
   */
  @TableField("company_id")
  private String companyId;
  /** 
   * 性别
   */
  @TableField("sex")
  private String sex;
  /** 
   * 出生日期
   */
  @TableField("date_of_birth")
  private String dateOfBirth;
  /** 
   * 最高学历
   */
  @TableField("the_highest_degree_of_education")
  private String theHighestDegreeOfEducation;
  /** 
   * 国家地区
   */
  @TableField("national_area")
  private String nationalArea;
  /** 
   * 护照号
   */
  @TableField("passport_no")
  private String passportNo;
  /** 
   * 身份证号
   */
  @TableField("id_number")
  private String idNumber;
  /** 
   * 身份证照片-正面
   */
  @TableField("id_card_photo_positive")
  private String idCardPhotoPositive;
  /** 
   * 身份证照片-背面
   */
  @TableField("id_card_photo_back")
  private String idCardPhotoBack;
  /** 
   * 籍贯
   */
  @TableField("native_place")
  private String nativePlace;
  /** 
   * 民族
   */
  @TableField("nation")
  private String nation;
  /** 
   * 英文名
   */
  @TableField("english_name")
  private String englishName;
  /** 
   * 婚姻状况
   */
  @TableField("marital_status")
  private String maritalStatus;
  /** 
   * 员工照片
   */
  @TableField("staff_photo")
  private String staffPhoto;
  /** 
   * 生日
   */
  @TableField("birthday")
  private String birthday;
  /** 
   * 属相
   */
  @TableField("zodiac")
  private String zodiac;
  /** 
   * 年龄
   */
  @TableField("age")
  private String age;
  /** 
   * 星座
   */
  @TableField("constellation")
  private String constellation;
  /** 
   * 血型
   */
  @TableField("blood_type")
  private String bloodType;
  /** 
   * 户籍所在地
   */
  @TableField("domicile")
  private String domicile;
  /** 
   * 政治面貌
   */
  @TableField("political_outlook")
  private String politicalOutlook;
  /** 
   * 入党时间
   */
  @TableField("time_to_join_the_party")
  private String timeToJoinTheParty;
  /** 
   * 存档机构
   */
  @TableField("archiving_organization")
  private String archivingOrganization;
  /** 
   * 子女状态
   */
  @TableField("state_of_children")
  private String stateOfChildren;
  /** 
   * 子女有无商业保险
   */
  @TableField("do_children_have_commercial_insurance")
  private String doChildrenHaveCommercialInsurance;
  /** 
   * 有无违法违纪行为
   */
  @TableField("is_there_any_violation_of_law_or_discipline")
  private String isThereAnyViolationOfLawOrDiscipline;
  /** 
   * 有无重大病史
   */
  @TableField("are_there_any_major_medical_histories")
  private String areThereAnyMajorMedicalHistories;
  /** 
   * QQ
   */
  @TableField("qq")
  private String qq;
  /** 
   * 微信
   */
  @TableField("wechat")
  private String wechat;
  /** 
   * 居住证城市
   */
  @TableField("residence_card_city")
  private String residenceCardCity;
  /** 
   * 居住证办理日期
   */
  @TableField("date_of_residence_permit")
  private String dateOfResidencePermit;
  /** 
   * 居住证截止日期
   */
  @TableField("residence_permit_deadline")
  private String residencePermitDeadline;
  /** 
   * 现居住地
   */
  @TableField("place_of_residence")
  private String placeOfResidence;
  /** 
   * 通讯地址
   */
  @TableField("postal_address")
  private String postalAddress;

  @TableField("contact_the_mobile_phone")
  private String contactTheMobilePhone;

  @TableField("personal_mailbox")
  private String personalMailbox;
  /** 
   * 紧急联系人
   */
  @TableField("emergency_contact")
  private String emergencyContact;
  /** 
   * 紧急联系电话
   */
  @TableField("emergency_contact_number")
  private String emergencyContactNumber;
  /** 
   * 社保电脑号
   */
  @TableField("social_security_computer_number")
  private String socialSecurityComputerNumber;
  /** 
   * 公积金账号
   */
  @TableField("provident_fund_account")
  private String providentFundAccount;
  /** 
   * 银行卡号
   */
  @TableField("bank_card_number")
  private String bankCardNumber;
  /** 
   * 开户行
   */
  @TableField("opening_bank")
  private String openingBank;
  /** 
   * 学历类型
   */
  @TableField("educational_type")
  private String educationalType;
  /** 
   * 毕业学校
   */
  @TableField("graduate_school")
  private String graduateSchool;
  /** 
   * 入学时间
   */
  @TableField("enrolment_time")
  private String enrolmentTime;
  /** 
   * 毕业时间
   */
  @TableField("graduation_time")
  private String graduationTime;
  /** 
   * 专业
   */
  @TableField("major")
  private String major;
  /** 
   * 毕业证书
   */
  @TableField("graduation_certificate")
  private String graduationCertificate;
  /** 
   * 学位证书
   */
  @TableField("certificate_of_academic_degree")
  private String certificateOfAcademicDegree;
  /** 
   * 上家公司
   */
  @TableField("home_company")
  private String homeCompany;
  /** 
   * 职称
   */
  @TableField("title")
  private String title;
  /** 
   * 简历
   */
  @TableField("resume")
  private String resume;
  /** 
   * 有无竞业限制
   */
  @TableField("is_there_any_competition_restriction")
  private String isThereAnyCompetitionRestriction;
  /** 
   * 前公司离职证明
   */
  @TableField("proof_of_departure_of_former_company")
  private String proofOfDepartureOfFormerCompany;
  /** 
   * 备注
   */
  @TableField("remarks")
  private String remarks;
  /**
   * 员工号
   */
  @TableField(exist = false)
  private String workNumber;

  /**
   * 员工密码
   */
  @TableField(exist = false)
  private String password;

  public EmUserCompanyPersonal() {
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getTimeOfEntry() {
    return timeOfEntry;
  }

  public void setTimeOfEntry(String timeOfEntry) {
    this.timeOfEntry = timeOfEntry;
  }

  public String getDepartmentName() {
    return departmentName;
  }

  public void setDepartmentName(String departmentName) {
    this.departmentName = departmentName;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getTheHighestDegreeOfEducation() {
    return theHighestDegreeOfEducation;
  }

  public void setTheHighestDegreeOfEducation(String theHighestDegreeOfEducation) {
    this.theHighestDegreeOfEducation = theHighestDegreeOfEducation;
  }

  public String getNationalArea() {
    return nationalArea;
  }

  public void setNationalArea(String nationalArea) {
    this.nationalArea = nationalArea;
  }

  public String getPassportNo() {
    return passportNo;
  }

  public void setPassportNo(String passportNo) {
    this.passportNo = passportNo;
  }

  public String getIdNumber() {
    return idNumber;
  }

  public void setIdNumber(String idNumber) {
    this.idNumber = idNumber;
  }

  public String getIdCardPhotoPositive() {
    return idCardPhotoPositive;
  }

  public void setIdCardPhotoPositive(String idCardPhotoPositive) {
    this.idCardPhotoPositive = idCardPhotoPositive;
  }

  public String getIdCardPhotoBack() {
    return idCardPhotoBack;
  }

  public void setIdCardPhotoBack(String idCardPhotoBack) {
    this.idCardPhotoBack = idCardPhotoBack;
  }

  public String getNativePlace() {
    return nativePlace;
  }

  public void setNativePlace(String nativePlace) {
    this.nativePlace = nativePlace;
  }

  public String getNation() {
    return nation;
  }

  public void setNation(String nation) {
    this.nation = nation;
  }

  public String getEnglishName() {
    return englishName;
  }

  public void setEnglishName(String englishName) {
    this.englishName = englishName;
  }

  public String getMaritalStatus() {
    return maritalStatus;
  }

  public void setMaritalStatus(String maritalStatus) {
    this.maritalStatus = maritalStatus;
  }

  public String getStaffPhoto() {
    return staffPhoto;
  }

  public void setStaffPhoto(String staffPhoto) {
    this.staffPhoto = staffPhoto;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }

  public String getZodiac() {
    return zodiac;
  }

  public void setZodiac(String zodiac) {
    this.zodiac = zodiac;
  }

  public String getAge() {
    return age;
  }

  public void setAge(String age) {
    this.age = age;
  }

  public String getConstellation() {
    return constellation;
  }

  public void setConstellation(String constellation) {
    this.constellation = constellation;
  }

  public String getBloodType() {
    return bloodType;
  }

  public void setBloodType(String bloodType) {
    this.bloodType = bloodType;
  }

  public String getDomicile() {
    return domicile;
  }

  public void setDomicile(String domicile) {
    this.domicile = domicile;
  }

  public String getPoliticalOutlook() {
    return politicalOutlook;
  }

  public void setPoliticalOutlook(String politicalOutlook) {
    this.politicalOutlook = politicalOutlook;
  }

  public String getTimeToJoinTheParty() {
    return timeToJoinTheParty;
  }

  public void setTimeToJoinTheParty(String timeToJoinTheParty) {
    this.timeToJoinTheParty = timeToJoinTheParty;
  }

  public String getArchivingOrganization() {
    return archivingOrganization;
  }

  public void setArchivingOrganization(String archivingOrganization) {
    this.archivingOrganization = archivingOrganization;
  }

  public String getStateOfChildren() {
    return stateOfChildren;
  }

  public void setStateOfChildren(String stateOfChildren) {
    this.stateOfChildren = stateOfChildren;
  }

  public String getDoChildrenHaveCommercialInsurance() {
    return doChildrenHaveCommercialInsurance;
  }

  public void setDoChildrenHaveCommercialInsurance(String doChildrenHaveCommercialInsurance) {
    this.doChildrenHaveCommercialInsurance = doChildrenHaveCommercialInsurance;
  }

  public String getIsThereAnyViolationOfLawOrDiscipline() {
    return isThereAnyViolationOfLawOrDiscipline;
  }

  public void setIsThereAnyViolationOfLawOrDiscipline(String isThereAnyViolationOfLawOrDiscipline) {
    this.isThereAnyViolationOfLawOrDiscipline = isThereAnyViolationOfLawOrDiscipline;
  }

  public String getAreThereAnyMajorMedicalHistories() {
    return areThereAnyMajorMedicalHistories;
  }

  public void setAreThereAnyMajorMedicalHistories(String areThereAnyMajorMedicalHistories) {
    this.areThereAnyMajorMedicalHistories = areThereAnyMajorMedicalHistories;
  }

  public String getQq() {
    return qq;
  }

  public void setQq(String qq) {
    this.qq = qq;
  }

  public String getWechat() {
    return wechat;
  }

  public void setWechat(String wechat) {
    this.wechat = wechat;
  }

  public String getResidenceCardCity() {
    return residenceCardCity;
  }

  public void setResidenceCardCity(String residenceCardCity) {
    this.residenceCardCity = residenceCardCity;
  }

  public String getDateOfResidencePermit() {
    return dateOfResidencePermit;
  }

  public void setDateOfResidencePermit(String dateOfResidencePermit) {
    this.dateOfResidencePermit = dateOfResidencePermit;
  }

  public String getResidencePermitDeadline() {
    return residencePermitDeadline;
  }

  public void setResidencePermitDeadline(String residencePermitDeadline) {
    this.residencePermitDeadline = residencePermitDeadline;
  }

  public String getPlaceOfResidence() {
    return placeOfResidence;
  }

  public void setPlaceOfResidence(String placeOfResidence) {
    this.placeOfResidence = placeOfResidence;
  }

  public String getPostalAddress() {
    return postalAddress;
  }

  public void setPostalAddress(String postalAddress) {
    this.postalAddress = postalAddress;
  }

  public String getContactTheMobilePhone() {
    return contactTheMobilePhone;
  }

  public void setContactTheMobilePhone(String contactTheMobilePhone) {
    this.contactTheMobilePhone = contactTheMobilePhone;
  }

  public String getPersonalMailbox() {
    return personalMailbox;
  }

  public void setPersonalMailbox(String personalMailbox) {
    this.personalMailbox = personalMailbox;
  }

  public String getEmergencyContact() {
    return emergencyContact;
  }

  public void setEmergencyContact(String emergencyContact) {
    this.emergencyContact = emergencyContact;
  }

  public String getEmergencyContactNumber() {
    return emergencyContactNumber;
  }

  public void setEmergencyContactNumber(String emergencyContactNumber) {
    this.emergencyContactNumber = emergencyContactNumber;
  }

  public String getSocialSecurityComputerNumber() {
    return socialSecurityComputerNumber;
  }

  public void setSocialSecurityComputerNumber(String socialSecurityComputerNumber) {
    this.socialSecurityComputerNumber = socialSecurityComputerNumber;
  }

  public String getProvidentFundAccount() {
    return providentFundAccount;
  }

  public void setProvidentFundAccount(String providentFundAccount) {
    this.providentFundAccount = providentFundAccount;
  }

  public String getBankCardNumber() {
    return bankCardNumber;
  }

  public void setBankCardNumber(String bankCardNumber) {
    this.bankCardNumber = bankCardNumber;
  }

  public String getOpeningBank() {
    return openingBank;
  }

  public void setOpeningBank(String openingBank) {
    this.openingBank = openingBank;
  }

  public String getEducationalType() {
    return educationalType;
  }

  public void setEducationalType(String educationalType) {
    this.educationalType = educationalType;
  }

  public String getGraduateSchool() {
    return graduateSchool;
  }

  public void setGraduateSchool(String graduateSchool) {
    this.graduateSchool = graduateSchool;
  }

  public String getEnrolmentTime() {
    return enrolmentTime;
  }

  public void setEnrolmentTime(String enrolmentTime) {
    this.enrolmentTime = enrolmentTime;
  }

  public String getGraduationTime() {
    return graduationTime;
  }

  public void setGraduationTime(String graduationTime) {
    this.graduationTime = graduationTime;
  }

  public String getMajor() {
    return major;
  }

  public void setMajor(String major) {
    this.major = major;
  }

  public String getGraduationCertificate() {
    return graduationCertificate;
  }

  public void setGraduationCertificate(String graduationCertificate) {
    this.graduationCertificate = graduationCertificate;
  }

  public String getCertificateOfAcademicDegree() {
    return certificateOfAcademicDegree;
  }

  public void setCertificateOfAcademicDegree(String certificateOfAcademicDegree) {
    this.certificateOfAcademicDegree = certificateOfAcademicDegree;
  }

  public String getHomeCompany() {
    return homeCompany;
  }

  public void setHomeCompany(String homeCompany) {
    this.homeCompany = homeCompany;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getResume() {
    return resume;
  }

  public void setResume(String resume) {
    this.resume = resume;
  }

  public String getIsThereAnyCompetitionRestriction() {
    return isThereAnyCompetitionRestriction;
  }

  public void setIsThereAnyCompetitionRestriction(String isThereAnyCompetitionRestriction) {
    this.isThereAnyCompetitionRestriction = isThereAnyCompetitionRestriction;
  }

  public String getProofOfDepartureOfFormerCompany() {
    return proofOfDepartureOfFormerCompany;
  }

  public void setProofOfDepartureOfFormerCompany(String proofOfDepartureOfFormerCompany) {
    this.proofOfDepartureOfFormerCompany = proofOfDepartureOfFormerCompany;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public String getWorkNumber() {
    return workNumber;
  }

  public void setWorkNumber(String workNumber) {
    this.workNumber = workNumber;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}

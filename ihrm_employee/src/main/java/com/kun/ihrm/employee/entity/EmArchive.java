package com.kun.ihrm.employee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;


/**
 * EmArchive实体类
 * @packageName .main.java
 * @fileName EmArchive.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */

@TableName("em_archive")
public class EmArchive {

  /** 
   * ID
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 操作用户
   */
  @TableField("op_user")
  private String opuser;
  /** 
   * 月份
   */
  @TableField("month")
  private String month;
  /** 
   * 企业ID
   */
  @TableField("company_id")
  private String companyId;
  /** 
   * 总人数
   */
  @TableField("totals")
  private Integer totals;
  /** 
   * 在职人数
   */
  @TableField("payrolls")
  private Integer payrolls;
  /** 
   * 离职人数
   */
  @TableField("departures")
  private Integer departures;
  /** 
   * 数据
   */
  @TableField("data")
  private String data;
  /** 
   * 创建时间
   */
  @TableField("create_time")
  private Date createTime;

  public EmArchive() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getOpuser() {
    return opuser;
  }

  public void setOpuser(String opuser) {
    this.opuser = opuser;
  }

  public String getMonth() {
    return month;
  }

  public void setMonth(String month) {
    this.month = month;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public Integer getTotals() {
    return totals;
  }

  public void setTotals(Integer totals) {
    this.totals = totals;
  }

  public Integer getPayrolls() {
    return payrolls;
  }

  public void setPayrolls(Integer payrolls) {
    this.payrolls = payrolls;
  }

  public Integer getDepartures() {
    return departures;
  }

  public void setDepartures(Integer departures) {
    this.departures = departures;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }
}

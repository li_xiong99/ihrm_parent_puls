package com.kun.ihrm.employee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.employee.entity.AtteArchiveMonthlyInfo;
import org.apache.ibatis.annotations.Mapper;


/**
 * 考勤归档信息详情表 Service接口
 * @packageName com.kun.ihrm.employee.copy
 * @fileName AtteArchiveMonthlyInfoService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年9月8日 下午4:59:41
 * @lastModify 2023年9月8日 下午4:59:41
 * @version 1.0.0
 */
@Mapper
public interface AtteArchiveMonthlyInfoService extends IService<AtteArchiveMonthlyInfo>{


}

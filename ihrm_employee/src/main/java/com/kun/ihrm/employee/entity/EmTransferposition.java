package com.kun.ihrm.employee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;


/**
 * EmTransferposition实体类
 * @packageName .main.java
 * @fileName EmTransferposition.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */

@TableName("em_transferposition")
public class EmTransferposition {

  /** 
   * 用户ID
   */
  @TableId(value = "user_id", type = IdType.AUTO)
  private String userId;
  /** 
   * 岗位
   */
  @TableField("post")
  private String post;
  /** 
   * 职级
   */
  @TableField("rank")
  private String rank;
  /** 
   * 汇报对象
   */
  @TableField("reporting_object")
  private String reportingObject;
  /** 
   * HRBP
   */
  @TableField("hrbp")
  private String hrbp;
  /** 
   * 调岗时间
   */
  @TableField("adjustment_time")
  private Date adjustmentTime;
  /** 
   * 调岗原因
   */
  @TableField("cause_of_adjusting_post")
  private String causeOfAdjustingPost;
  /** 
   * 附件 [1,2,3]
   */
  @TableField("enclosure")
  private String enclosure;
  /** 
   * 管理形式
   */
  @TableField("form_of_management")
  private String formOfManagement;
  /** 
   * 工作城市
   */
  @TableField("working_city")
  private String workingCity;
  /** 
   * 纳税城市
   */
  @TableField("taxable_city")
  private String taxableCity;
  /** 
   * 现合同开始时间
   */
  @TableField("current_contract_start_time")
  private String currentContractStartTime;
  /** 
   * 现合同结束时间
   */
  @TableField("closing_time_of_current_contract")
  private String closingTimeOfCurrentContract;
  /** 
   * 工作地点
   */
  @TableField("working_place")
  private String workingPlace;
  /** 
   * 首次合同开始时间
   */
  @TableField("initial_contract_start_time")
  private String initialContractStartTime;
  /** 
   * 首次合同结束时间
   */
  @TableField("first_contract_termination_time")
  private String firstContractTerminationTime;
  /** 
   * 合同期限
   */
  @TableField("contract_period")
  private String contractPeriod;
  /** 
   * 续签次数
   */
  @TableField("renewal_number")
  private Integer renewalNumber;
  /** 
   * 推荐企业人
   */
  @TableField("recommender_business_people")
  private String recommenderBusinessPeople;
  /** 
   * 单据状态 1是未执行，2是已执行
   */
  @TableField("estatus")
  private Integer estatus;
  /** 
   * 创建时间
   */
  @TableField("create_time")
  private Date createTime;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getPost() {
    return post;
  }

  public void setPost(String post) {
    this.post = post;
  }

  public String getRank() {
    return rank;
  }

  public void setRank(String rank) {
    this.rank = rank;
  }

  public String getReportingObject() {
    return reportingObject;
  }

  public void setReportingObject(String reportingObject) {
    this.reportingObject = reportingObject;
  }

  public String getHrbp() {
    return hrbp;
  }

  public void setHrbp(String hrbp) {
    this.hrbp = hrbp;
  }

  public Date getAdjustmentTime() {
    return adjustmentTime;
  }

  public void setAdjustmentTime(Date adjustmentTime) {
    this.adjustmentTime = adjustmentTime;
  }

  public String getCauseOfAdjustingPost() {
    return causeOfAdjustingPost;
  }

  public void setCauseOfAdjustingPost(String causeOfAdjustingPost) {
    this.causeOfAdjustingPost = causeOfAdjustingPost;
  }

  public String getEnclosure() {
    return enclosure;
  }

  public void setEnclosure(String enclosure) {
    this.enclosure = enclosure;
  }

  public String getFormOfManagement() {
    return formOfManagement;
  }

  public void setFormOfManagement(String formOfManagement) {
    this.formOfManagement = formOfManagement;
  }

  public String getWorkingCity() {
    return workingCity;
  }

  public void setWorkingCity(String workingCity) {
    this.workingCity = workingCity;
  }

  public String getTaxableCity() {
    return taxableCity;
  }

  public void setTaxableCity(String taxableCity) {
    this.taxableCity = taxableCity;
  }

  public String getCurrentContractStartTime() {
    return currentContractStartTime;
  }

  public void setCurrentContractStartTime(String currentContractStartTime) {
    this.currentContractStartTime = currentContractStartTime;
  }

  public String getClosingTimeOfCurrentContract() {
    return closingTimeOfCurrentContract;
  }

  public void setClosingTimeOfCurrentContract(String closingTimeOfCurrentContract) {
    this.closingTimeOfCurrentContract = closingTimeOfCurrentContract;
  }

  public String getWorkingPlace() {
    return workingPlace;
  }

  public void setWorkingPlace(String workingPlace) {
    this.workingPlace = workingPlace;
  }

  public String getInitialContractStartTime() {
    return initialContractStartTime;
  }

  public void setInitialContractStartTime(String initialContractStartTime) {
    this.initialContractStartTime = initialContractStartTime;
  }

  public String getFirstContractTerminationTime() {
    return firstContractTerminationTime;
  }

  public void setFirstContractTerminationTime(String firstContractTerminationTime) {
    this.firstContractTerminationTime = firstContractTerminationTime;
  }

  public String getContractPeriod() {
    return contractPeriod;
  }

  public void setContractPeriod(String contractPeriod) {
    this.contractPeriod = contractPeriod;
  }

  public Integer getRenewalNumber() {
    return renewalNumber;
  }

  public void setRenewalNumber(Integer renewalNumber) {
    this.renewalNumber = renewalNumber;
  }

  public String getRecommenderBusinessPeople() {
    return recommenderBusinessPeople;
  }

  public void setRecommenderBusinessPeople(String recommenderBusinessPeople) {
    this.recommenderBusinessPeople = recommenderBusinessPeople;
  }

  public Integer getEstatus() {
    return estatus;
  }

  public void setEstatus(Integer estatus) {
    this.estatus = estatus;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }
}

package com.kun.ihrm.employee.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.employee.entity.AtteArchiveMonthly;
import com.kun.ihrm.employee.mapper.AtteArchiveMonthlyMapper;
import com.kun.ihrm.employee.service.AtteArchiveMonthlyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 考勤归档信息表 Service接口实现
 * @packageName com.kun.ihrm.employee.copy
 * @fileName AtteArchiveMonthlyServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年9月8日 下午4:59:41
 * @lastModify 2023年9月8日 下午4:59:41
 * @version 1.0.0
 */
@Service
public class AtteArchiveMonthlyServiceImpl 
				extends ServiceImpl<AtteArchiveMonthlyMapper, AtteArchiveMonthly> 
				implements AtteArchiveMonthlyService {
	@Resource
	private AtteArchiveMonthlyMapper  atteArchiveMonthlyMapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(AtteArchiveMonthlyServiceImpl.class);
	@Override
	public PageResult getbyPageList(Integer page, Integer pageSize, String year) {
		PageResult pageResult = new PageResult();
		pageResult.setRows(atteArchiveMonthlyMapper.findByPageList((page - 1) * pageSize, pageSize,year));
		pageResult.setTotal(10l);
		return pageResult;
	}
}

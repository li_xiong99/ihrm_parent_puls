package com.kun.ihrm.employee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.employee.entity.EmUserCompanyPersonal;
import org.apache.ibatis.annotations.Mapper;

/**
 * EmuserCompanyPersonalService接口
 * @packageName .main.java
 * @fileName EmUserCompanyPersonalService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
@Mapper
public interface EmUserCompanyPersonalService extends IService<EmUserCompanyPersonal> {

  EmUserCompanyPersonal selectByInfo(String id);

    boolean updateCompany(String id);
}

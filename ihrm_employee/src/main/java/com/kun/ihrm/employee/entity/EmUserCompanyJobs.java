package com.kun.ihrm.employee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * EmuserCompanyJobs实体类
 * @packageName .main.java
 * @fileName EmuserCompanyJobs.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */

@TableName("em_user_company_jobs")
public class EmUserCompanyJobs {

  /** 
   * 员工ID
   */
  @TableId(value = "user_id", type = IdType.AUTO)
  private String userId;
  /** 
   * 企业ID
   */
  @TableField("company_id")
  private String companyId;
  /** 
   * 岗位
   */
  @TableField("post")
  private String post;
  /** 
   * 工作邮箱
   */
  @TableField("work_mailbox")
  private String workMailbox;
  /** 
   * 职级
   */
  @TableField("rank")
  private String rank;
  /** 
   * 转正评价
   */
  @TableField("correction_evaluation")
  private String correctionEvaluation;
  /** 
   * 汇报对象
   */
  @TableField("report_id")
  private String reportId;

  @TableField("report_name")
  private String reportName;
  /** 
   * 转正状态
   */
  @TableField("state_of_correction")
  private String stateOfCorrection;
  /** 
   * hrbp
   */
  @TableField("hrbp")
  private String hrbp;
  /** 
   * 首次参加工作时间
   */
  @TableField("working_time_for_the_first_time")
  private String workingTimeForTheFirstTime;
  /** 
   * 调整司龄天
   */
  @TableField("adjustment_agedays")
  private Integer adjustmentAgedays;
  /** 
   * 调整工龄天
   */
  @TableField("adjustment_of_length_of_service")
  private Integer adjustmentOfLengthOfService;
  /** 
   * 工作城市
   */
  @TableField("working_city")
  private String workingCity;
  /** 
   * 纳税城市
   */
  @TableField("taxable_city")
  private String taxableCity;
  /** 
   * 现合同开始时间
   */
  @TableField("current_contract_start_time")
  private String currentContractStartTime;
  /** 
   * 现合同结束时间
   */
  @TableField("closing_time_of_current_contract")
  private String closingTimeOfCurrentContract;
  /** 
   * 首次合同开始时间
   */
  @TableField("initial_contract_start_time")
  private String initialContractStartTime;
  /** 
   * 首次合同结束时间
   */
  @TableField("first_contract_termination_time")
  private String firstContractTerminationTime;
  /** 
   * 合同期限
   */
  @TableField("contract_period")
  private String contractPeriod;
  /** 
   * 合同文件
   */
  @TableField("contract_documents")
  private String contractDocuments;
  /** 
   * 续签次数
   */
  @TableField("renewal_number")
  private Integer renewalNumber;
  /** 
   * 其他招聘渠道
   */
  @TableField("other_recruitment_channels")
  private String otherRecruitmentChannels;
  /** 
   * 招聘渠道
   */
  @TableField("recruitment_channels")
  private String recruitmentChannels;
  /** 
   * 社招校招
   */
  @TableField("social_recruitment")
  private String socialRecruitment;
  /** 
   * 推荐企业人
   */
  @TableField("recommender_business_people")
  private String recommenderBusinessPeople;

  public EmUserCompanyJobs() {
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getPost() {
    return post;
  }

  public void setPost(String post) {
    this.post = post;
  }

  public String getWorkMailbox() {
    return workMailbox;
  }

  public void setWorkMailbox(String workMailbox) {
    this.workMailbox = workMailbox;
  }

  public String getRank() {
    return rank;
  }

  public void setRank(String rank) {
    this.rank = rank;
  }

  public String getCorrectionEvaluation() {
    return correctionEvaluation;
  }

  public void setCorrectionEvaluation(String correctionEvaluation) {
    this.correctionEvaluation = correctionEvaluation;
  }

  public String getReportId() {
    return reportId;
  }

  public void setReportId(String reportId) {
    this.reportId = reportId;
  }

  public String getReportName() {
    return reportName;
  }

  public void setReportName(String reportName) {
    this.reportName = reportName;
  }

  public String getStateOfCorrection() {
    return stateOfCorrection;
  }

  public void setStateOfCorrection(String stateOfCorrection) {
    this.stateOfCorrection = stateOfCorrection;
  }

  public String getHrbp() {
    return hrbp;
  }

  public void setHrbp(String hrbp) {
    this.hrbp = hrbp;
  }

  public String getWorkingTimeForTheFirstTime() {
    return workingTimeForTheFirstTime;
  }

  public void setWorkingTimeForTheFirstTime(String workingTimeForTheFirstTime) {
    this.workingTimeForTheFirstTime = workingTimeForTheFirstTime;
  }

  public Integer getAdjustmentAgedays() {
    return adjustmentAgedays;
  }

  public void setAdjustmentAgedays(Integer adjustmentAgedays) {
    this.adjustmentAgedays = adjustmentAgedays;
  }

  public Integer getAdjustmentOfLengthOfService() {
    return adjustmentOfLengthOfService;
  }

  public void setAdjustmentOfLengthOfService(Integer adjustmentOfLengthOfService) {
    this.adjustmentOfLengthOfService = adjustmentOfLengthOfService;
  }

  public String getWorkingCity() {
    return workingCity;
  }

  public void setWorkingCity(String workingCity) {
    this.workingCity = workingCity;
  }

  public String getTaxableCity() {
    return taxableCity;
  }

  public void setTaxableCity(String taxableCity) {
    this.taxableCity = taxableCity;
  }

  public String getCurrentContractStartTime() {
    return currentContractStartTime;
  }

  public void setCurrentContractStartTime(String currentContractStartTime) {
    this.currentContractStartTime = currentContractStartTime;
  }

  public String getClosingTimeOfCurrentContract() {
    return closingTimeOfCurrentContract;
  }

  public void setClosingTimeOfCurrentContract(String closingTimeOfCurrentContract) {
    this.closingTimeOfCurrentContract = closingTimeOfCurrentContract;
  }

  public String getInitialContractStartTime() {
    return initialContractStartTime;
  }

  public void setInitialContractStartTime(String initialContractStartTime) {
    this.initialContractStartTime = initialContractStartTime;
  }

  public String getFirstContractTerminationTime() {
    return firstContractTerminationTime;
  }

  public void setFirstContractTerminationTime(String firstContractTerminationTime) {
    this.firstContractTerminationTime = firstContractTerminationTime;
  }

  public String getContractPeriod() {
    return contractPeriod;
  }

  public void setContractPeriod(String contractPeriod) {
    this.contractPeriod = contractPeriod;
  }

  public String getContractDocuments() {
    return contractDocuments;
  }

  public void setContractDocuments(String contractDocuments) {
    this.contractDocuments = contractDocuments;
  }

  public Integer getRenewalNumber() {
    return renewalNumber;
  }

  public void setRenewalNumber(Integer renewalNumber) {
    this.renewalNumber = renewalNumber;
  }

  public String getOtherRecruitmentChannels() {
    return otherRecruitmentChannels;
  }

  public void setOtherRecruitmentChannels(String otherRecruitmentChannels) {
    this.otherRecruitmentChannels = otherRecruitmentChannels;
  }

  public String getRecruitmentChannels() {
    return recruitmentChannels;
  }

  public void setRecruitmentChannels(String recruitmentChannels) {
    this.recruitmentChannels = recruitmentChannels;
  }

  public String getSocialRecruitment() {
    return socialRecruitment;
  }

  public void setSocialRecruitment(String socialRecruitment) {
    this.socialRecruitment = socialRecruitment;
  }

  public String getRecommenderBusinessPeople() {
    return recommenderBusinessPeople;
  }

  public void setRecommenderBusinessPeople(String recommenderBusinessPeople) {
    this.recommenderBusinessPeople = recommenderBusinessPeople;
  }
}

package com.kun.ihrm.employee.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.employee.entity.EmUserCompanyJobs;
import com.kun.ihrm.employee.mapper.EmUserCompanyJobsMapper;
import com.kun.ihrm.employee.service.EmUserCompanyJobsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
/**
 * EmuserCompanyJobsService接口实现
 * @packageName .main.java
 * @fileName EmuserCompanyJobsServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 * 				extends ServiceImpl<, EmUserCompanyJobs>
 */
@Service
public class EmUserCompanyJobsServiceImpl
				extends ServiceImpl<EmUserCompanyJobsMapper,EmUserCompanyJobs>
				implements EmUserCompanyJobsService {

    private static final Logger LOGGER =  LoggerFactory.getLogger(EmUserCompanyJobsServiceImpl.class);




}

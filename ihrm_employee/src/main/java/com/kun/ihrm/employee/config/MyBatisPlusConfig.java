package com.kun.ihrm.employee.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * MyBatis-Plus配置类
 * @packageName .main.java.config;
 * @fileName MyBatisPlusConfig.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
@Configuration
@MapperScan(".main.java.mapper")
public class MyBatisPlusConfig {

//    /**
//     * 新版本 分页插件，一级缓存和二级缓存遵循MyBatis的规则
//     */
//    @Bean
//    public MybatisPlusInterceptor mybatisPlusInterceptor() {
//        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
//        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
//        return interceptor;
//    }
@Bean
public PaginationInterceptor paginationInterceptor(){
    return new PaginationInterceptor();
}
}

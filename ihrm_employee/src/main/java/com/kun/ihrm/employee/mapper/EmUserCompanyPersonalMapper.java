package com.kun.ihrm.employee.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.employee.entity.EmUserCompanyPersonal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * EmuserCompanyPersonalMapper接口
 * @packageName .main.java
 * @fileName EmuserCompanyPersonalMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
@Mapper
public interface EmUserCompanyPersonalMapper extends BaseMapper<EmUserCompanyPersonal> {
    EmUserCompanyPersonal selectByInfo(@Param("id") String id);
    boolean updateCompany(@Param("id") String id);
}

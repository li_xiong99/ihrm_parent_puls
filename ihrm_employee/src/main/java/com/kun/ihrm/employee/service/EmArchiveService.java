package com.kun.ihrm.employee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.employee.entity.EmArchive;
import com.kun.ihrm.model.employee.EmployeeArchive;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.Map;

/**
 * EmArchiveService接口
 * @packageName .main.java
 * @fileName EmArchiveService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
public interface EmArchiveService extends IService<EmArchive> {
    public PageResult<EmArchive> findSearch(Map map, Integer page, Integer pagesize);
}

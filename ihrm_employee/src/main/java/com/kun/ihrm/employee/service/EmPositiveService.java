package com.kun.ihrm.employee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.employee.entity.EmPositive;

/**
 * EmPositiveService接口
 * @packageName .main.java
 * @fileName EmPositiveService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
public interface EmPositiveService extends IService<EmPositive> {


}

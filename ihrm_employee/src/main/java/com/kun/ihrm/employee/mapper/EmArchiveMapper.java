package com.kun.ihrm.employee.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.employee.entity.EmArchive;
import com.kun.ihrm.model.employee.EmployeeArchive;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import java.util.Map;


/**
 * EmArchiveMapper接口
 * @packageName .main.java
 * @fileName EmArchiveMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
public interface EmArchiveMapper extends BaseMapper<EmArchive> {
    PageResult<EmArchive > findSearch(@Param("map") Map map, @Param("page") Integer page, @Param("pageSize") Integer pageSize);

    List<EmArchive > findList();
}

package com.kun.ihrm.employee.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.employee.entity.AtteArchiveMonthly;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 考勤归档信息表 Mapper接口
 * @packageName com.kun.ihrm.employee.copy
 * @fileName AtteArchiveMonthlyMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年9月8日 下午4:59:41
 * @lastModify 2023年9月8日 下午4:59:41
 * @version 1.0.0
 */
@Mapper
public interface AtteArchiveMonthlyMapper extends BaseMapper<AtteArchiveMonthly> {
    List<AtteArchiveMonthly> findByPageList(@Param("page") Integer page, @Param("pageSize") Integer pageSize, @Param("year") String year);

}

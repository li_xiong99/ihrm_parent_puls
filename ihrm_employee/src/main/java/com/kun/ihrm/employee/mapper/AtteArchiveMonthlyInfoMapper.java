package com.kun.ihrm.employee.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.employee.entity.AtteArchiveMonthlyInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 考勤归档信息详情表 Mapper接口
 * @packageName com.kun.ihrm.employee.copy
 * @fileName AtteArchiveMonthlyInfoMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年9月8日 下午4:59:41
 * @lastModify 2023年9月8日 下午4:59:41
 * @version 1.0.0
 */
@Mapper
public interface AtteArchiveMonthlyInfoMapper extends BaseMapper<AtteArchiveMonthlyInfo> {
    AtteArchiveMonthlyInfo getUserInfoAndMothByYear(String year, String month, String type);

}

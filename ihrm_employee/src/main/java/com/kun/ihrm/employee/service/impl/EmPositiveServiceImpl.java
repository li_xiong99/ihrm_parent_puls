package com.kun.ihrm.employee.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.employee.entity.EmPositive;
import com.kun.ihrm.employee.mapper.EmPositiveMapper;
import com.kun.ihrm.employee.service.EmPositiveService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
/**
 * EmPositiveService接口实现
 * @packageName .main.java
 * @fileName EmPositiveServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
@Service
public class EmPositiveServiceImpl 
				extends ServiceImpl<EmPositiveMapper, EmPositive>
				implements EmPositiveService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmPositiveServiceImpl.class);


}

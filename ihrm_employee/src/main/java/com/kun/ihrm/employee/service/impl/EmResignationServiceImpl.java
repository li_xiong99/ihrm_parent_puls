package com.kun.ihrm.employee.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.employee.entity.EmResignation;
import com.kun.ihrm.employee.mapper.EmResignationMapper;
import com.kun.ihrm.employee.service.EmResignationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * EmResignationService接口实现
 * @packageName .main.java
 * @fileName EmResignationServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
@Service
public class EmResignationServiceImpl 
				extends ServiceImpl<EmResignationMapper, EmResignation>
				implements EmResignationService {

    private static final Logger LOGGER =  LoggerFactory.getLogger(EmResignationServiceImpl.class);


}

package com.kun.ihrm.employee.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.employee.entity.AtteArchiveMonthlyInfo;
import com.kun.ihrm.employee.mapper.AtteArchiveMonthlyInfoMapper;
import com.kun.ihrm.employee.service.AtteArchiveMonthlyInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 考勤归档信息详情表 Service接口实现
 * @packageName com.kun.ihrm.employee.copy
 * @fileName AtteArchiveMonthlyInfoServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年9月8日 下午4:59:41
 * @lastModify 2023年9月8日 下午4:59:41
 * @version 1.0.0
 */
@Service
public class AtteArchiveMonthlyInfoServiceImpl 
				extends ServiceImpl<AtteArchiveMonthlyInfoMapper, AtteArchiveMonthlyInfo>
				implements AtteArchiveMonthlyInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AtteArchiveMonthlyInfoServiceImpl.class);


}

package com.kun.ihrm.employee.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.employee.entity.EmUserCompanyPersonal;
import com.kun.ihrm.employee.mapper.EmUserCompanyPersonalMapper;
import com.kun.ihrm.employee.service.EmUserCompanyPersonalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * EmuserCompanyPersonalService接口实现
 * @packageName .main.java
 * @fileName EmUserCompanyPersonalServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
@Service
public class EmUserCompanyPersonalServiceImpl
				extends ServiceImpl<EmUserCompanyPersonalMapper, EmUserCompanyPersonal>
				implements EmUserCompanyPersonalService {

    private static final Logger LOGGER =  LoggerFactory.getLogger(EmUserCompanyPersonalServiceImpl.class);

    @Resource
	EmUserCompanyPersonalMapper userCompanyPersonalMapper;

	@Override
	public EmUserCompanyPersonal selectByInfo(String id) {
		return userCompanyPersonalMapper.selectByInfo(id);
	}

	@Override
	public boolean updateCompany(String id) {

		return baseMapper.updateCompany(id);
	}
}

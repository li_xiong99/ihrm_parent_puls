package com.kun.ihrm.employee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.employee.entity.AtteArchiveMonthly;


/**
 * 考勤归档信息表 Service接口
 * @packageName com.kun.ihrm.employee.copy
 * @fileName AtteArchiveMonthlyService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年9月8日 下午4:59:41
 * @lastModify 2023年9月8日 下午4:59:41
 * @version 1.0.0
 */
public interface AtteArchiveMonthlyService extends IService<AtteArchiveMonthly>{

    PageResult getbyPageList(Integer page, Integer pageSize,String year);
}

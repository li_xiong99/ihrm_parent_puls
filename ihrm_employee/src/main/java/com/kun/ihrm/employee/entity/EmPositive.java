package com.kun.ihrm.employee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


/**
 * EmPositive实体类
 * @packageName .main.java
 * @fileName EmPositive.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */

@TableName("em_positive")
public class EmPositive {

  /** 
   * 员工ID
   */
  @TableId(value = "user_id", type = IdType.INPUT)
  private String UserId;
  /** 
   * 转正日期
   */
  @TableField("date_of_correction")
  @JsonFormat(pattern="yyyy-MM-dd")
  private Date dateOfCorrection;
  /** 
   * 转正评价
   */
  @TableField("correction_evaluation")
  private String correctionEvaluation;
  /** 
   * 附件
   */
  @TableField("enclosure")
  private String enclosure;
  /** 
   * 单据状态 1是未执行，2是已执行
   */
  @TableField("estatus")
  private Integer estatus;
  /** 
   * 创建时间
   */
  @TableField("create_time")
  private Date createTime;


  public EmPositive() {
  }

  public String getUserId() {
    return UserId;
  }

  public void setUserId(String userId) {
    UserId = userId;
  }

  public Date getDateOfCorrection() {
    return dateOfCorrection;
  }

  public void setDateOfCorrection(Date dateOfCorrection) {
    this.dateOfCorrection = dateOfCorrection;
  }

  public String getCorrectionEvaluation() {
    return correctionEvaluation;
  }

  public void setCorrectionEvaluation(String correctionEvaluation) {
    this.correctionEvaluation = correctionEvaluation;
  }

  public String getEnclosure() {
    return enclosure;
  }

  public void setEnclosure(String enclosure) {
    this.enclosure = enclosure;
  }

  public Integer getEstatus() {
    return estatus;
  }

  public void setEstatus(Integer estatus) {
    this.estatus = estatus;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }
}

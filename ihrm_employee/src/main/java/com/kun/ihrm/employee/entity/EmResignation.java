package com.kun.ihrm.employee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;


/**
 * EmResignation实体类
 * @packageName .main.java
 * @fileName EmResignation.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */

@TableName("em_resignation")
public class EmResignation {

  /** 
   * 用户ID
   */
  @TableId(value = "user_id", type = IdType.INPUT)
  private String userId;

  @TableField("resignation_time")
  private String resignationTime;
  /** 
   * 离职类型
   */
  @TableField("type_of_turnover")
  private String typeOfTurnover;
  /** 
   * 申请离职原因
   */
  @TableField("reasons_for_leaving")
  private String reasonsForLeaving;
  /** 
   * 补偿金
   */
  @TableField("compensation")
  private String compensation;
  /** 
   * 代通知金
   */
  @TableField("notifications")
  private String notifications;
  /** 
   * 社保减员月
   */
  @TableField("social_security_reduction_month")
  private String socialSecurityReductionMonth;
  /** 
   * 公积金减员月
   */
  @TableField("provident_fund_reduction_month")
  private String providentFundReductionMonth;
  /** 
   * 图片
   */
  @TableField("picture")
  private String picture;
  /** 
   * 创建时间
   */
  @TableField("create_time")
  private Date createTime;

  public EmResignation() {
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getResignationTime() {
    return resignationTime;
  }

  public void setResignationTime(String resignationTime) {
    this.resignationTime = resignationTime;
  }

  public String getTypeOfTurnover() {
    return typeOfTurnover;
  }

  public void setTypeOfTurnover(String typeOfTurnover) {
    this.typeOfTurnover = typeOfTurnover;
  }

  public String getReasonsForLeaving() {
    return reasonsForLeaving;
  }

  public void setReasonsForLeaving(String reasonsForLeaving) {
    this.reasonsForLeaving = reasonsForLeaving;
  }

  public String getCompensation() {
    return compensation;
  }

  public void setCompensation(String compensation) {
    this.compensation = compensation;
  }

  public String getNotifications() {
    return notifications;
  }

  public void setNotifications(String notifications) {
    this.notifications = notifications;
  }

  public String getSocialSecurityReductionMonth() {
    return socialSecurityReductionMonth;
  }

  public void setSocialSecurityReductionMonth(String socialSecurityReductionMonth) {
    this.socialSecurityReductionMonth = socialSecurityReductionMonth;
  }

  public String getProvidentFundReductionMonth() {
    return providentFundReductionMonth;
  }

  public void setProvidentFundReductionMonth(String providentFundReductionMonth) {
    this.providentFundReductionMonth = providentFundReductionMonth;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }
}

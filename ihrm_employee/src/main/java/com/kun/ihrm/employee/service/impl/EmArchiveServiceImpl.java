package com.kun.ihrm.employee.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.employee.entity.EmArchive;
import com.kun.ihrm.employee.mapper.EmArchiveMapper;
import com.kun.ihrm.employee.service.EmArchiveService;
import com.kun.ihrm.model.employee.EmployeeArchive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * EmArchiveService接口实现
 * @packageName .main.java
 * @fileName EmArchiveServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
@Service
public class EmArchiveServiceImpl 
				extends ServiceImpl<EmArchiveMapper, EmArchive  >
				implements EmArchiveService {


    private static final Logger LOGGER = LoggerFactory.getLogger(EmArchiveServiceImpl.class);

	@Resource
	private EmArchiveMapper emArchiveMapper;

	@Override
	public PageResult<EmArchive > findSearch(Map map, Integer page, Integer pagesize) {
		PageResult pageResult=new PageResult();
		pageResult.setRows(emArchiveMapper.findList());
		pageResult.setTotal(10l);
		return emArchiveMapper.findSearch(map,(page-1)*pagesize,pagesize);
	}
}

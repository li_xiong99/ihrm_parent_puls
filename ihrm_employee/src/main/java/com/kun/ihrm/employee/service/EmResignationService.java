package com.kun.ihrm.employee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.employee.entity.EmResignation;


/**
 * EmResignationService接口
 * @packageName .main.java
 * @fileName EmResignationService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
public interface EmResignationService extends IService<EmResignation> {

}

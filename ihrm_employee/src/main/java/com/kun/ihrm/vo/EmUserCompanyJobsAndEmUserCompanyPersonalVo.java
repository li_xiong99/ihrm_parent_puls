package com.kun.ihrm.vo;

public class EmUserCompanyJobsAndEmUserCompanyPersonalVo{

    /**
     * 员工ID
     */
    private String UserId;
    /**
     * 企业ID
     */
    private String companyId;
    /**
     * 岗位
     */
    private String post;
    /**
     * 工作邮箱
     */
    private String workMailbox;
    /**
     * 职级
     */
    private String rank;
    /**
     * 转正评价
     */
    private String correctionEvaluation;
    /**
     * 汇报对象
     */
    private String reportId;


    private String reportName;
    /**
     * 转正状态
     */
    private String stateOfCorrection;
    /**
     * hrbp
     */
    private String hrbp;
    /**
     * 首次参加工作时间
     */
    private String workingTimeForTheFirstTime;
    /**
     * 调整司龄天
     */
    private Integer adjustmentAgedays;
    /**
     * 调整工龄天
     */
    private Integer adjustmentOfLengthOfService;
    /**
     * 工作城市
     */
    private String workingCity;
    /**
     * 纳税城市
     */
    private String taxableCity;
    /**
     * 现合同开始时间
     */
    private String currentContractStartTime;
    /**
     * 现合同结束时间
     */
    private String closingTimeOfCurrentContract;
    /**
     * 首次合同开始时间
     */
    private String initialContractStartTime;
    /**
     * 首次合同结束时间
     */
    private String firstContractTerminationTime;
    /**
     * 合同期限
     */
    private String contractPeriod;
    /**
     * 合同文件
     */
    private String contractDocuments;
    /**
     * 续签次数
     */
    private Integer renewalNumber;
    /**
     * 其他招聘渠道
     */
    private String otherRecruitmentChannels;
    /**
     * 招聘渠道
     */
    private String recruitmentChannels;
    /**
     * 社招校招
     */
    private String socialRecruitment;
    /**
     * 推荐企业人
     */
    private String recommenderBusinessPeople;

    private String username;


    private String mobile;


    private String timeOfEntry;

    private String departmentName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 出生日期
     */
    private String dateOfBirth;
    /**
     * 最高学历
     */
    private String theHighestDegreeOfEducation;
    /**
     * 国家地区
     */
    private String nationalArea;
    /**
     * 护照号
     */
    private String passportNo;
    /**
     * 身份证号
     */
    private String idNumber;
    /**
     * 身份证照片-正面
     */
    private String idCardPhotoPositive;
    /**
     * 身份证照片-背面
     */
    private String idCardPhotoBack;
    /**
     * 籍贯
     */
    private String nativePlace;
    /**
     * 民族
     */
    private String nation;
    /**
     * 英文名
     */
    private String englishName;
    /**
     * 婚姻状况
     */
    private String maritalStatus;
    /**
     * 员工照片
     */
    private String staffPhoto;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 属相
     */
    private String zodiac;
    /**
     * 年龄
     */
    private String age;
    /**
     * 星座
     */
    private String constellation;
    /**
     * 血型
     */
    private String bloodType;
    /**
     * 户籍所在地
     */
    private String domicile;
    /**
     * 政治面貌
     */
    private String politicalOutlook;
    /**
     * 入党时间
     */
    private String timeToJoinTheParty;
    /**
     * 存档机构
     */
    private String archivingOrganization;
    /**
     * 子女状态
     */
    private String stateOfChildren;
    /**
     * 子女有无商业保险
     */
    private String doChildrenHaveCommercialInsurance;
    /**
     * 有无违法违纪行为
     */
    private String isThereAnyViolationOfLawOrDiscipline;
    /**
     * 有无重大病史
     */
    private String areThereAnyMajorMedicalHistories;
    /**
     * QQ
     */
    private String qq;
    /**
     * 微信
     */
    private String wechat;
    /**
     * 居住证城市
     */
    private String residenceCardCity;
    /**
     * 居住证办理日期
     */
    private String dateOfResidencePermit;
    /**
     * 居住证截止日期
     */
    private String residencePermitDeadline;
    /**
     * 现居住地
     */
    private String placeOfResidence;
    /**
     * 通讯地址
     */
    private String postalAddress;

    private String contactTheMobilePhone;

    private String personalMailbox;
    /**
     * 紧急联系人
     */
    private String emergencyContact;
    /**
     * 紧急联系电话
     */
    private String emergencyContactNumber;
    /**
     * 社保电脑号
     */
    private String socialSecurityComputerNumber;
    /**
     * 公积金账号
     */
    private String providentFundAccount;
    /**
     * 银行卡号
     */
    private String bankCardNumber;
    /**
     * 开户行
     */
    private String openingBank;
    /**
     * 学历类型
     */
    private String educationalType;
    /**
     * 毕业学校
     */
    private String graduateSchool;
    /**
     * 入学时间
     */
    private String enrolmentTime;
    /**
     * 毕业时间
     */
    private String graduationTime;
    /**
     * 专业
     */
    private String major;
    /**
     * 毕业证书
     */
    private String graduationCertificate;
    /**
     * 学位证书
     */
    private String certificateOfAcademicDegree;
    /**
     * 上家公司
     */
    private String homeCompany;
    /**
     * 职称
     */
    private String title;
    /**
     * 简历
     */
    private String resume;
    /**
     * 有无竞业限制
     */
    private String isThereAnyCompetitionRestriction;
    /**
     * 前公司离职证明
     */
    private String proofOfDepartureOfFormerCompany;
    /**
     * 备注
     */
    private String remarks;

}

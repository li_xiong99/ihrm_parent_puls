package com.kun.ihrm.attendance.controller;


import com.kun.ihrm.attendance.entity.AtteAttendanceConfig;
import com.kun.ihrm.attendance.service.impl.AtteAttendanceConfigServiceImpl;
import com.kun.ihrm.common.controller.BaseController;
import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 设置考勤设置的controller
 * @author: hyl
 * @date: 2020/03/16
 **/
@RestController
@RequestMapping("/cfg")
public class ConfigController extends BaseController {
    @Resource
    private AtteAttendanceConfigServiceImpl configurationService;

    /**
     * 获取考勤设置
     */
    @RequestMapping(value = "/atte/item" , method = RequestMethod.POST)
    public Result atteConfig(String departmentId){
        AtteAttendanceConfig ac = configurationService.getAtteConfig(companyId, departmentId);
        return new Result(ResultCode.SUCCESS , ac);
    }

    /**
     * 保存考勤设置
     */
    @RequestMapping(value = "/atte" , method = RequestMethod.PUT)
    public Result saveAtteConfig(@RequestBody AtteAttendanceConfig ac){
        ac.setCompanyId(companyId);
        configurationService.saveAtteConfig(ac);
        return new Result(ResultCode.SUCCESS);
    }

}

package com.kun.ihrm.attendance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.attendance.entity.AtteAttendanceConfig;


/**
 * 考勤配置表 Service接口
 * @packageName com.ihrm.atte.copy
 * @fileName AtteAttendanceConfigService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */
public interface AtteAttendanceConfigService extends IService<AtteAttendanceConfig>{

    /**
     * 查询考勤设置
     *
     * @param companyId    企业id
     * @param departmentId 部门id
     * @return 企业id和部门id对应的考勤设置
     */
    AtteAttendanceConfig getAtteConfig(String companyId, String departmentId);

    void saveAtteConfig(AtteAttendanceConfig ac);
}

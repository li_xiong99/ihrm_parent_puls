package com.kun.ihrm.attendance.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.attendance.entity.AtteAttendanceConfig;


/**
 * 考勤配置表 Mapper接口
 * @packageName com.ihrm.atte.copy
 * @fileName AtteAttendanceConfigMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */
public interface AtteAttendanceConfigMapper extends BaseMapper<AtteAttendanceConfig> {

}

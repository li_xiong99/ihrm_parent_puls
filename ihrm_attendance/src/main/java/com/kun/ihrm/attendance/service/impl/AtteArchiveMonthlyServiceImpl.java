package com.kun.ihrm.attendance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.attendance.entity.AtteArchiveMonthly;
import com.kun.ihrm.attendance.entity.AtteArchiveMonthlyInfo;
import com.kun.ihrm.attendance.entity.BsUser;
import com.kun.ihrm.attendance.mapper.AtteArchiveMonthlyMapper;
import com.kun.ihrm.attendance.service.AtteArchiveMonthlyInfoService;
import com.kun.ihrm.attendance.service.AtteArchiveMonthlyService;
import com.kun.ihrm.attendance.service.AtteAttendanceService;
import com.kun.ihrm.attendance.service.BsUserService;
import com.kun.ihrm.common.utils.IdWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 考勤归档信息表 Service接口实现
 *
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @version 1.0.0
 * @packageName com.ihrm.atte.copy
 * @fileName AtteArchiveMonthlyServiceImpl.java
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 */
@Service
public class AtteArchiveMonthlyServiceImpl
        extends ServiceImpl<AtteArchiveMonthlyMapper, AtteArchiveMonthly>
        implements AtteArchiveMonthlyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AtteArchiveMonthlyServiceImpl.class);

    @Resource
    private BsUserService userService;

    @Resource
    private AtteAttendanceService attendanceService;

    @Resource
    private AtteArchiveMonthlyInfoService infoService;

    @Resource
    private IdWorker idWorker;

    @Override
    public List<AtteArchiveMonthlyInfoService> getReports(String atteDate, String companyId) {
        return null;
    }

    @Override
    public boolean newReports(String atteDate, String companyId, String userId) {
        AtteArchiveMonthly archiveMonthly = this.getOne(new QueryWrapper<AtteArchiveMonthly>().eq("company_id", companyId));
        if (archiveMonthly == null) {
            log.error("考勤归档信息表实体对象为空 ,公司id = {}" + companyId);
            return false;
        }
        archiveMonthly.setArchiveYear(atteDate.substring(0, 4));
        archiveMonthly.setArchiveMonth(atteDate.substring(4, 6));
        archiveMonthly.setUpdateBy(userId);
        archiveMonthly.setUpdateDate(new Date());
        return this.save(archiveMonthly);
    }

    @Override
    public List<AtteArchiveMonthly> findByYear(String year, String companyId) {
        return this.list(new QueryWrapper<AtteArchiveMonthly>()
                .eq("company_id", companyId)
                .eq("archive_year", year)
        );
    }

    @Override
    public void saveArchive(String archiveDate, String companyId) {
        //查询所有企业用户
        List<BsUser> users = userService.list(new QueryWrapper<BsUser>()
                .eq("company_id", companyId)
        );
        //保存归档主表数据
        AtteArchiveMonthly archiveMonthly = new AtteArchiveMonthly();
        archiveMonthly.setId(idWorker.nextId());
        archiveMonthly.setCompanyId(companyId);
        archiveMonthly.setArchiveYear(archiveDate.substring(0, 4));
        archiveMonthly.setArchiveMonth(archiveDate.substring(5));
        //保存归档明细表数据
        for (BsUser user : users) {
            AtteArchiveMonthlyInfo info = new AtteArchiveMonthlyInfo(user);
            //统计每个用户的考勤数量
            Map<String, String> map = attendanceService.statisByUser(user.getId(), archiveDate + "%");
            info.setStatisData(map);
            info.setId(idWorker.nextId() + "");
            info.setAtteArchiveMonthlyId(archiveMonthly.getId());
            info.setArchiveDate(archiveDate);
            infoService.save(info);
        }
        //总人数
        archiveMonthly.setTotalPeopleNum(users.size());
        archiveMonthly.setFullAttePeopleNum(users.size());
        archiveMonthly.setIsArchived(0);
        this.save(archiveMonthly);
    }

    @Override
    public boolean newReports(String atteDate, String companyId) {



        String year = atteDate.substring(0, 4);
        String month = atteDate.substring(4, 6);
        AtteArchiveMonthly monthly = new AtteArchiveMonthly();
        monthly.setId(idWorker.nextId());
        monthly.setCompanyId(companyId);
//        monthly.setDepartmentId(); 部门ID
        monthly.setArchiveYear(year);
        monthly.setArchiveMonth(month);



        //TODO
//        return false;
        return this.save(monthly);
    }
}

package com.kun.ihrm.attendance.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.attendance.entity.AtteCompanySettings;


/**
 * 考勤-企业设置信息 Mapper接口
 * @packageName com.ihrm.atte
 * @fileName AtteCompanySettingsMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午9:41:01
 * @lastModify 2023年8月25日 上午9:41:01
 * @version 1.0.0
 */
public interface AtteCompanySettingsMapper extends BaseMapper<AtteCompanySettings> {

}

package com.kun.ihrm.attendance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.attendance.entity.AtteArchiveMonthly;

import java.util.List;

;


/**
 * 考勤归档信息表 Service接口
 * @packageName com.ihrm.atte.copy
 * @fileName AtteArchiveMonthlyService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */
public interface AtteArchiveMonthlyService extends IService<AtteArchiveMonthly>{

    public List<AtteArchiveMonthlyInfoService> getReports(String atteDate, String companyId);


    boolean newReports(String atteDate, String companyId,String userId);

    List<AtteArchiveMonthly> findByYear(String year, String companyId);

    void saveArchive(String archiveDate, String companyId);

    /**
     * 新建报表
     * @param atteDate
     * @param companyId
     * @return
     */
    boolean newReports(String atteDate, String companyId);
}

package com.kun.ihrm.attendance.controller;


import com.kun.ihrm.attendance.entity.AtteArchiveMonthly;
import com.kun.ihrm.attendance.entity.AtteArchiveMonthlyInfo;
import com.kun.ihrm.attendance.pojg.dto.AttendanceDTO;
import com.kun.ihrm.attendance.pojg.dto.ConditionDTO;
import com.kun.ihrm.attendance.service.AtteArchiveMonthlyInfoService;
import com.kun.ihrm.attendance.service.AtteArchiveMonthlyService;
import com.kun.ihrm.attendance.service.AtteAttendanceService;
import com.kun.ihrm.common.controller.BaseController;
import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("attendances")
public class AttendanceController extends BaseController {
//atte_archive_monthly
    @Autowired
    private AtteAttendanceService attendanceService;

    @Resource
    private AtteArchiveMonthlyService archiveMonthlyService;

    @Resource
    private AtteArchiveMonthlyInfoService archiveMonthlyInfoService;


    /**
     * 查询 每个用户 当月的考勤记录
     * @param conditionDTO
     * @return
     * @throws ParseException
     */
    @GetMapping("")
    public Result getList(ConditionDTO conditionDTO) throws ParseException {
        Map<String, Object> map = attendanceService.getPages(companyId, conditionDTO);
        return new Result(ResultCode.SUCCESS, map);
    }

    @PutMapping("/{id}")
    public Result editAtte(@RequestBody AttendanceDTO attendance,@PathVariable("id")String id) {
        attendance.setId(id);
        if (attendanceService.editAtte(attendance, userId)) {
            return new Result(ResultCode.SUCCESS);
        }
        return new Result(ResultCode.FAIL);
    }

    /**
     * 归档历史列表
     */
    @GetMapping(value = "/reports/year")
    public Result findReportsYear(String year) {
        List<AtteArchiveMonthly> list = archiveMonthlyService.findByYear(year, companyId);
        return new Result(ResultCode.SUCCESS, list);
    }
    /**
     * 查询归档详情
     * /reports/1240302168376995800
     */
    @PostMapping(value = "/reports/{id}")
    public Result findInfosById(@PathVariable String id) {
        List<AtteArchiveMonthlyInfo> list = archiveMonthlyInfoService.findMonthInfoByAmid(id);
        return new Result(ResultCode.SUCCESS , list);
    }

    /**
     * 获取月度报表归档数据
     */
    @GetMapping(value = "/reports")
    public Result reports(String atteDate) {
//        List<ArchiveMonthlyInfo> list = atteService.getReports(atteDate , companyId);
        List<AtteArchiveMonthlyInfo> list = archiveMonthlyInfoService.getReports(atteDate, companyId);
        return new Result(ResultCode.SUCCESS , list);
    }

    /**
     * 数据归档
     */
    @GetMapping(value = "/archive/item")
    public Result archive(String archiveDate) {
        archiveMonthlyService.saveArchive(archiveDate , companyId);
        return new Result(ResultCode.SUCCESS);
    }

    /**
     *  新建报表
     */
    @RequestMapping(value = "/newReports" , method = RequestMethod.GET)
    public Result newReports(String atteDate) {
        if (archiveMonthlyService.newReports(atteDate , companyId)){
            return new Result(ResultCode.SUCCESS);
        }
        return new Result(ResultCode.FAIL);
    }




}

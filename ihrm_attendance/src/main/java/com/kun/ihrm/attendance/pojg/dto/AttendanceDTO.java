package com.kun.ihrm.attendance.pojg.dto;

/**
 * 修改 用户 考勤信息
 */

public class AttendanceDTO {
    /**
     * ID
     */
    private String Id;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 用于判断该用户是否再今天考勤，如果为空者缺勤
     */
    private String day;
    /**
     * 考情状态
     */
    private Integer adtStatu;
    /**
     * 部门 ID
     */
    private String departmentId;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getAdtStatu() {
        return adtStatu;
    }

    public void setAdtStatu(Integer adtStatu) {
        this.adtStatu = adtStatu;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public AttendanceDTO() {
    }
}

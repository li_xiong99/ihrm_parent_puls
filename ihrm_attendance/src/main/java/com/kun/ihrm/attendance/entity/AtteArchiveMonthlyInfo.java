package com.kun.ihrm.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;
import java.util.Map;


/**
 * 考勤归档信息详情表实体类
 *
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @version 1.0.0
 * @packageName com.ihrm.atte.copy
 * @fileName AtteArchiveMonthlyInfo.java
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 */


@TableName("atte_archive_monthly_info")
public class AtteArchiveMonthlyInfo {

    public AtteArchiveMonthlyInfo(BsUser user) {
        this.userId = user.getId();
        this.name = user.getUsername();
        this.workNumber = user.getWorkNumber();
        this.department = user.getDepartmentName();
        this.mobile = user.getMobile();
    }

    public void setStatisData(Map map) {
        this.normalDays = map.get("at1").toString();
        this.absenceDays = map.get("at2").toString();
        this.laterTimes = map.get("at3").toString();
        this.earlyTimes = map.get("at4").toString();
        this.leaveDays = map.get("at8").toString();
        this.dayOffLeaveDays = map.get("at17").toString();
        //平均工作日21.75
        this.workingDays="21.75";
        //是否全勤
        this.isFullAttendanceint = Integer.parseInt(this.normalDays)>=21.75?0:1;
        //出勤天数 = 正常 + 早退 + 迟到
        this.actualAtteOfficialDays = Integer.parseInt(this.laterTimes) +
                Integer.parseInt(this.normalDays) +
                Integer.parseInt(this.earlyTimes) + "";
        //出勤天数 = 计薪天数
        this.salaryOfficialDays = this.actualAtteOfficialDays;

    }

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;
    /**
     * 归档id
     */
    @TableField("atte_archive_monthly_id")
    private Long atteArchiveMonthlyId;
    /**
     * 姓名
     */
    @TableField("name")
    private String name;
    /**
     * 工号
     */
    @TableField("work_number")
    private String workNumber;
    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;
    /**
     * 当月考勤方案
     */
    @TableField("atte_solution")
    private String atteSolution;
    /**
     * 一级部门
     */
    @TableField("department")
    private String department;
    /**
     * 工作城市
     */
    @TableField("work_city")
    private String workCity;
    /**
     * 年假天数
     */
    @TableField("year_leave_days")
    private String yearLeaveDays;
    /**
     * 事假天数
     */
    @TableField("leave_days")
    private String leaveDays;
    /**
     * 病假天数
     */
    @TableField("sick_leave_days")
    private String sickLeaveDays;
    /**
     * 长期病假天数
     */
    @TableField("long_sick_leave_days")
    private String longSickLeaveDays;
    /**
     * 婚假天数
     */
    @TableField("marraiage_leave_days")
    private String marraiageLeaveDays;
    /**
     * 丧假天数
     */
    @TableField("funeral_leave_days")
    private String funeralLeaveDays;
    /**
     * 产假天数
     */
    @TableField("maternity_leave_days")
    private String maternityLeaveDays;
    /**
     * 奖励产假天数
     */
    @TableField("reward_maternity_leave_days")
    private String rewardMaternityLeaveDays;
    /**
     * 陪产假天数
     */
    @TableField("paternity_leave_days")
    private String paternityLeaveDays;
    /**
     * 探亲假天数
     */
    @TableField("home_leava_days")
    private String homeLeavaDays;
    /**
     * 工伤假
     */
    @TableField("accidential_leave_days")
    private String accidentialLeaveDays;
    /**
     * 调休天数
     */
    @TableField("day_Off_leave_days")
    private String dayOffLeaveDays;
    /**
     * 产检假天数
     */
    @TableField("doctor_Off_leave_days")
    private String doctorOffLeaveDays;
    /**
     * 流产假天数
     */
    @TableField("abortion_leave_days")
    private String abortionLeaveDays;
    /**
     * 正常天数
     */
    @TableField("normal_days")
    private String normalDays;
    /**
     * 外出天数
     */
    @TableField("outgoing_days")
    private String outgoingDays;
    /**
     * 出差天数
     */
    @TableField("on_business_days")
    private String onBusinessDays;
    /**
     * 迟到次数
     */
    @TableField("later_times")
    private String laterTimes;
    /**
     * 早退次数
     */
    @TableField("early_times")
    private String earlyTimes;
    /**
     * 迟到次数
     */
    @TableField("signed_times")
    private Integer signedTimes;
    /**
     * 日均时长（自然日）
     */
    @TableField("hours_per_days")
    private String hoursPerDays;
    /**
     * 日均时长(工作日)
     */
    @TableField("hours_per_work_day")
    private String hoursPerWorkDay;
    /**
     * 日均时长（休息日）
     */
    @TableField("hours_per_rest_day")
    private String hoursPerRestDay;
    /**
     * 打卡率
     */
    @TableField("clock_rate")
    private String clockRate;
    /**
     * 旷工天数
     */
    @TableField("absence_days")
    private String absenceDays;
    /**
     * 是否全勤0全勤1非全勤
     */
    @TableField("is_full_attendanceint")
    private int isFullAttendanceint;
    /**
     * 实际出勤天数（非正式）
     */
    @TableField("actual_atte_unofficial_days")
    private String actualAtteUnofficialDays;
    /**
     * 实际出勤天数（正式）
     */
    @TableField("actual_atte_official_days")
    private String actualAtteOfficialDays;
    /**
     * 应出勤工作日
     */
    @TableField("working_days")
    private String workingDays;
    /**
     * 计薪标准
     */
    @TableField("salary_standards")
    private String salaryStandards;
    /**
     * 计薪天数调整
     */
    @TableField("salary_adjustment_days")
    private String salaryAdjustmentDays;
    /**
     * 工作时长
     */
    @TableField("work_hour")
    private String workHour;
    /**
     * 计薪天数（非正式）
     */
    @TableField("salary_unofficial_days")
    private String salaryUnofficialDays;
    /**
     * 计薪天数（正式）
     */
    @TableField("salary_official_days")
    private String salaryOfficialDays;

    @TableField("create_by")
    private String createBy;

    @TableField("create_date")
    private Date createDate;

    @TableField("update_by")
    private String updateBy;

  @TableField("update_date")
  private Date updateDate;

    @TableField("remarks")
    private String remarks;

    @TableField("user_id")
    private String userId;

    @TableField("archive_date")
    private String archiveDate;


    public AtteArchiveMonthlyInfo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getAtteArchiveMonthlyId() {
        return atteArchiveMonthlyId;
    }

    public void setAtteArchiveMonthlyId(Long atteArchiveMonthlyId) {
        this.atteArchiveMonthlyId = atteArchiveMonthlyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAtteSolution() {
        return atteSolution;
    }

    public void setAtteSolution(String atteSolution) {
        this.atteSolution = atteSolution;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getWorkCity() {
        return workCity;
    }

    public void setWorkCity(String workCity) {
        this.workCity = workCity;
    }

    public String getYearLeaveDays() {
        return yearLeaveDays;
    }

    public void setYearLeaveDays(String yearLeaveDays) {
        this.yearLeaveDays = yearLeaveDays;
    }

    public String getLeaveDays() {
        return leaveDays;
    }

    public void setLeaveDays(String leaveDays) {
        this.leaveDays = leaveDays;
    }

    public String getSickLeaveDays() {
        return sickLeaveDays;
    }

    public void setSickLeaveDays(String sickLeaveDays) {
        this.sickLeaveDays = sickLeaveDays;
    }

    public String getLongSickLeaveDays() {
        return longSickLeaveDays;
    }

    public void setLongSickLeaveDays(String longSickLeaveDays) {
        this.longSickLeaveDays = longSickLeaveDays;
    }

    public String getMarraiageLeaveDays() {
        return marraiageLeaveDays;
    }

    public void setMarraiageLeaveDays(String marraiageLeaveDays) {
        this.marraiageLeaveDays = marraiageLeaveDays;
    }

    public String getFuneralLeaveDays() {
        return funeralLeaveDays;
    }

    public void setFuneralLeaveDays(String funeralLeaveDays) {
        this.funeralLeaveDays = funeralLeaveDays;
    }

    public String getMaternityLeaveDays() {
        return maternityLeaveDays;
    }

    public void setMaternityLeaveDays(String maternityLeaveDays) {
        this.maternityLeaveDays = maternityLeaveDays;
    }

    public String getRewardMaternityLeaveDays() {
        return rewardMaternityLeaveDays;
    }

    public void setRewardMaternityLeaveDays(String rewardMaternityLeaveDays) {
        this.rewardMaternityLeaveDays = rewardMaternityLeaveDays;
    }

    public String getPaternityLeaveDays() {
        return paternityLeaveDays;
    }

    public void setPaternityLeaveDays(String paternityLeaveDays) {
        this.paternityLeaveDays = paternityLeaveDays;
    }

    public String getHomeLeavaDays() {
        return homeLeavaDays;
    }

    public void setHomeLeavaDays(String homeLeavaDays) {
        this.homeLeavaDays = homeLeavaDays;
    }

    public String getAccidentialLeaveDays() {
        return accidentialLeaveDays;
    }

    public void setAccidentialLeaveDays(String accidentialLeaveDays) {
        this.accidentialLeaveDays = accidentialLeaveDays;
    }

    public String getDayOffLeaveDays() {
        return dayOffLeaveDays;
    }

    public void setDayOffLeaveDays(String dayOffLeaveDays) {
        this.dayOffLeaveDays = dayOffLeaveDays;
    }

    public String getDoctorOffLeaveDays() {
        return doctorOffLeaveDays;
    }

    public void setDoctorOffLeaveDays(String doctorOffLeaveDays) {
        this.doctorOffLeaveDays = doctorOffLeaveDays;
    }

    public String getAbortionLeaveDays() {
        return abortionLeaveDays;
    }

    public void setAbortionLeaveDays(String abortionLeaveDays) {
        this.abortionLeaveDays = abortionLeaveDays;
    }

    public String getNormalDays() {
        return normalDays;
    }

    public void setNormalDays(String normalDays) {
        this.normalDays = normalDays;
    }

    public String getOutgoingDays() {
        return outgoingDays;
    }

    public void setOutgoingDays(String outgoingDays) {
        this.outgoingDays = outgoingDays;
    }

    public String getOnBusinessDays() {
        return onBusinessDays;
    }

    public void setOnBusinessDays(String onBusinessDays) {
        this.onBusinessDays = onBusinessDays;
    }

    public String getLaterTimes() {
        return laterTimes;
    }

    public void setLaterTimes(String laterTimes) {
        this.laterTimes = laterTimes;
    }

    public String getEarlyTimes() {
        return earlyTimes;
    }

    public void setEarlyTimes(String earlyTimes) {
        this.earlyTimes = earlyTimes;
    }

    public Integer getSignedTimes() {
        return signedTimes;
    }

    public void setSignedTimes(Integer signedTimes) {
        this.signedTimes = signedTimes;
    }

    public String getHoursPerDays() {
        return hoursPerDays;
    }

    public void setHoursPerDays(String hoursPerDays) {
        this.hoursPerDays = hoursPerDays;
    }

    public String getHoursPerWorkDay() {
        return hoursPerWorkDay;
    }

    public void setHoursPerWorkDay(String hoursPerWorkDay) {
        this.hoursPerWorkDay = hoursPerWorkDay;
    }

    public String getHoursPerRestDay() {
        return hoursPerRestDay;
    }

    public void setHoursPerRestDay(String hoursPerRestDay) {
        this.hoursPerRestDay = hoursPerRestDay;
    }

    public String getClockRate() {
        return clockRate;
    }

    public void setClockRate(String clockRate) {
        this.clockRate = clockRate;
    }

    public String getAbsenceDays() {
        return absenceDays;
    }

    public void setAbsenceDays(String absenceDays) {
        this.absenceDays = absenceDays;
    }

    public int getIsFullAttendanceint() {
        return isFullAttendanceint;
    }

    public void setIsFullAttendanceint(int isFullAttendanceint) {
        this.isFullAttendanceint = isFullAttendanceint;
    }

    public String getActualAtteUnofficialDays() {
        return actualAtteUnofficialDays;
    }

    public void setActualAtteUnofficialDays(String actualAtteUnofficialDays) {
        this.actualAtteUnofficialDays = actualAtteUnofficialDays;
    }

    public String getActualAtteOfficialDays() {
        return actualAtteOfficialDays;
    }

    public void setActualAtteOfficialDays(String actualAtteOfficialDays) {
        this.actualAtteOfficialDays = actualAtteOfficialDays;
    }

    public String getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(String workingDays) {
        this.workingDays = workingDays;
    }

    public String getSalaryStandards() {
        return salaryStandards;
    }

    public void setSalaryStandards(String salaryStandards) {
        this.salaryStandards = salaryStandards;
    }

    public String getSalaryAdjustmentDays() {
        return salaryAdjustmentDays;
    }

    public void setSalaryAdjustmentDays(String salaryAdjustmentDays) {
        this.salaryAdjustmentDays = salaryAdjustmentDays;
    }

    public String getWorkHour() {
        return workHour;
    }

    public void setWorkHour(String workHour) {
        this.workHour = workHour;
    }

    public String getSalaryUnofficialDays() {
        return salaryUnofficialDays;
    }

    public void setSalaryUnofficialDays(String salaryUnofficialDays) {
        this.salaryUnofficialDays = salaryUnofficialDays;
    }

    public String getSalaryOfficialDays() {
        return salaryOfficialDays;
    }

    public void setSalaryOfficialDays(String salaryOfficialDays) {
        this.salaryOfficialDays = salaryOfficialDays;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getArchiveDate() {
        return archiveDate;
    }

    public void setArchiveDate(String archiveDate) {
        this.archiveDate = archiveDate;
    }
}

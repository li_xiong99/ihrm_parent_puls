package com.kun.ihrm.attendance.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.attendance.entity.AtteAttendance;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 考勤表 Mapper接口
 * @packageName com.ihrm.atte.copy
 * @fileName AtteAttendanceMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:43
 * @lastModify 2023年8月24日 上午9:50:43
 * @version 1.0.0
 */
public interface AtteAttendanceMapper extends BaseMapper<AtteAttendance> {
    Map<String,String> statisByUser(@Param("userId") String userId, @Param("s") String s);
}

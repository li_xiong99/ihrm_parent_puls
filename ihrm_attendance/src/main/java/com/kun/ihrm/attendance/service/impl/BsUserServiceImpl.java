package com.kun.ihrm.attendance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.attendance.entity.BsUser;
import com.kun.ihrm.attendance.mapper.BsUserMapper;
import com.kun.ihrm.attendance.service.BsUserService;
import com.kun.ihrm.common.utils.PageUtils;
import com.kun.ihrm.common.utils.Query;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * BsUserService接口实现
 * @packageName com.ihrm.atte.copy
 * @fileName BsUserServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */
@Service
public class BsUserServiceImpl 
				extends ServiceImpl<BsUserMapper, BsUser>
				implements BsUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BsUserServiceImpl.class);


    @Override
    public PageUtils getPages(String companyId, Map<String, Object> map) {
        List<String> list = (List) map.get("departmentChecks");
        String keyword = (String)map.get("keyword");
        QueryWrapper<BsUser> qw = new QueryWrapper<>();
        if (!StringUtils.isEmpty(keyword)){
            qw.like("username", keyword);
        }
        if (!StringUtils.isEmpty(companyId)){
            qw.like("company_id", companyId);
        }
        if (list!=null){
            qw.and(wrapper ->{
                for (String value : list) {
                    wrapper.or().eq("a", value);
                }
            });
        }

        IPage<BsUser> page = this.page(
                new Query<BsUser>().getPage(map),
                qw
        );
        return new PageUtils(page);
    }
}

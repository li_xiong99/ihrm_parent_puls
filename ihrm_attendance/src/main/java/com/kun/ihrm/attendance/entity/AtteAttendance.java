package com.kun.ihrm.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;


/**
 * 考勤表实体类
 * @packageName com.ihrm.atte.copy
 * @fileName AtteAttendance.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:43
 * @lastModify 2023年8月24日 上午9:50:43
 * @version 1.0.0
 */

@TableName("atte_attendance")
public class AtteAttendance {

  /** 
   * 主键ID
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 用户ID
   */
  @TableField("user_id")
  private String userId;
  /** 
   * 公司ID
   */
  @TableField("company_id")
  private String companyId;
  /** 
   * 部门ID
   */
  @TableField("department_id")
  private String departmentId;
  /** 
   * 考情状态 1正常2旷工3迟到4早退5外出6出差7年假8事假9病假10婚假11丧假12产假13奖励产假14陪产假15探亲假16工伤假17调休18产检假19流产假20长期病假21测试架22补签23休息
   */
  @TableField("adt_statu")
  private Integer adtStatu;
  /** 
   * 职位状态 1在职2离职
   */
  @TableField("job_statu")
  private Integer jobStatu;
  /** 
   * 上班考勤时间
   */
  @TableField("adt_in_time")
  private Date adtInTime;
  /** 
   * 考勤地点
   */
  @TableField("adt_in_place")
  private String adtInPlace;
  /** 
   * 考勤办公室
   */
  @TableField("adt_in_hourse")
  private String adtInHourse;
  /** 
   * 考勤坐标
   */
  @TableField("adt_in_coordinate")
  private String adtInCoordinate;
  /** 
   * 下班考勤时间
   */
  @TableField("adt_out_time")
  private Date adtOutTime;
  /** 
   * 下班考情地点
   */
  @TableField("adt_out_place")
  private String adtOutPlace;
  /** 
   * 考勤办公室
   */
  @TableField("adt_out_hourse")
  private String adtOutHourse;

  @TableField("create_by")
  private String createBy;

  @TableField("create_date")
  private Date createDate;

  @TableField("update_by")
  private String updateBy;

  @TableField("update_date")
  private Date updateDate;

  @TableField("remarks")
  private String remarks;

  @TableField("username")
  private String username;

  @TableField("mobile")
  private String mobile;

  @TableField("department_name")
  private String departmentName;

  @TableField("day")
  private String day;


  public AtteAttendance() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getDepartmentId() {
    return departmentId;
  }

  public void setDepartmentId(String departmentId) {
    this.departmentId = departmentId;
  }

  public Integer getAdtStatu() {
    return adtStatu;
  }

  public void setAdtStatu(Integer adtStatu) {
    this.adtStatu = adtStatu;
  }

  public Integer getJobStatu() {
    return jobStatu;
  }

  public void setJobStatu(Integer jobStatu) {
    this.jobStatu = jobStatu;
  }

  public Date getAdtInTime() {
    return adtInTime;
  }

  public void setAdtInTime(Date adtInTime) {
    this.adtInTime = adtInTime;
  }

  public String getAdtInPlace() {
    return adtInPlace;
  }

  public void setAdtInPlace(String adtInPlace) {
    this.adtInPlace = adtInPlace;
  }

  public String getAdtInHourse() {
    return adtInHourse;
  }

  public void setAdtInHourse(String adtInHourse) {
    this.adtInHourse = adtInHourse;
  }

  public String getAdtInCoordinate() {
    return adtInCoordinate;
  }

  public void setAdtInCoordinate(String adtInCoordinate) {
    this.adtInCoordinate = adtInCoordinate;
  }

  public Date getAdtOutTime() {
    return adtOutTime;
  }

  public void setAdtOutTime(Date adtOutTime) {
    this.adtOutTime = adtOutTime;
  }

  public String getAdtOutPlace() {
    return adtOutPlace;
  }

  public void setAdtOutPlace(String adtOutPlace) {
    this.adtOutPlace = adtOutPlace;
  }

  public String getAdtOutHourse() {
    return adtOutHourse;
  }

  public void setAdtOutHourse(String adtOutHourse) {
    this.adtOutHourse = adtOutHourse;
  }

  public String getCreateBy() {
    return createBy;
  }

  public void setCreateBy(String createBy) {
    this.createBy = createBy;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public String getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(String updateBy) {
    this.updateBy = updateBy;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getDepartmentName() {
    return departmentName;
  }

  public void setDepartmentName(String departmentName) {
    this.departmentName = departmentName;
  }

  public String getDay() {
    return day;
  }

  public void setDay(String day) {
    this.day = day;
  }
}

package com.kun.ihrm.attendance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.attendance.entity.AtteArchiveMonthlyInfo;

import java.util.List;


/**
 * 考勤归档信息详情表 Service接口
 * @packageName com.ihrm.atte.copy
 * @fileName AtteArchiveMonthlyInfoService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */
public interface AtteArchiveMonthlyInfoService extends IService<AtteArchiveMonthlyInfo>{

    /**
     * 根据 公司ID 查询 公司员工 再根据 公司员工 和日期 循环查询 员工 考勤详情信息
     * @param atteDate
     * @param companyId
     * @return
     */
    List<AtteArchiveMonthlyInfo> getReports(String atteDate, String companyId);

    /**
     * 根据归档列表查询月归档详情
     *
     * @param atteArchiveMonthlyId atte_archive_monthly_id
     * @return 月归档记录
     */
    List<AtteArchiveMonthlyInfo> findMonthInfoByAmid(String atteArchiveMonthlyId);
}

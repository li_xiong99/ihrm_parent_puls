package com.kun.ihrm.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;


/**
 * 考勤归档信息表实体类
 * @packageName com.ihrm.atte.copy
 * @fileName AtteArchiveMonthly.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */

@TableName("atte_archive_monthly")
public class AtteArchiveMonthly {


  /** 
   * 主键ID
   */
  @TableId(value = "id", type = IdType.INPUT)
  private Long id;
  /** 
   * 公司ID
   */
  @TableField("company_id")
  private String companyId;
  /** 
   * 部门ID
   */
  @TableField("department_id")
  private String departmentId;
  /** 
   * 归档年份
   */
  @TableField("archive_year")
  private String archiveYear;
  /** 
   * 归档月份
   */
  @TableField("archive_month")
  private String archiveMonth;
  /** 
   * 总人数
   */
  @TableField("total_people_num")
  private Integer totalPeopleNum;
  /** 
   * 全勤人数
   */
  @TableField("full_atte_people_num")
  private Integer fullAttePeopleNum;
  /** 
   * 是否归档(0已经归档1没有归档)
   */
  @TableField("is_archived")
  private Integer isArchived;
    /**
     * 创建人（id）
     */
  @TableField("create_by")
  private String createBy;
    /**
     * 创建时间
     */
  @TableField("create_date")
  private Date createDate;
    /**
     * 修改人（ID）
     */
  @TableField("update_by")
  private String updateBy;
    /**
     * 修改时间
     */
  @TableField("update_date")
  private Date updateDate;
    /**
     * 备注
     */
  @TableField("remarks")
  private String remarks;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getDepartmentId() {
    return departmentId;
  }

  public void setDepartmentId(String departmentId) {
    this.departmentId = departmentId;
  }

  public String getArchiveYear() {
    return archiveYear;
  }

  public void setArchiveYear(String archiveYear) {
    this.archiveYear = archiveYear;
  }

  public String getArchiveMonth() {
    return archiveMonth;
  }

  public void setArchiveMonth(String archiveMonth) {
    this.archiveMonth = archiveMonth;
  }

  public Integer getTotalPeopleNum() {
    return totalPeopleNum;
  }

  public void setTotalPeopleNum(Integer totalPeopleNum) {
    this.totalPeopleNum = totalPeopleNum;
  }

  public Integer getFullAttePeopleNum() {
    return fullAttePeopleNum;
  }

  public void setFullAttePeopleNum(Integer fullAttePeopleNum) {
    this.fullAttePeopleNum = fullAttePeopleNum;
  }

  public Integer getIsArchived() {
    return isArchived;
  }

  public void setIsArchived(Integer isArchived) {
    this.isArchived = isArchived;
  }

  public String getCreateBy() {
    return createBy;
  }

  public void setCreateBy(String createBy) {
    this.createBy = createBy;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public String getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(String updateBy) {
    this.updateBy = updateBy;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  public AtteArchiveMonthly() {
  }
}

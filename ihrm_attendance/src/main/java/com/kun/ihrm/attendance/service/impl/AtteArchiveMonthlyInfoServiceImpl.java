package com.kun.ihrm.attendance.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.attendance.entity.AtteArchiveMonthlyInfo;
import com.kun.ihrm.attendance.entity.BsUser;
import com.kun.ihrm.attendance.mapper.AtteArchiveMonthlyInfoMapper;
import com.kun.ihrm.attendance.service.AtteArchiveMonthlyInfoService;
import com.kun.ihrm.attendance.service.AtteAttendanceService;
import com.kun.ihrm.attendance.service.BsUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 考勤归档信息详情表 Service接口实现
 * @packageName com.ihrm.atte.copy
 * @fileName AtteArchiveMonthlyInfoServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */
@Service
public class AtteArchiveMonthlyInfoServiceImpl 
				extends ServiceImpl<AtteArchiveMonthlyInfoMapper, AtteArchiveMonthlyInfo>
				implements AtteArchiveMonthlyInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AtteArchiveMonthlyInfoServiceImpl.class);

    @Resource
    private BsUserService userService;

    @Resource
    private AtteAttendanceService attendanceService;

    @Override
    public List<AtteArchiveMonthlyInfo> getReports(String atteDate, String companyId) {
        List<BsUser> users = userService.list(
                new QueryWrapper<BsUser>().eq("company_id", companyId)
        );
        List<AtteArchiveMonthlyInfo> list = new ArrayList<>();
        for (BsUser user : users) {
            AtteArchiveMonthlyInfo info = new AtteArchiveMonthlyInfo(user);
            //统计每个用户的考勤数量
            Map<String, String> map = attendanceService.statisByUser(user.getId(), atteDate + "%");
            info.setStatisData(map);
            list.add(info);
        }
        return list;
    }

    @Override
    public List<AtteArchiveMonthlyInfo> findMonthInfoByAmid(String atteArchiveMonthlyId) {
        return this.list(new QueryWrapper<AtteArchiveMonthlyInfo>().eq("atte_archive_monthly_id", atteArchiveMonthlyId));
    }
}

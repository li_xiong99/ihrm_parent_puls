package com.kun.ihrm.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 考勤-企业设置信息实体类
 * @packageName com.ihrm.atte

 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午9:41:01
 * @lastModify 2023年8月25日 上午9:41:01
 * @version 1.0.0
 */

@TableName("atte_company_settings")
public class AtteCompanySettings {

  /** 
   * 企业id
   */
  @TableId(value = "company_id", type = IdType.AUTO)
  private String companyId;
  /** 
   * 0是未设置，1是已设置
   */
  @TableField("is_settings")
  private Integer isSettings;
  /** 
   * 当前显示报表月份
   */
  @TableField("data_month")
  private String dataMonth;

  public AtteCompanySettings() {
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public Integer getIsSettings() {
    return isSettings;
  }

  public void setIsSettings(Integer isSettings) {
    this.isSettings = isSettings;
  }

  public String getDataMonth() {
    return dataMonth;
  }

  public void setDataMonth(String dataMonth) {
    this.dataMonth = dataMonth;
  }
}

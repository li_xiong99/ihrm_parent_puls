package com.kun.ihrm.attendance.config;


import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * MyBatis-Plus配置类
 * @packageName com.ihrm.atte.config;
 * @fileName MyBatisPlusConfig.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午9:41:01
 * @lastModify 2023年8月25日 上午9:41:01
 * @version 1.0.0
 */
@Configuration

public class MyBatisPlusConfig {
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        return new PaginationInterceptor();
    }
}

package com.kun.ihrm.attendance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.attendance.entity.AtteCompanySettings;


/**
 * 考勤-企业设置信息 Service接口
 * @packageName com.ihrm.atte
 * @fileName AtteCompanySettingsService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午9:41:01
 * @lastModify 2023年8月25日 上午9:41:01
 * @version 1.0.0
 */
public interface AtteCompanySettingsService extends IService<AtteCompanySettings>{

}

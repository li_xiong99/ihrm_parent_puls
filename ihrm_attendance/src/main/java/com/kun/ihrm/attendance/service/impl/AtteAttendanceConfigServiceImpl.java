package com.kun.ihrm.attendance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.attendance.entity.AtteAttendanceConfig;
import com.kun.ihrm.attendance.mapper.AtteAttendanceConfigMapper;
import com.kun.ihrm.attendance.service.AtteAttendanceConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 考勤配置表 Service接口实现
 * @packageName com.ihrm.atte.copy
 * @fileName AtteAttendanceConfigServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */
@Service
public class AtteAttendanceConfigServiceImpl 
				extends ServiceImpl<AtteAttendanceConfigMapper, AtteAttendanceConfig>
				implements AtteAttendanceConfigService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AtteAttendanceConfigServiceImpl.class);

    @Override
    public void saveAtteConfig(AtteAttendanceConfig ac) {
        this.save(ac);
    }

    @Override
    public AtteAttendanceConfig getAtteConfig(String companyId, String departmentId) {
        return this.getOne(new QueryWrapper<AtteAttendanceConfig>().eq("company_id", companyId).eq("department_id", departmentId));
    }
}

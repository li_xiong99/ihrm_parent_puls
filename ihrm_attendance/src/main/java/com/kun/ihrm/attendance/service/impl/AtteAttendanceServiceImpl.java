package com.kun.ihrm.attendance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.attendance.entity.AtteAttendance;
import com.kun.ihrm.attendance.entity.AtteCompanySettings;
import com.kun.ihrm.attendance.entity.BsUser;
import com.kun.ihrm.attendance.mapper.AtteAttendanceMapper;
import com.kun.ihrm.attendance.pojg.dto.AttendanceDTO;
import com.kun.ihrm.attendance.pojg.dto.ConditionDTO;
import com.kun.ihrm.attendance.pojg.vo.AtteArchiveMonthlyInfoVO;
import com.kun.ihrm.attendance.service.AtteAttendanceService;
import com.kun.ihrm.attendance.service.AtteCompanySettingsService;
import com.kun.ihrm.attendance.service.BsUserService;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.common.utils.BeanMapUtils;
import com.kun.ihrm.common.utils.DateUtil;
import com.kun.ihrm.common.utils.IdWorker;
import com.kun.ihrm.common.utils.PageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;

/**
 * 考勤表 Service接口实现
 *
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @version 1.0.0
 * @packageName com.ihrm.atte.copy
 * @fileName AtteAttendanceServiceImpl.java
 * @createTime 2023年8月24日 上午9:50:43
 * @lastModify 2023年8月24日 上午9:50:43
 */
@Service
public class AtteAttendanceServiceImpl
        extends ServiceImpl<AtteAttendanceMapper, AtteAttendance>
        implements AtteAttendanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AtteAttendanceServiceImpl.class);

    @Resource
    private AtteCompanySettingsService companySettingsService;
    @Resource
    private BsUserService bsUserService;
    @Resource
    private IdWorker idWorker;
    @Resource
    private AtteAttendanceMapper attendanceMapper;


    @Override
    public Map<String, Object> getPages(String companyId, ConditionDTO conditionDTO) throws ParseException {
        // 根据 公司 ID 和 年月份  先查询  是否 拥有考勤记录
        AtteCompanySettings companySettings = companySettingsService.getOne(new QueryWrapper<AtteCompanySettings>().eq("company_id", companyId));
        if (companySettings == null) {
            return null;
        }
        // 用 年月份 查询 当月考勤记录
        String dataMonth = companySettings.getDataMonth();
        // 调用 根据 公司ID 查询 公司员工方法
        PageUtils pages = bsUserService.getPages(companyId, BeanMapUtils.beanToMap(conditionDTO));
        List<BsUser> pagesList = (List<BsUser>) pages.getList();
        //声明存放 AtteArchiveMonthlyInfoVO 集合
        List<AtteArchiveMonthlyInfoVO> list = new ArrayList<>();
        //循环
        for (BsUser user : pagesList) {
            AtteArchiveMonthlyInfoVO vo = new AtteArchiveMonthlyInfoVO();
            BeanUtils.copyProperties(user, vo);
            List<AtteAttendance> attendances = new ArrayList<>();
            //获取当前月所有的天数
            String[] days = DateUtil.getDaysByYearMonth(dataMonth);
            for (String day : days) {
                //根据 user 对象的 id 查询 考勤记录
                AtteAttendance attendance = this.getOne(new QueryWrapper<AtteAttendance>()
                        .eq("user_id", user.getId())
                        .eq("day", day)
                );

                //如果 考勤记录为空 说明缺勤
                if (attendance == null) {
                    attendance = new AtteAttendance();
                    //设置为旷工
                    attendance.setId(idWorker.nextId() + "");
                    attendance.setUserId(user.getId());
                    attendance.setCompanyId(companyId);
                    attendance.setAdtStatu(2);
                    attendance.setCreateDate(new Date());
                }
                attendances.add(attendance);
            }
            vo.setAttendanceRecord(attendances);
            list.add(vo);
        }
        Map<String, Object> map = new HashMap<>(3);

        //分页对象数据
        PageResult<AtteArchiveMonthlyInfoVO> pr = new PageResult<>((long) list.size(), list);
        //待处理的考勤数据
        map.put("data", pr);
        //待处理的考勤数量
        map.put("tobeTaskCount", 0);
        //当前的考勤月份
        int month = Integer.parseInt(dataMonth.substring(4));
        map.put("monthOfReport", month);
        return map;
    }

    @Override
    public Map<String, String> statisByUser(String id, String s) {
        return attendanceMapper.statisByUser(id, s);
    }

    @Override
    public boolean editAtte(AttendanceDTO attendance, String userId) {

        //查询 考勤记录 是否存在
        AtteAttendance ae = this.getOne(new QueryWrapper<AtteAttendance>()
                .eq("user_id", attendance.getUserId())
                .eq("day", attendance.getDay())
        );
        if (ae == null) {
            //TODO 业务逻辑暂时不清晰 如果不存在,需要设置id
//            ae.setId(idWorker.nextId() + "");
//            ae.setCreateBy(userId);
//            ae.setCreateDate(new Date());
//            return this.save(ae);
            return  false;
        }else {
            ae.setId(attendance.getId());
            ae.setUserId(attendance.getUserId());
            ae.setDepartmentId(attendance.getDepartmentId());
            ae.setAdtStatu(attendance.getAdtStatu());
            ae.setJobStatu(1);
            ae.setDay(attendance.getDay());
            ae.setUpdateBy(userId);
            ae.setUpdateDate(new Date());
            return this.update(ae,new QueryWrapper<AtteAttendance>()
                    .eq("user_id", ae.getUserId())
                    .eq("day", ae.getDay())
            );
        }
    }
}

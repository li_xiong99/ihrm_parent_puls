package com.kun.ihrm.attendance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.attendance.entity.BsUser;
import com.kun.ihrm.common.utils.PageUtils;

import java.util.Map;


/**
 * BsUserService接口
 * @packageName com.ihrm.atte.copy
 * @fileName BsUserService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */
public interface BsUserService extends IService<BsUser>{

    /**
     * 查询企业id下面的所有用户信息
     *
     * @param companyId 企业id
     * @param map
     * @return 企业id下面的所有用户信息
     */
    PageUtils getPages(String companyId, Map<String, Object> map);
}

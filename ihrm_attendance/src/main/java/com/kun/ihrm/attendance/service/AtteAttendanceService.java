package com.kun.ihrm.attendance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.attendance.entity.AtteAttendance;
import com.kun.ihrm.attendance.pojg.dto.AttendanceDTO;
import com.kun.ihrm.attendance.pojg.dto.ConditionDTO;

import java.text.ParseException;
import java.util.Map;


/**
 * 考勤表 Service接口
 * @packageName com.ihrm.atte.copy
 * @fileName AtteAttendanceService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:43
 * @lastModify 2023年8月24日 上午9:50:43
 * @version 1.0.0
 */
public interface AtteAttendanceService extends IService<AtteAttendance>{
    /**
     * 根据 条件 查询 考勤集合
     * @return
     */
    Map<String, Object> getPages(String companyId, ConditionDTO conditionDTO) throws ParseException;

    /**
     * 根据 用户ID 和 年月 修改 考勤状态
     * @param attendanceDTO 考勤信息实体
     * @param userId
     */
    boolean editAtte(AttendanceDTO attendanceDTO, String userId);


    Map<String, String> statisByUser(String id, String s);
}

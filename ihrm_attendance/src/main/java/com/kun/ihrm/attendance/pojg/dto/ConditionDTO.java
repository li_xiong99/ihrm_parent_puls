package com.kun.ihrm.attendance.pojg.dto;

import lombok.Data;

import java.util.List;

@Data
public class ConditionDTO {
    /**
     * 当前页数
     */
    private int page;
    /**
     * 每页显示数据量
     */
    private int pagesize;
    /**
     * 部门 数组
     */
    private List<String> departmentChecks;
    /**
     * 考勤状态 ID
     */
    private int stateID;
    /**
     * 查询框 模糊查询
     */
    private String keyword;
}

package com.kun.ihrm.attendance.pojg.vo;

import com.kun.ihrm.attendance.entity.AtteAttendance;

import java.util.List;


public class AtteArchiveMonthlyInfoVO {
    /**
     * 用户 ID
     */
    String id;
    /**
     * 部门 ID
     */
    String departmentId;
    /**
     * 部门名称
     */
    String departmentName;
    /**
     * 手机号码
     */
    String mobile;
    /**
     * 用户名称
     */
    String username;
    /**
     * 工号
     */
    String workNumber;
    /**
     * 该用户 该月的 考勤记录 列表
     */
    List<AtteAttendance> attendanceRecord;

    public AtteArchiveMonthlyInfoVO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public List<AtteAttendance> getAttendanceRecord() {
        return attendanceRecord;
    }

    public void setAttendanceRecord(List<AtteAttendance> attendanceRecord) {
        this.attendanceRecord = attendanceRecord;
    }
}

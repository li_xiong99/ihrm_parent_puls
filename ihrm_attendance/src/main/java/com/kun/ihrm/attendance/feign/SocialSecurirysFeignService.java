package com.kun.ihrm.attendance.feign;

import com.kun.ihrm.common.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("ihrm_social_securirys")//声明远程调用
public interface SocialSecurirysFeignService {

    @GetMapping("/social_securitys/settings")
    public Result settings();

}

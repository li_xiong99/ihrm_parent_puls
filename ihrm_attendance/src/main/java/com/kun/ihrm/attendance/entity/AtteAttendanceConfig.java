package com.kun.ihrm.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;


/**
 * 考勤配置表实体类
 * @packageName com.ihrm.atte.copy
 * @fileName AtteAttendanceConfig.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */

@TableName("atte_attendance_config")
public class AtteAttendanceConfig {

  /** 
   * 主键ID
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 公司ID
   */
  @TableField("company_id")
  private String companyId;
  /** 
   * 部门ID
   */
  @TableField("department_id")
  private String departmentId;
  /** 
   * 上午打卡时间
   */
  @TableField("morning_start_time")
  private Date morningStartTime;
  /** 
   * 上午打卡时间
   */
  @TableField("morning_end_time")
  private Date morningEndTime;
  /** 
   * 下午打卡时间
   */
  @TableField("afternoon_start_time")
  private Date afternoonStartTime;
  /** 
   * 下午打卡时间
   */
  @TableField("afternoon_end_time")
  private Date afternoonEndTime;

  @TableField("create_by")
  private String createBy;

  @TableField("create_date")
  private Date createDate;

  @TableField("update_by")
  private String updateBy;

  @TableField("update_date")
  private Date updateDate;

  @TableField("remarks")
  private String remarks;

  public AtteAttendanceConfig() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getDepartmentId() {
    return departmentId;
  }

  public void setDepartmentId(String departmentId) {
    this.departmentId = departmentId;
  }

  public Date getMorningStartTime() {
    return morningStartTime;
  }

  public void setMorningStartTime(Date morningStartTime) {
    this.morningStartTime = morningStartTime;
  }

  public Date getMorningEndTime() {
    return morningEndTime;
  }

  public void setMorningEndTime(Date morningEndTime) {
    this.morningEndTime = morningEndTime;
  }

  public Date getAfternoonStartTime() {
    return afternoonStartTime;
  }

  public void setAfternoonStartTime(Date afternoonStartTime) {
    this.afternoonStartTime = afternoonStartTime;
  }

  public Date getAfternoonEndTime() {
    return afternoonEndTime;
  }

  public void setAfternoonEndTime(Date afternoonEndTime) {
    this.afternoonEndTime = afternoonEndTime;
  }

  public String getCreateBy() {
    return createBy;
  }

  public void setCreateBy(String createBy) {
    this.createBy = createBy;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public String getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(String updateBy) {
    this.updateBy = updateBy;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }
}

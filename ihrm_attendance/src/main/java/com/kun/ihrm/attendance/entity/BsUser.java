package com.kun.ihrm.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;


/**
 * BsUser实体类
 * @packageName com.ihrm.atte.copy
 * @fileName BsUser.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月24日 上午9:50:44
 * @lastModify 2023年8月24日 上午9:50:44
 * @version 1.0.0
 */

@TableName("bs_user")
public class BsUser {

  /** 
   * ID
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 手机号码
   */
  @TableField("mobile")
  private String mobile;
  /** 
   * 用户名称
   */
  @TableField("username")
  private String username;
  /** 
   * 密码
   */
  @TableField("password")
  private String password;
  /** 
   * 启用状态 0是禁用，1是启用
   */
  @TableField("enable_state")
  private Integer enableState;
  /** 
   * 创建时间
   */
  @TableField("create_time")
  private Date createTime;
  /** 
   * 部门ID
   */
  @TableField("department_id")
  private String departmentId;
  /** 
   * 入职时间
   */
  @TableField("time_of_entry")
  private Date timeOfEntry;
  /** 
   * 聘用形式
   */
  @TableField("form_of_employment")
  private Integer formOfEmployment;
  /** 
   * 工号
   */
  @TableField("work_number")
  private String workNumber;
  /** 
   * 管理形式
   */
  @TableField("form_of_management")
  private String formOfManagement;
  /** 
   * 工作城市
   */
  @TableField("working_city")
  private String workingCity;
  /** 
   * 转正时间
   */
  @TableField("correction_time")
  private Date correctionTime;
  /** 
   * 在职状态 1.在职  2.离职
   */
  @TableField("in_service_status")
  private Integer inServiceStatus;
  /** 
   * 企业ID
   */
  @TableField("company_id")
  private String companyId;

  @TableField("company_name")
  private String companyName;

  @TableField("department_name")
  private String departmentName;
  /** 
   * 用户级别：saasAdmin，coAdmin，user
   */
  @TableField("level")
  private String level;

  @TableField("staff_photo")
  private String staffPhoto;
  /** 
   * 离职时间
   */
  @TableField("time_of_dimission")
  private Date timeOfDimission;


  public BsUser() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Integer getEnableState() {
    return enableState;
  }

  public void setEnableState(Integer enableState) {
    this.enableState = enableState;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getDepartmentId() {
    return departmentId;
  }

  public void setDepartmentId(String departmentId) {
    this.departmentId = departmentId;
  }

  public Date getTimeOfEntry() {
    return timeOfEntry;
  }

  public void setTimeOfEntry(Date timeOfEntry) {
    this.timeOfEntry = timeOfEntry;
  }

  public Integer getFormOfEmployment() {
    return formOfEmployment;
  }

  public void setFormOfEmployment(Integer formOfEmployment) {
    this.formOfEmployment = formOfEmployment;
  }

  public String getWorkNumber() {
    return workNumber;
  }

  public void setWorkNumber(String workNumber) {
    this.workNumber = workNumber;
  }

  public String getFormOfManagement() {
    return formOfManagement;
  }

  public void setFormOfManagement(String formOfManagement) {
    this.formOfManagement = formOfManagement;
  }

  public String getWorkingCity() {
    return workingCity;
  }

  public void setWorkingCity(String workingCity) {
    this.workingCity = workingCity;
  }

  public Date getCorrectionTime() {
    return correctionTime;
  }

  public void setCorrectionTime(Date correctionTime) {
    this.correctionTime = correctionTime;
  }

  public Integer getInServiceStatus() {
    return inServiceStatus;
  }

  public void setInServiceStatus(Integer inServiceStatus) {
    this.inServiceStatus = inServiceStatus;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getDepartmentName() {
    return departmentName;
  }

  public void setDepartmentName(String departmentName) {
    this.departmentName = departmentName;
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public String getStaffPhoto() {
    return staffPhoto;
  }

  public void setStaffPhoto(String staffPhoto) {
    this.staffPhoto = staffPhoto;
  }

  public Date getTimeOfDimission() {
    return timeOfDimission;
  }

  public void setTimeOfDimission(Date timeOfDimission) {
    this.timeOfDimission = timeOfDimission;
  }
}

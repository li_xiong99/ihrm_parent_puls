package com.kun.ihrm.attendance.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.attendance.entity.AtteCompanySettings;
import com.kun.ihrm.attendance.mapper.AtteCompanySettingsMapper;
import com.kun.ihrm.attendance.service.AtteCompanySettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 考勤-企业设置信息 Service接口实现
 * @packageName com.ihrm.atte
 * @fileName AtteCompanySettingsServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午9:41:01
 * @lastModify 2023年8月25日 上午9:41:01
 * @version 1.0.0
 */

@Service
public class AtteCompanySettingsServiceImpl 
				extends ServiceImpl<AtteCompanySettingsMapper, AtteCompanySettings>
				implements AtteCompanySettingsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AtteCompanySettingsServiceImpl.class);



}

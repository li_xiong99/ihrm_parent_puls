package com.kun.ihrm.model.company.response;

import com.kun.ihrm.model.company.CoCompany;
import com.kun.ihrm.model.company.CoDepartment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DeptListResult {

    private String companyId;
    private String companyName;
    private String companyManage;//公司联系人
    private List<CoDepartment> depts;

    public DeptListResult(CoCompany company, List depts) {
        this.companyId = company.getId();
        this.companyName = company.getName();
        this.companyManage = company.getLegalRepresentative();//公司联系人
        this.depts = depts;
    }
}

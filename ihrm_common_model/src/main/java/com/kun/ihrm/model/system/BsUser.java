package com.kun.ihrm.model.system;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.kun.ihrm.model.poi.ExcelAttribute;
import lombok.Data;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * BsUser实体类
 * @packageName system.com.kun.ihrm.copy
 * @fileName BsUser.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月30日 下午7:59:44
 * @lastModify 2023年8月30日 下午7:59:44
 * @version 1.0.0
 */
@Data
@TableName("bs_user")
public class BsUser implements Serializable {

  /**
   * ID
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /**
   * 手机号码
   */
  @TableField("mobile")
  @ExcelAttribute(sort = 2)
  private String mobile;
  /**
   * 用户名称
   */
  @TableField("username")
  @ExcelAttribute(sort = 1)
  private String username;
  /**
   * 密码
   */
  @TableField("password")
  private String password;
  /**
   * 启用状态 0是禁用，1是启用
   */
  @TableField("enable_state")
  private Integer enableState;
  /**
   * 创建时间
   */
  @TableField("create_time")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date createTime;
  /**
   * 部门ID
   */
  @TableField("department_id")
  @ExcelAttribute(sort = 6)
  private String departmentId;
  /**
   * 入职时间
   */
  @TableField("time_of_entry")
  @ExcelAttribute(sort = 5)
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date timeOfEntry;
  /**
   * 聘用形式
   */
  @TableField("form_of_employment")
  @ExcelAttribute(sort = 4)
  private Integer formOfEmployment;
  /**
   * 工号
   */
  @TableField("work_number")
  @ExcelAttribute(sort = 3)
  private String workNumber;
  /**
   * 管理形式
   */
  @TableField("form_of_management")
  private String formOfManagement;
  /**
   * 工作城市
   */
  @TableField("working_city")
  private String workingCity;
  /**
   * 转正时间
   */
  @TableField("correction_time")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date correctionTime;
  /**
   * 在职状态 1.在职  2.离职
   */
  @TableField("in_service_status")
  private Integer inServiceStatus;
  /**
   * 企业ID
   */
  @TableField("company_id")
  private String companyId;

  @TableField("company_name")
  private String companyName;

  @TableField("department_name")
  private String departmentName;
  /**
   * 用户级别：saasAdmin，coAdmin，user
   */
  @TableField("level")
  private String level;

  @TableField("staff_photo")
  private String staffPhoto;
  /**
   * 离职时间
   */
  @TableField("time_of_dimission")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date timeOfDimission;


  @ManyToMany
  @TableField(exist = false)
  @JoinTable(
          name = "pe_user_role",
          joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
          inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
  )

  private Set<PeRole> roles = new HashSet<PeRole>();// 用户与角色   多对多

    public BsUser(){}

  public BsUser(Object [] values) {
    //用户名	手机号	工号	聘用 形式	入职 时间	部门编码
    this.username = values[1].toString();
    this.mobile = values[2].toString();
    this.workNumber = new DecimalFormat("#").format(values[3]);
    this.formOfEmployment =((Double) values[4]).intValue();
    this.timeOfEntry = (Date) values[5];
    this.departmentId = values[6].toString(); //部门编码 != 部门id
  }



}

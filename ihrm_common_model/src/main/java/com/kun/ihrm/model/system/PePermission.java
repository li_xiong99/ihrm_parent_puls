package com.kun.ihrm.model.system;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * PePermission实体类
 * @packageName system.com.kun.ihrm
 * @fileName PePermission.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Data
@TableName("pe_permission")
public class PePermission implements Serializable {

  /**
   * 主键
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /**
   * 权限描述
   */
  @TableField("description")
  private String description;
  /**
   * 权限名称
   */
  @TableField("name")
  private String name;
  /**
   * 权限类型 1为菜单 2为功能 3为API
   */
  @TableField("type")
  private Integer type;
  /**
   * 主键
   */
  @TableField("pid")
  private String pid;
  /**
   * 权限编码
   */
  @TableField("code")
  private String code;
  /**
   * 企业可见性 0：不可见，1：可见
   */
  @TableField("en_visible")
  private Integer enVisible;

  public PePermission(String name, Integer type, String code, String description) {
    this.name = name;
    this.type = type;
    this.code = code;
    this.description = description;
  }

  public PePermission() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getPid() {
    return pid;
  }

  public void setPid(String pid) {
    this.pid = pid;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Integer getEnVisible() {
    return enVisible;
  }

  public void setEnVisible(Integer enVisible) {
    this.enVisible = enVisible;
  }
}

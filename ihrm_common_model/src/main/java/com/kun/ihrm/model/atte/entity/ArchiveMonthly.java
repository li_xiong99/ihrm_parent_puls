package com.kun.ihrm.model.atte.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.kun.ihrm.model.atte.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;


//@Table(name = "atte_archive_monthly")
@EqualsAndHashCode(callSuper = true)
@Data
//@Entity
@NoArgsConstructor
@AllArgsConstructor
@TableName("atte_archive_monthly")
public class ArchiveMonthly extends BaseEntity implements Serializable {

  @TableId(type = IdType.AUTO)
  private String id;
  private String companyId;
  private String departmentId;

  private String archiveYear;
  private String archiveMonth;
  private Integer totalPeopleNum;

  private Integer fullAttePeopleNum;
  private Integer isArchived;
}

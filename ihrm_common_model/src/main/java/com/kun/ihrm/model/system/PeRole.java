package com.kun.ihrm.model.system;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * PeRole实体类
 * @packageName system.com.kun.ihrm
 * @fileName PeRole.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Data
@TableName("pe_role")
public class PeRole implements Serializable {

  /**
   * 主键ID
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /**
   * 权限名称
   */
  @TableField("name")
  private String name;
  /**
   * 说明
   */
  @TableField("description")
  private String description;
  /**
   * 企业id
   */
  @TableField("company_id")
  private String companyId;


    @ManyToMany(mappedBy = "roles")
    @TableField(exist = false)
    private Set<BsUser> users = new HashSet<BsUser>(0);// 角色与用户 多对多


    @ManyToMany
    @TableField(exist = false)
    @JoinTable(
            name = "pe_role_permission",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id")
    )
    private Set<PePermission> permissions = new HashSet<PePermission>(0);// 角色与模块 多对多

}

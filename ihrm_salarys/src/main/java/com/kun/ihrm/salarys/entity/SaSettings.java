package com.kun.ihrm.salarys.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 工资-企业设置信息实体类
 * @packageName com.kun.ihrm
 * @fileName SaSettings.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
@Data
@TableName("sa_settings")
public class SaSettings {

  /** 
   * 企业id
   */
  @TableId(value = "company_id", type = IdType.AUTO)
  private String companyId;
  /** 
   * 对应社保自然月
   */
  @TableField("social_security_type")
  private Integer socialSecurityType;
  /** 
   * 津贴方案名称
   */
  @TableField("subsidy_name")
  private String subsidyName;
  /** 
   * 津贴备注
   */
  @TableField("subsidy_remark")
  private String subsidyRemark;
  /** 
   * 交通补贴计算类型
   */
  @TableField("transportation_subsidy_scheme")
  private Integer transportationSubsidyScheme;
  /** 
   * 交通补贴金额
   */
  @TableField("transportation_subsidy_amount")
  private Double transportationSubsidyAmount;
  /** 
   * 通讯补贴计算类型
   */
  @TableField("communication_subsidy_scheme")
  private Integer communicationSubsidyScheme;
  /** 
   * 通讯补贴金额
   */
  @TableField("communication_subsidy_amount")
  private Double communicationSubsidyAmount;
  /** 
   * 午餐补贴计算类型
   */
  @TableField("lunch_allowance_scheme")
  private Integer lunchAllowanceScheme;
  /** 
   * 午餐补贴金额
   */
  @TableField("lunch_allowance_amount")
  private Double lunchAllowanceAmount;
  /** 
   * 住房补助计算类型
   */
  @TableField("housing_subsidy_scheme")
  private Integer housingSubsidyScheme;
  /** 
   * 住房补助金额
   */
  @TableField("housing_subsidy_amount")
  private Double housingSubsidyAmount;
  /** 
   * 计税方式
   */
  @TableField("tax_calculation_type")
  private Integer taxCalculationType;

}

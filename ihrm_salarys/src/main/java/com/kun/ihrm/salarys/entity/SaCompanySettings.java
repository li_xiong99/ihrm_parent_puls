package com.kun.ihrm.salarys.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 工资-企业设置信息实体类
 * @packageName com.kun.ihrm
 * @fileName SaCompanySettings.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
@Data
@TableName("sa_company_settings")
public class SaCompanySettings {

  /** 
   * 企业id
   */
  @TableId(value = "company_id", type = IdType.AUTO)
  private String companyId;
  /** 
   * 0是未设置，1是已设置
   */
  @TableField("is_settings")
  private Integer isSettings;
  /** 
   * 当前显示报表月份
   */
  @TableField("data_month")
  private String dataMonth;

}

package com.kun.ihrm.salarys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.salarys.entity.SaArchiveDetail;


/**
 * 工资-归档详情 Service接口
 * @packageName com.kun.ihrm
 * @fileName SaArchiveDetailService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
public interface SaArchiveDetailService extends IService<SaArchiveDetail>{


    PageResult getlistPage(Integer page, Integer pageSize, String companyId);
}

package com.kun.ihrm.salarys.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;


/**
 * SaUserSalaryChange实体类
 * @packageName com.kun.ihrm
 * @fileName SaUserSalaryChange.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
@Data
@TableName("sa_user_salary_change")
public class SaUserSalaryChange {

  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 用户id
   */
  @TableField(value = "user_id")
  private String userId;
  /** 
   * 调整前基本工资
   */
  @TableField("current_basic_salary")
  private Double currentBasicSalary;
  /** 
   * 调整前岗位工资
   */
  @TableField("current_post_wage")
  private Double currentPostWage;
  /** 
   * 调整基本工资
   */
  @TableField("adjustment_of_basic_wages")
  private Double adjustmentOfBasicWages;
  /** 
   * 调整岗位工资
   */
  @TableField("adjust_post_wages")
  private Double adjustPostWages;
  /** 
   * 调薪生效时间
   */
  @TableField("effective_time_of_pay_adjustment")
  private Date effectiveTimeOfPayAdjustment;
  /** 
   * 调薪原因
   */
  @TableField("cause_of_salary_adjustment")
  private String causeOfSalaryAdjustment;
  /** 
   * 附件
   */
  @TableField("enclosure")
  private String enclosure;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Double getCurrentBasicSalary() {
    return currentBasicSalary;
  }

  public void setCurrentBasicSalary(Double currentBasicSalary) {
    this.currentBasicSalary = currentBasicSalary;
  }

  public Double getCurrentPostWage() {
    return currentPostWage;
  }

  public void setCurrentPostWage(Double currentPostWage) {
    this.currentPostWage = currentPostWage;
  }

  public Double getAdjustmentOfBasicWages() {
    return adjustmentOfBasicWages;
  }

  public void setAdjustmentOfBasicWages(Double adjustmentOfBasicWages) {
    this.adjustmentOfBasicWages = adjustmentOfBasicWages;
  }

  public Double getAdjustPostWages() {
    return adjustPostWages;
  }

  public void setAdjustPostWages(Double adjustPostWages) {
    this.adjustPostWages = adjustPostWages;
  }

  public Date getEffectiveTimeOfPayAdjustment() {
    return effectiveTimeOfPayAdjustment;
  }

  public void setEffectiveTimeOfPayAdjustment(Date effectiveTimeOfPayAdjustment) {
    this.effectiveTimeOfPayAdjustment = effectiveTimeOfPayAdjustment;
  }

  public String getCauseOfSalaryAdjustment() {
    return causeOfSalaryAdjustment;
  }

  public void setCauseOfSalaryAdjustment(String causeOfSalaryAdjustment) {
    this.causeOfSalaryAdjustment = causeOfSalaryAdjustment;
  }

  public String getEnclosure() {
    return enclosure;
  }

  public void setEnclosure(String enclosure) {
    this.enclosure = enclosure;
  }
}

package com.kun.ihrm.salarys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.salarys.entity.SaArchiveDetail;
import com.kun.ihrm.salarys.mapper.SaArchiveDetailMapper;
import com.kun.ihrm.salarys.service.SaArchiveDetailService;
import com.kun.ihrm.salarys.vo.BsUserVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 * 工资-归档详情 Service接口实现
 * @packageName com.kun.ihrm
 * @fileName SaArchiveDetailServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
@Service
public class SaArchiveDetailServiceImpl 
				extends ServiceImpl<SaArchiveDetailMapper, SaArchiveDetail>
				implements SaArchiveDetailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SaArchiveDetailServiceImpl.class);



	@Override
	public PageResult getlistPage(Integer page, Integer pageSize, String companyId) {
//		IPage<BsUserVo> pages = new Page(page, pageSize);
//		baseMapper.selectPageList(companyId, page);
		// 处理查询结果
//		List<Map<String, Object>> userDataList = page.getRecords();
//		long totalCount = page.getTotal();

		PageResult pageResult = new PageResult();
		pageResult.setRows(baseMapper.selectPageList(companyId, PageRequest.of(page - 1, pageSize)));
		pageResult.setTotal(baseMapper.selectCountBsUser(companyId));
		return new PageResult<BsUserVo>(pageResult.getTotal(),pageResult.getRows());
	}
}

package com.kun.ihrm.salarys.controller;


//import com.kun.common.controller.BaseController;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.kun.ihrm.common.controller.BaseController;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import com.kun.ihrm.common.utils.R;
import com.kun.ihrm.model.salarys.UserSalary;
import com.kun.ihrm.salarys.entity.SaCompanySettings;
import com.kun.ihrm.salarys.entity.SaSettings;
import com.kun.ihrm.salarys.entity.SaUserSalary;
import com.kun.ihrm.salarys.entity.SaUserSalaryChange;
import com.kun.ihrm.salarys.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@CrossOrigin
@RequestMapping("/salarys")
public class CompanySettingsController extends BaseController {

    @Autowired
    SaCompanySettingsService companySettingsService;


    @Autowired
    private SaArchiveDetailService saArchiveDetailService;


    @Autowired
    SaSettingsService saSettingsService;

    @Autowired
    SaUserSalaryService saUserSalaryService;

    @Autowired
    SaUserSalaryChangeService saUserSalaryChangeService;


//    @Autowired
//    SaUserSalaryService saUserSalaryService;


    /**
     * 定薪
     * POST http://localhost:8080/api/salarys/init/1066370498633486336
     * 接口ID：105154879
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105154879
     *
     * @param saUserSalary
     * @return
     */
    @PostMapping("/init/{userId}")
    public Result addModify(@RequestBody SaUserSalary saUserSalary) {
        boolean save = saUserSalaryService.updateById(saUserSalary);
        return new Result(ResultCode.SUCCESS,save);
    }

    @GetMapping("test")
    public String test() {
        return "进来了";
    }


    /**
     * 设置——计新设置——津贴设置
     * GET http://localhost:8080/api/salarys/settings
     * 接口ID：105156047
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105156047
     *
     * @return
     */
    @GetMapping("/settings")
    public R getSettings(@RequestParam("id") String id) {
        SaSettings byId = saSettingsService.getById(id);
        return R.ok().setElement(true, 10000, "操作成功！", byId);
    }

    /**
     * 查看
     * GET http://localhost:8080/api/salarys/1074238801402007552
     * 接口ID：105169137
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105169137
     *
     * @param userId
     * @return
     */
    @GetMapping("/{userId}")
    public Result getYearMonth(@RequestParam("userId") String userId) {
        SaUserSalary byId = saUserSalaryService.getById(userId);
        return new Result(ResultCode.SUCCESS, byId);
    }


    /**
     * 初始加载分页列表
     * GET http://localhost:8080/api/salarys/list
     * 接口ID：105129400
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105129400
     *
     * @return
     */
    @GetMapping("/list")
    public Result getList(@RequestParam("page") Integer page, @RequestParam("pageSize") Integer pageSize) {
        PageResult rows = saArchiveDetailService.getlistPage(page, pageSize, companyId);
        return new Result(ResultCode.SUCCESS, rows);
    }

    /**
     * 初始加载出报表日期，和自己的状态
     * GET http://localhost:8080/api/salarys/company-settings
     * 接口ID：105066629
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105066629
     *
     * @param session
     * @return
     */
    @GetMapping("/company-settings")
    public R getUserSettings(HttpSession session) {
//        BsUser user = (BsUser) session.getAttribute("user");
        SaCompanySettings byId = companySettingsService.getById(1);//user.getId()
        if (byId != null) {
            return R.ok().setElement(true, 10000, "操作成功！", byId);
        } else {
            return R.ok().setElement(false, 400, "操作失败！", byId);

        }
    }


    /**
     * 调薪
     * GET http://localhost:8080/api/salarys/modify/1063705989926227968
     * 接口ID：105137190
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105137190
     *
     * @param userId
     * @return
     */
    @GetMapping("/modify/{userId}")
    public Result getModify(@RequestParam("userId") String userId ) {
        SaUserSalary saUserSalary = saUserSalaryService.getById(userId);
        Result result = new Result(ResultCode.SUCCESS, saUserSalary);
        return result;
    }



    @PostMapping("/modify/{userId}")
    public Result getModify(@PathVariable("userId") String userId, @RequestBody SaUserSalary saUserSalary) {
        saUserSalary.setUserId(userId);
        Result result = new Result(ResultCode.SUCCESS,saUserSalaryService.updateById(saUserSalary));
        return result;
    }


}

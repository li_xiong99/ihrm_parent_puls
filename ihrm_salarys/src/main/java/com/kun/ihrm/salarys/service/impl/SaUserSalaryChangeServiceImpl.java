package com.kun.ihrm.salarys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.salarys.entity.SaUserSalaryChange;
import com.kun.ihrm.salarys.mapper.SaUserSalaryChangeMapper;
import com.kun.ihrm.salarys.service.SaUserSalaryChangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * SaUserSalaryChangeService接口实现
 * @packageName com.kun.ihrm
 * @fileName SaUserSalaryChangeServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
@Service
public class SaUserSalaryChangeServiceImpl 
				extends ServiceImpl<SaUserSalaryChangeMapper, SaUserSalaryChange>
				implements SaUserSalaryChangeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SaUserSalaryChangeServiceImpl.class);


}

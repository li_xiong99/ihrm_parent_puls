package com.kun.ihrm.salarys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.salarys.entity.SaArchive;


/**
 * 工资-归档表 Service接口
 * @packageName com.kun.ihrm
 * @fileName SaArchiveService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
public interface SaArchiveService extends IService<SaArchive>{


}

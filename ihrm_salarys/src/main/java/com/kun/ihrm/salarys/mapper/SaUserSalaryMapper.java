package com.kun.ihrm.salarys.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.salarys.entity.SaUserSalary;

/**
 * 工资-员工薪资表 Mapper接口
 * @packageName com.kun.ihrm
 * @fileName SaUserSalaryMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
public interface SaUserSalaryMapper extends BaseMapper<SaUserSalary> {

}

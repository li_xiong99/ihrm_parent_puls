package com.kun.ihrm.salarys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.salarys.entity.SaArchiveDetail;
import com.kun.ihrm.salarys.vo.BsUserVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.PageRequest;

import java.util.List;


/**
 * 工资-归档详情 Mapper接口
 * @packageName com.kun.ihrm
 * @fileName SaArchiveDetailMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
public interface SaArchiveDetailMapper extends BaseMapper<SaArchiveDetail> {


//    @Select("SELECT bu.id, bu.username, bu.mobile, bu.work_number AS workNumber, "
//            + "bu.in_service_status AS inServiceStatus, bu.department_name AS departmentName, "
//            + "bu.department_id AS departmentId, bu.time_of_entry AS timeOfEntry, "
//            + "bu.form_of_employment AS formOfEmployment, sauss.current_basic_salary AS currentBasicSalary, "
//            + "sauss.current_post_wage AS currentPostWage "
//            + "FROM bs_user bu "
//            + "LEFT JOIN sa_user_salary sauss ON bu.id = sauss.user_id "
//            + "WHERE bu.company_id = #{companyId}")
    List<BsUserVo> selectPageList(@Param("companyId") String companyId, @Param("pageRequest") PageRequest pageRequest);

    Long selectCountBsUser(@Param("companyId") String companyId);
}

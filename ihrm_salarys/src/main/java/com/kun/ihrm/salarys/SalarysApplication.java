package com.kun.ihrm.salarys;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@MapperScan("com.kun.ihrm.salarys.mapper")
public class SalarysApplication {
    public static void main(String[] args) {
        SpringApplication.run(SalarysApplication.class, args);
    }
}

package com.kun.ihrm.salarys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.salarys.entity.SaSettings;
import com.kun.ihrm.salarys.mapper.SaSettingsMapper;
import com.kun.ihrm.salarys.service.SaSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 工资-企业设置信息 Service接口实现
 * @packageName com.kun.ihrm
 * @fileName SaSettingsServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
@Service
public class SaSettingsServiceImpl 
				extends ServiceImpl<SaSettingsMapper, SaSettings>
				implements SaSettingsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SaSettingsServiceImpl.class);


}

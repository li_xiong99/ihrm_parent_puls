package com.kun.ihrm.salarys.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 工资-归档详情实体类
 * @packageName com.kun.ihrm
 * @fileName SaArchiveDetail.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
@Data
@TableName("sa_archive_detail")
public class SaArchiveDetail {

  /** 
   * id
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 归档id
   */
  @TableField("archive_id")
  private String archiveId;
  /** 
   * 用户id
   */
  @TableField("user_id")
  private String userId;
  /** 
   * 姓名
   */
  @TableField("username")
  private String username;
  /** 
   * 手机号
   */
  @TableField("mobile")
  private String mobile;
  /** 
   * 工号
   */
  @TableField("work_number")
  private String workNumber;
  /** 
   * 部门
   */
  @TableField("department_name")
  private String departmentName;
  /** 
   * 身份证号
   */
  @TableField("id_number")
  private String idNumber;
  /** 
   * 在职状态
   */
  @TableField("in_service_status")
  private String inServiceStatus;
  /** 
   * 聘用形式
   */
  @TableField("form_of_employment")
  private String formOfEmployment;
  /** 
   * 银行卡号
   */
  @TableField("bank_card_number")
  private String bankCardNumber;
  /** 
   * 银行卡号
   */
  @TableField("opening_bank")
  private String openingBank;
  /** 
   * 公积金个人
   */
  @TableField("provident_fund_individual")
  private Double providentFundIndividual;
  /** 
   * 社保个人
   */
  @TableField("social_security_individual")
  private Double socialSecurityIndividual;
  /** 
   * 养老个人
   */
  @TableField("old_age_individual")
  private Double oldAgeIndividual;
  /** 
   * 医疗个人
   */
  @TableField("medical_individual")
  private Double medicalIndividual;
  /** 
   * 失业个人
   */
  @TableField("unemployed_individual")
  private Double unemployedIndividual;
  /** 
   * 大病个人
   */
  @TableField("a_person_of_great_disease")
  private Double aPersonOfGreatDisease;
  /** 
   * 社保扣款
   */
  @TableField("social_security")
  private Double socialSecurity;
  /** 
   * 公积金扣款
   */
  @TableField("total_provident_fund_individual")
  private Double totalProvidentFundIndividual;
  /** 
   * 社保企业
   */
  @TableField("social_security_enterprise")
  private Double socialSecurityEnterprise;
  /** 
   * 养老企业
   */
  @TableField("pension_enterprise")
  private Double pensionEnterprise;
  /** 
   * 医疗企业
   */
  @TableField("medical_enterprise")
  private Double medicalEnterprise;
  /** 
   * 失业企业
   */
  @TableField("unemployed_enterprise")
  private Double unemployedEnterprise;
  /** 
   * 工伤企业
   */
  @TableField("industrial_injury_enterprise")
  private Double industrialInjuryEnterprise;
  /** 
   * 生育企业
   */
  @TableField("childbearing_enterprise")
  private Double childbearingEnterprise;
  /** 
   * 大病企业
   */
  @TableField("big_disease_enterprise")
  private Double bigDiseaseEnterprise;
  /** 
   * 公积金企业
   */
  @TableField("provident_fund_enterprises")
  private Double providentFundEnterprises;
  /** 
   * 公积金社保企业
   */
  @TableField("social_security_provident_fund_enterprises")
  private Double socialSecurityProvidentFundEnterprises;
  /** 
   * 公积金需纳税额
   */
  @TableField("tax_to_provident_fund")
  private Double taxToProvidentFund;
  /** 
   * 计薪天数
   */
  @TableField("official_salary_days")
  private Double officialSalaryDays;
  /** 
   * 考勤扣款
   */
  @TableField("attendance_deduction_monthly")
  private Double attendanceDeductionMonthly;
  /** 
   * 计薪标准
   */
  @TableField("salary_standard")
  private Double salaryStandard;
  /** 
   * 最新工资基数合计
   */
  @TableField("current_salary_total_base")
  private Double currentSalaryTotalBase;
  /** 
   * 最新基本工资基数
   */
  @TableField("current_base_salary")
  private Double currentBaseSalary;
  /** 
   * 当月基本工资基数
   */
  @TableField("base_salary_by_month")
  private Double baseSalaryByMonth;
  /** 
   * 计税方式
   */
  @TableField("tax_counting_method")
  private String taxCountingMethod;
  /** 
   * 当月纳税基本工资
   */
  @TableField("base_salary_to_tax_by_month")
  private Double baseSalaryToTaxByMonth;
  /** 
   * 税前工资合计
   */
  @TableField("salary_before_tax")
  private Double salaryBeforeTax;
  /** 
   * 工资合计
   */
  @TableField("salary")
  private Double salary;
  /** 
   * 应纳税工资
   */
  @TableField("salary_by_tax")
  private Double salaryByTax;
  /** 
   * 税前实发
   */
  @TableField("payment_before_tax")
  private Double paymentBeforeTax;
  /** 
   * 应扣税
   */
  @TableField("tax")
  private Double tax;
  /** 
   * 税后工资合计
   */
  @TableField("salary_after_tax")
  private Double salaryAfterTax;
  /** 
   * 实发工资
   */
  @TableField("payment")
  private Double payment;
  /** 
   * 实发工资备注
   */
  @TableField("payment_remark")
  private String paymentRemark;
  /** 
   * 薪酬成本
   */
  @TableField("salary_cost")
  private Double salaryCost;
  /** 
   * 企业人工成本
   */
  @TableField("enterprise_labor_cost")
  private Double enterpriseLaborCost;
  /** 
   * 调薪金额
   */
  @TableField("salary_change_amount")
  private Double salaryChangeAmount;
  /** 
   * 调薪比例
   */
  @TableField("salary_change_scale")
  private Double salaryChangeScale;
  /** 
   * 调薪生效时间
   */
  @TableField("effective_time_of_pay_adjustment")
  private Double effectiveTimeOfPayAdjustment;
  /** 
   * 调薪原因
   */
  @TableField("cause_of_salary_adjustment")
  private Double causeOfSalaryAdjustment;
  /** 
   * 注释
   */
  @TableField("remark")
  private String remark;
  /** 
   * 发薪月数
   */
  @TableField("payment_months")
  private Integer paymentMonths;

}

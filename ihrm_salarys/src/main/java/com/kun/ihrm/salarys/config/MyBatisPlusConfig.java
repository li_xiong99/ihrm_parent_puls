package com.kun.ihrm.salarys.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * MyBatis-Plus配置类
 * @packageName com.kun.ihrm.config;
 * @fileName MyBatisPlusConfig.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
@Configuration
@MapperScan("com.kun.ihrm.mapper")
public class MyBatisPlusConfig {

   @Bean
   public PaginationInterceptor paginationInterceptor(){
       return new PaginationInterceptor();
   }

//    @Bean
//    public MybatisPlusInterceptor mybatisPlusInterceptor() {
//        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
//        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
//        return interceptor;
//    }

}

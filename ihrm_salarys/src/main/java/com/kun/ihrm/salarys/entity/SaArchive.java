package com.kun.ihrm.salarys.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;


/**
 * 工资-归档表实体类
 * @packageName com.kun.ihrm
 * @fileName SaArchive.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
@Data
@TableName("sa_archive")
public class SaArchive {

  /** 
   * id
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 企业id
   */
  @TableField("company_id")
  private String companyId;
  /** 
   * 年月
   */
  @TableField("years_month")
  private String yearsMonth;
  /** 
   * 创建时间
   */
  @TableField("creation_time")
  private Date creationTime;
  /** 
   * 人工成本
   */
  @TableField("artificial_cost")
  private Double artificialCost;
  /** 
   * 税前工资
   */
  @TableField("gross_salary")
  private Double grossSalary;
  /** 
   * 五险一金
   */
  @TableField("five_insurances")
  private Double fiveInsurances;

}

package com.kun.ihrm.salarys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * 工资-员工薪资表实体类
 * @packageName com.kun.ihrm
 * @fileName SaUserSalary.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:59:51
 * @lastModify 2023年8月25日 上午10:59:51
 * @version 1.0.0
 */
@TableName("sa_user_salary")
public class SaUserSalary implements Serializable {


  @TableId(value = "user_id", type = IdType.AUTO)
  private String userId;
  /** 
   * 当前基本工资
   */
  @TableField("current_basic_salary")
  private Double currentBasicSalary;
  /** 
   * 当前岗位工资
   */
  @TableField("current_post_wage")
  private Double currentPostWage;
  /** 
   * 定薪基本工资
   */
  @TableField("fixed_basic_salary")
  private Double fixedBasicSalary;
  /** 
   * 定薪岗位工资
   */
  @TableField("fixed_post_wage")
  private Double fixedPostWage;
  /** 
   * 转正基本工资
   */
  @TableField("correction_of_basic_wages")
  private Double correctionOfBasicWages;
  /** 
   * 转正岗位工资
   */
  @TableField("turn_to_post_wages")
  private Double turnToPostWages;


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Double getCurrentBasicSalary() {
    return currentBasicSalary;
  }

  public void setCurrentBasicSalary(Double currentBasicSalary) {
    this.currentBasicSalary = currentBasicSalary;
  }

  public Double getCurrentPostWage() {
    return currentPostWage;
  }

  public void setCurrentPostWage(Double currentPostWage) {
    this.currentPostWage = currentPostWage;
  }

  public Double getFixedBasicSalary() {
    return fixedBasicSalary;
  }

  public void setFixedBasicSalary(Double fixedBasicSalary) {
    this.fixedBasicSalary = fixedBasicSalary;
  }

  public Double getFixedPostWage() {
    return fixedPostWage;
  }

  public void setFixedPostWage(Double fixedPostWage) {
    this.fixedPostWage = fixedPostWage;
  }

  public Double getCorrectionOfBasicWages() {
    return correctionOfBasicWages;
  }

  public void setCorrectionOfBasicWages(Double correctionOfBasicWages) {
    this.correctionOfBasicWages = correctionOfBasicWages;
  }

  public Double getTurnToPostWages() {
    return turnToPostWages;
  }

  public void setTurnToPostWages(Double turnToPostWages) {
    this.turnToPostWages = turnToPostWages;
  }
}

package com.kun.ihrm.salarys.vo;

//import lombok.Data;

import java.util.Date;
//@Data
public class BsUserVo {
    public Integer currentBasicSalary;
    public String departmentName;
    public String id;
    public Integer currentPostWage;
    public Date timeOfEntry;
    public String username;
    public String mobile;
    public String workNumber;
    public String departmentId;
    public Integer inServiceStatus;
    public Integer formOfEmployment;

    public Integer getCurrentBasicSalary() {
        return currentBasicSalary;
    }

    public void setCurrentBasicSalary(Integer currentBasicSalary) {
        this.currentBasicSalary = currentBasicSalary;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCurrentPostWage() {
        return currentPostWage;
    }

    public void setCurrentPostWage(Integer currentPostWage) {
        this.currentPostWage = currentPostWage;
    }

    public Date getTimeOfEntry() {
        return timeOfEntry;
    }

    public void setTimeOfEntry(Date timeOfEntry) {
        this.timeOfEntry = timeOfEntry;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getInServiceStatus() {
        return inServiceStatus;
    }

    public void setInServiceStatus(Integer inServiceStatus) {
        this.inServiceStatus = inServiceStatus;
    }

    public Integer getFormOfEmployment() {
        return formOfEmployment;
    }

    public void setFormOfEmployment(Integer formOfEmployment) {
        this.formOfEmployment = formOfEmployment;
    }
}

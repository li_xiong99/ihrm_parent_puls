package com.kun.ihrm.system.controller;

import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import com.kun.ihrm.system.entity.BsCity;
import com.kun.ihrm.system.service.BsCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/sys/city")
public class CityController {

    @Autowired
    private BsCityService bsCityService;

    @GetMapping("")
    public Result findAll(){
        List<BsCity> list = bsCityService.findAll();
        return  new Result(ResultCode.SUCCESS,list);
    }


}

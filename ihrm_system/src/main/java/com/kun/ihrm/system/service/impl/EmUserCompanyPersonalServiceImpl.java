package com.kun.ihrm.system.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.system.entity.EmUserCompanyPersonal;
import com.kun.ihrm.system.mapper.EmUserCompanyPersonalMapper;
import com.kun.ihrm.system.service.EmUserCompanyPersonalService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class EmUserCompanyPersonalServiceImpl extends ServiceImpl<EmUserCompanyPersonalMapper, EmUserCompanyPersonal> implements EmUserCompanyPersonalService {

    @Resource
    private EmUserCompanyPersonalMapper emCompanyPersonalMapper;

    @Override
    public PageResult getByPageList(Integer page, Integer pageSize) {
        PageResult pageUtils=new PageResult();
        pageUtils.setRows(emCompanyPersonalMapper.selectByPageList((page-1)*pageSize,pageSize));
        pageUtils.setTotal(emCompanyPersonalMapper.selectByTotal());
        return pageUtils;
    }


}

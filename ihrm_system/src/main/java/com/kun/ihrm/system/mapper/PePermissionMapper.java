package com.kun.ihrm.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.model.system.PePermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;
//import com.kun.ihrm.system.entity.PePermission;

/**
 * PePermissionMapper接口
 * @packageName system.com.kun.ihrm
 * @fileName PePermissionMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
public interface PePermissionMapper extends BaseMapper<PePermission> {


    List<PePermission> getPermissionInfo(@Param("type") Integer type, @Param("enVisible") Integer enVisible);
}

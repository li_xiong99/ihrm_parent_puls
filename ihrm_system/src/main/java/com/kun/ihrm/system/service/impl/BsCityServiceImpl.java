package com.kun.ihrm.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.system.entity.BsCity;
import com.kun.ihrm.system.mapper.BsCityMapper;
import com.kun.ihrm.system.service.BsCityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * BsCityService接口实现
 * @packageName system.com.kun.ihrm
 * @fileName BsCityServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Service
public class BsCityServiceImpl 
				extends ServiceImpl<BsCityMapper, BsCity> 
				implements BsCityService { 

    private static final Logger LOGGER = LoggerFactory.getLogger(BsCityServiceImpl.class);

    @Override
    public List<BsCity> findAll() {
        return this.list();
    }
}

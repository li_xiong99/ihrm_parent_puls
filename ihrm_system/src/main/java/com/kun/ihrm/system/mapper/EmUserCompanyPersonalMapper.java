package com.kun.ihrm.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.system.entity.EmUserCompanyPersonal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * EmuserCompanyPersonalMapper接口
 * @packageName .main.java
 * @fileName EmuserCompanyPersonalMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
@Mapper
public interface EmUserCompanyPersonalMapper extends BaseMapper<EmUserCompanyPersonal> {
    List<EmUserCompanyPersonal> selectByPageList(@Param("page") Integer page, @Param("pageSize") Integer pageSize);

//    List<EmuserCompanyPersonal> selectByPageList(@Param("page") Integer page, @Param("pageSize") Integer pageSize, @Param("companyId") String companyId);
Long selectByTotal();
}

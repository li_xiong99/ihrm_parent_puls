package com.kun.ihrm.system.vo;


import com.baomidou.mybatisplus.annotation.TableField;

import java.util.List;

public class PeRolePermission {
    /**
     * 主键ID
     */
    private String roleId;
    /**
     * 权限名称
     */
    private String permissionId;

    /**
     * 权限id的集合
     */
    @TableField(exist = false)
    List<String> list ;

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public PeRolePermission() {
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }



}

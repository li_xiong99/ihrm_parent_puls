package com.kun.ihrm.system.service.impl;

import com.baidu.aip.util.Base64Util;
import com.kun.ihrm.common.utils.IdWorker;
import com.kun.ihrm.model.system.BsUser;
import com.kun.ihrm.model.system.response.FaceLoginResult;
import com.kun.ihrm.model.system.response.QRCode;
import com.kun.ihrm.system.mapper.BsUserMapper;
import com.kun.ihrm.system.utils.BaiduAiUtil;
import com.kun.ihrm.system.utils.QRCodeUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Service
public class FaceLoginService {

    @Value("${my.url}")
    private String url;

    @Resource
    private IdWorker idWorker;

    @Resource
    private QRCodeUtil qrCodeUtil;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private BaiduAiUtil baiduAiUtil;

    @Resource
    private BsUserMapper userMapper;

	//创建二维码
    public QRCode getQRCode() throws Exception {
        //1.创建唯一标识
        String code = idWorker.nextId() + "";
        //2.生成二维码
        String content = url + "?code=" + code;
        //测试
        System.out.println(content);

        String file = qrCodeUtil.crateQRCode(content);
        //3.存入当前二维码的状态(存入redis)
        FaceLoginResult result = new FaceLoginResult("-1");
        redisTemplate.boundValueOps(getCacheKey(code)).set(result , 10 , TimeUnit.MINUTES);
		return new QRCode(code , file);
    }

	//根据唯一标识，查询用户是否登录成功
    public FaceLoginResult checkQRCode(String code) {
        String key = getCacheKey(code);
        return (FaceLoginResult) redisTemplate.opsForValue().get(key);
    }

	//扫描二维码之后，使用拍摄照片进行登录
    public String loginByFace(String code, MultipartFile attachment) throws Exception {
        //1.调用百度云AI查询当前的用户
        String userId = baiduAiUtil.faceSearch(Base64Util.encode(attachment.getBytes()));
        //2.自动登录
        FaceLoginResult result = new FaceLoginResult("0");
        if (userId != null){
            //登陆
            BsUser user = userMapper.selectById(userId);
            //获取subject
            Subject subject = SecurityUtils.getSubject();
            //调用login方法登陆
            subject.login(new UsernamePasswordToken(user.getMobile() , user.getPassword()));
            //获取token
            String token = (String) subject.getSession().getId();
            result = new FaceLoginResult("1" , token , userId);
        }
        //3.修改二维码的状态
        redisTemplate.boundValueOps(getCacheKey(code)).set(result , 10 , TimeUnit.MINUTES);
		return userId;
    }

	//构造缓存key
    private String getCacheKey(String code) {
        return "qrcode_" + code;
    }
}

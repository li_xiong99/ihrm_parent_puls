package com.kun.ihrm.system.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * PeUserRole实体类
 * @packageName system.com.kun.ihrm
 * @fileName PeUserRole.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Data
@TableName("pe_user_role")
public class PeUserRole {

  /** 
   * 角色ID
   */
  @TableId(value = "role_id", type = IdType.AUTO)
  private String roleId;
  /** 
   * 权限ID
   */
  @TableId(value = "user_id", type = IdType.AUTO)
  private String userId;

}

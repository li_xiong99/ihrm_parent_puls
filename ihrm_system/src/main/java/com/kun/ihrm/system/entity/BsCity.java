package com.kun.ihrm.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;


/**
 * BsCity实体类
 *
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @version 1.0.0
 * @packageName system.com.kun.ihrm
 * @fileName BsCity.java
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 */
@Data
@TableName("bs_city")
public class BsCity {

    /**
     * 公积金城市ID
     */
    @TableId(value = "id", type = IdType.INPUT)
  private String id;
    /**
     * 公积金城市名称
     */
    @TableField("name")
    private String name;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

}

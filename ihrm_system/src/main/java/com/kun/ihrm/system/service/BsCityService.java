package com.kun.ihrm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.system.entity.BsCity;

import java.util.List;


/**
 * BsCityService接口
 * @packageName system.com.kun.ihrm
 * @fileName BsCityService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
public interface BsCityService extends IService<BsCity>{

    /**
     * 查詢 所有 公积金城市 接口
     * @return
     */
    List<BsCity> findAll();
}

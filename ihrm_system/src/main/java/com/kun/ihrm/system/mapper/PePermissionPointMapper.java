package com.kun.ihrm.system.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.system.entity.PePermissionPoint;

/**
 * PePermissionPointMapper接口
 * @packageName system.com.kun.ihrm
 * @fileName PePermissionPointMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
public interface PePermissionPointMapper extends BaseMapper<PePermissionPoint> {

}

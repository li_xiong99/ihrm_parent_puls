package com.kun.ihrm.system.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.system.entity.PeUserRole;
import com.kun.ihrm.system.mapper.PeUserRoleMapper;
import com.kun.ihrm.system.service.PeUserRoleService;
import org.mybatis.logging.Logger;
import org.mybatis.logging.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * PeuserRoleService接口实现
 * @packageName system.com.kun.ihrm
 * @fileName PeuserRoleServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Service
public class PeUserRoleServiceImpl
				extends ServiceImpl<PeUserRoleMapper, PeUserRole>
				implements PeUserRoleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PeUserRoleServiceImpl.class);


}

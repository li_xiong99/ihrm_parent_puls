package com.kun.ihrm.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.model.system.BsUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * BsUserMapper接口
 * @packageName system.com.kun.ihrm.copy
 * @fileName BsUserMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月30日 下午7:59:44
 * @lastModify 2023年8月30日 下午7:59:44
 * @version 1.0.0
 */
public interface BsUserMapper extends BaseMapper<BsUser> {
    //根据手机号查询用户
    BsUser selectMobile(@Param("mobile") String mobile);

    List<BsUser> selectByPageList(@Param("page") Integer page, @Param("pageSize") Integer pageSize);
//    BsUser getByUserInfo(@Param("id") String id);
}

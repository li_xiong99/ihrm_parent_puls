package com.kun.ihrm.system.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.kun.ihrm.common.Constants.Constant;
import com.kun.ihrm.common.controller.BaseController;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import com.kun.ihrm.common.poi.ExcelImportUtil;
import com.kun.ihrm.common.utils.IdWorker;
import com.kun.ihrm.model.company.CoCompany;
import com.kun.ihrm.model.system.BsUser;
import com.kun.ihrm.model.system.PePermission;
import com.kun.ihrm.model.system.PeRole;
import com.kun.ihrm.model.system.response.ProfileResult;
import com.kun.ihrm.system.client.CompanyFeignClient;
import com.kun.ihrm.system.service.BsUserService;
import com.kun.ihrm.system.service.PePermissionService;
import com.kun.ihrm.system.service.PeRoleService;
import com.kun.ihrm.system.vo.PeRolePermissionVO;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: hyl
 * @date: 2020/01/09
 **/
//解决跨域
//@CrossOrigin
//extends BaseController
@RestController
@RequestMapping(value = "/sys")
public class UserController extends BaseController {

    @Resource
    private BsUserService userService;

    @Resource
    private CompanyFeignClient companyFeignClient;
    //用户
    @Resource
    private BsUserService bsUserService;
    //角色
    @Resource
    private PeRoleService peRoleService;

    @Resource
    private PePermissionService pePermissionService;


    @Resource
    IdWorker idWorker;


    /**
     * 导入Excel,添加用户
     */
    @RequestMapping(value = "/user/import" , method = RequestMethod.POST)
    public Result importUser(@RequestParam(name = "file") MultipartFile file) throws Exception {
        List<BsUser> list = new ExcelImportUtil(BsUser.class).readExcel(file.getInputStream(), 1, 1);
        //3.批量保存用户
        userService.saveAll(list , companyId , companyName);
        return new Result(ResultCode.SUCCESS);
    }

    /**
     * 用户登录
     */
    @PostMapping(value = "/login")
    public Result login(@RequestBody Map<String, Object> loginMap) {
        System.out.println(loginMap);
        String mobile = (String) loginMap.get("mobile");
        String password = (String) loginMap.get("password");
        try {

              //如果该公司被禁止登录,直接返回
            BsUser user = userService.findByMobile(mobile);
            if (!Constant.UserLevel.SAASADMIN.equals(user.getLevel())){
                //返回时List<LinkedHashMap> ,需要对其解析
                LinkedHashMap linkedHashMap
                        = (LinkedHashMap) companyFeignClient.findCompanyById(user.getCompanyId()).getData();
                CoCompany company = JSON.parseObject(JSON.toJSONString(linkedHashMap), new TypeReference<CoCompany>() {
                });
                if (!ObjectUtils.isEmpty(company)){
                    if (Constant.Company.COMPANY_DISABLE.equals(String.valueOf(company.getState()))){
                        return new Result(ResultCode.UNAUTHORISE);
                    }
                }
            }
            //构造登录令牌, mobile , 3
            password = new Md5Hash(password, mobile , 3 ).toString();
            UsernamePasswordToken upToken = new UsernamePasswordToken(mobile, password);
            //获取subject
            Subject subject = SecurityUtils.getSubject();
            //调用login方法,进入realm完成认证
            subject.login(upToken);
            //获取sessionId
            String sessionId = (String) subject.getSession().getId();
            //构造返回结果
            return new Result(ResultCode.SUCCESS, sessionId);
        } catch (Exception e) {
            System.out.println("error : " + e);
            return new Result(ResultCode.MOBILEORPASSWORDERROR);
        }
    }

    /**
     * 用户登录成功之后,获取用户信息
     */
    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public Result profile() {
        //获取session中的安全数据
        Subject subject = SecurityUtils.getSubject();
        //subject获取所有的安全集合
        PrincipalCollection principals = subject.getPrincipals();
        //获取安全数据
        ProfileResult result = (ProfileResult) principals.getPrimaryPrincipal();
        return new Result(ResultCode.SUCCESS, result);
    }


//    /**
//     * http://localhost:8080/api/sys/user/1063705989926227968?id=1063705989926227968
//     * 点击员工页面显示员工信息
//     * @param id
//     * @return
//     */
//    @GetMapping("/user/{id}")
//    public Result getUser(@PathVariable("id")String id){
//        BsUser user = userService.getById(id);
//        return new Result(ResultCode.SUCCESS,user);
//    }


    /**
     * 员工列表 成功
     * GET /sys/user
     * 接口ID：104778856
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104778856
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Result userList(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "30") Integer pageSize) {
        PageResult userIPage = bsUserService.getByPageList(page, pageSize);
        return new Result(ResultCode.SUCCESS, userIPage);
    }

    /**
     * 员工查看个人详情   成功
     * GET /sys/user/{id}
     * 接口ID：105082097
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105082097
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public Result findByUserInfo(@PathVariable("id") String id) {
        BsUser user = bsUserService.getById(id);
        return new Result(ResultCode.SUCCESS, user);
    }

    /**
     * 员工删除 成功
     * DELETE /sys/user/{id}
     * 接口ID：104968698
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104968698
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public Result deleteUserId(@PathVariable("id") String id) {
        boolean rows = bsUserService.removeById(id);
        return new Result(ResultCode.SUCCESS, rows);
    }

    /**
     * 新增员工 完成
     * POST /sys/user
     * 接口ID：105094493
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105094493
     */
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public Result saveUsrInfo(@RequestBody  BsUser bsUser) {
        bsUser.setId(idWorker.nextId() + "");
        bsUser.setCreateTime(new Date());
        bsUser.setLevel("user");
        boolean save = bsUserService.save(bsUser);
        System.out.println(save);
        return new Result(ResultCode.SUCCESS, save);
    }

    /**
     * 查看员工保持 完成
     * sys/user/1063705989926227968
     */
    @RequestMapping(value = "/user/{id}",method = RequestMethod.PUT)
    public Result updateToUser(@PathVariable("id") String id,@RequestBody BsUser user){
        boolean rows = userService.updateById(user);
        if(rows){
            return new Result(ResultCode.SUCCESS,rows);
        }else {
            return new Result(ResultCode.FAIL);
        }

    }
    /**
     * 显示所有权限信息 完成
     * sys/role/list
     *
     */
    @RequestMapping(value = "/role/list",method = RequestMethod.GET)
    public Result findRoleList(){
        List<PeRole> roleList= peRoleService.findRoleList();
        return new Result(ResultCode.SUCCESS,roleList);
    }

    /**
     * 根据用户id获取全部权限
     * @param id
     * @return
     */
    @RequestMapping(value = "/role/userId/{id}", method = RequestMethod.GET)
    public Result findRolesByUserId(@PathVariable(name = "id") String id) {
        PeRole byId = peRoleService.getById(id);
        return new Result(ResultCode.SUCCESS);
    }
    /**
     * user/assignRoles
     * PUT
     * 提交权限
     */
    @RequestMapping(value = "/user/assignRoles",method = RequestMethod.PUT)
    public Result addRolesAndAssign(){
        //根据id查询员工，
        //员工添加对应的角色
        return new Result(ResultCode.SUCCESS,"");
    }



    /**
     * 公司设置
     */
    /**
     * 角色管理详情 完成
     * GET /sys/role
     * 接口ID：104972469
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104972469
     */
    @RequestMapping(value = "/role", method = RequestMethod.GET)
    public Result rolePage(@RequestParam(defaultValue = "1") Integer page,
                           @RequestParam(defaultValue = "30") Integer pageSize) {
        PageResult rolePage = peRoleService.getRolePage(page, pageSize);
        return new Result(ResultCode.SUCCESS, rolePage);
    }
    /**
     * 分配权限提交
     * role/assignPrem
     * PUT
     */
    @RequestMapping(value = "/role/assignPrem",method = RequestMethod.PUT)
    public Result roleToPrem(){

        return new Result(ResultCode.SUCCESS,"");
    }
    /**
 * 新增角色 完成
 * role
 */
@RequestMapping(value = "/role",method = RequestMethod.POST)
public Result roleToAdd(@RequestBody PeRole role){
    long id = idWorker.nextId();
    role.setId(id+"");
    role.setCompanyId("1");
    boolean save = peRoleService.save(role);
    if(save){
        return new Result(ResultCode.SUCCESS,save);
    }else {
        return new Result(ResultCode.FAIL,"添加角色失败");
    }

}
/**
 * 删除角色 完成
 * ole/1064099035536822276
 * DELETE
 */
@RequestMapping(value = "/role/{id}",method = RequestMethod.DELETE)
public Result roleToDel(@PathVariable("id") String id) {
    boolean save = peRoleService.removeById(id);
    if (save) {
        return new Result(ResultCode.SUCCESS, save);
    } else {
        return new Result(ResultCode.FAIL, "删除角色失败");
    }
}
    /**
     * 分配权限详情(根据id选中的权限) ok
     * GET /sys/role/{id}
     * 1062944989845262336?id=1062944989845262336
     * 接口ID：104981401
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104981401
     */
    @RequestMapping(value = "/role/{id}", method = RequestMethod.GET)
    public Result findRoleId(@PathVariable("id") String id, String sid) {
        PeRolePermissionVO data = peRoleService.getRoleInfo(id);
        return new Result(ResultCode.SUCCESS, data);
    }
    /**
     * 分配权限详情全部权限显示
     *   GET /sys/permission
     *   接口ID：107683477
     *   接口地址：https://app.apifox.com/link/project/3177061/apis/api-107683477
     * @param type
     * @param enVisible
     * @return
     */

    @GetMapping("/permission")
    public Result getPermissionTypeEnVisible(@RequestParam(defaultValue = "0") Integer type,@RequestParam(defaultValue = "1") Integer enVisible) {
      List<PePermission> permissions= pePermissionService.getPermissionInfo(type,enVisible);
        return new Result(ResultCode.SUCCESS,permissions);
    }

    /**
     *修改角色 成功
     * PUT
     */
    @RequestMapping(value = "/role/{id}", method = RequestMethod.PUT)
    public Result roleToPut(@PathVariable(name = "id") String id, @RequestBody PeRole role){
        role.setCompanyId("1");
        UpdateWrapper<PeRole> updateWrapper=new UpdateWrapper<>();
        updateWrapper.set("name",role.getName()).set("description",role.getDescription()).eq("id",role.getId());
        boolean update = peRoleService.update(updateWrapper);
        if(update){
         return new Result(ResultCode.SUCCESS,role);
     }
        return new Result(ResultCode.FAIL);

    }
}

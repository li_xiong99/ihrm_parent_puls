package com.kun.ihrm.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.model.system.PeRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
//import com.kun.ihrm.system.entity.PeRole;

/**
 * PeRoleMapper接口
 * @packageName system.com.kun.ihrm
 * @fileName PeRoleMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Mapper
public interface PeRoleMapper extends BaseMapper<PeRole> {
  List<PeRole> findByRolePage(@Param("page") Integer page, @Param("pageSize") Integer pageSize);

  List<PeRole> findRoleList();

    int updateToRole(@Param("peRole") PeRole peRole);

//  List<PeRolePermission> findByRoleInfo(@Param("sid") String sid);

}

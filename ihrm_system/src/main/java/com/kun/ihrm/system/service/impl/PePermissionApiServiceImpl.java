package com.kun.ihrm.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.system.entity.PePermissionApi;
import com.kun.ihrm.system.mapper.PePermissionApiMapper;
import com.kun.ihrm.system.service.PePermissionApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * PePermissionApiService接口实现
 * @packageName system.com.kun.ihrm
 * @fileName PePermissionApiServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Service
public class PePermissionApiServiceImpl 
				extends ServiceImpl<PePermissionApiMapper, PePermissionApi> 
				implements PePermissionApiService { 

    private static final Logger LOGGER = LoggerFactory.getLogger(PePermissionApiServiceImpl.class);


}

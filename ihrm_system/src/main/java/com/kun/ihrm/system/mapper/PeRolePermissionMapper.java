package com.kun.ihrm.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.system.vo.PeRolePermission;

public interface PeRolePermissionMapper extends BaseMapper<PeRolePermission> {

}

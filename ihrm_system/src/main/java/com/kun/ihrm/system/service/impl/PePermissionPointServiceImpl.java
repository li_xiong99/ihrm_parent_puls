package com.kun.ihrm.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.system.entity.PePermissionPoint;
import com.kun.ihrm.system.mapper.PePermissionPointMapper;
import com.kun.ihrm.system.service.PePermissionPointService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * PePermissionPointService接口实现
 * @packageName system.com.kun.ihrm
 * @fileName PePermissionPointServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Service
public class PePermissionPointServiceImpl 
				extends ServiceImpl<PePermissionPointMapper, PePermissionPoint> 
				implements PePermissionPointService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PePermissionPointServiceImpl.class);


}

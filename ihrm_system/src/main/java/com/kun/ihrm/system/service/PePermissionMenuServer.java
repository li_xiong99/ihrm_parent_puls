package com.kun.ihrm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.system.entity.PePermissionMenu;


public interface PePermissionMenuServer extends IService<PePermissionMenu> {
}

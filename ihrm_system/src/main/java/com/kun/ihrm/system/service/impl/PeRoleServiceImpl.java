package com.kun.ihrm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.model.system.PeRole;
import com.kun.ihrm.system.mapper.PeRoleMapper;
import com.kun.ihrm.system.mapper.PeRolePermissionMapper;
import com.kun.ihrm.system.service.PeRoleService;
import com.kun.ihrm.system.vo.PeRolePermission;
import com.kun.ihrm.system.vo.PeRolePermissionVO;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * PeRoleService接口实现
 * @packageName system.com.kun.ihrm
 * @fileName PeRoleServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Service
public class PeRoleServiceImpl 
				extends ServiceImpl<PeRoleMapper, PeRole>
				implements PeRoleService { 

    private static final Logger LOGGER = LoggerFactory.getLogger(PeRoleServiceImpl.class);

@Resource
private PeRoleMapper peRoleMapper;


@Resource
private PeRolePermissionMapper voMapper;

	@Override
	public PageResult getRolePage(Integer page, Integer pageSize) {
		PageResult result=new PageResult();
		result.setRows(peRoleMapper.findByRolePage((page-1)*pageSize,pageSize));
		return result;
	}

	@Override
	public PeRolePermissionVO getRoleInfo(String rid) {
		PeRolePermissionVO vo = new PeRolePermissionVO();
		PeRole peRole = peRoleMapper.selectById(rid);

		try {
			BeanUtils.copyProperties(vo,peRole);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		List<PeRolePermission> permIds = voMapper.selectList(new QueryWrapper<PeRolePermission>().eq("role_id", rid));
		List<String> collect = permIds.stream()
				.map(PeRolePermission::getPermissionId)
				.collect(Collectors.toList());
		vo.setPermIds(collect);
		return vo;
	}

	@Override
	public List<PeRole> findRoleList() {
		return  peRoleMapper.findRoleList();
	}

	@Override
	public int updateToRole(PeRole peRole) {
		return peRoleMapper.updateToRole(peRole);
	}
}

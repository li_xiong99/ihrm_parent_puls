package com.kun.ihrm.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * EmuserCompanyPersonal实体类
 * @packageName .main.java
 * @fileName EmuserCompanyPersonal.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
@Data
@TableName("em_user_company_personal")
public class EmUserCompanyPersonal {

  /** 
   * 用户ID
   */
  @TableId(value = "user_id", type = IdType.AUTO)
  private String UserId;

  @TableField("username")
  private String username;

  @TableField("mobile")
  private String mobile;

  @TableField("time_of_entry")
  private String timeOfEntry;

  @TableField("department_name")
  private String departmentName;
  /** 
   * 公司ID
   */
  @TableField("company_id")
  private String companyId;
  /** 
   * 性别
   */
  @TableField("sex")
  private String sex;
  /** 
   * 出生日期
   */
  @TableField("date_of_birth")
  private String dateOfBirth;
  /** 
   * 最高学历
   */
  @TableField("the_highest_degree_of_education")
  private String theHighestDegreeOfEducation;
  /** 
   * 国家地区
   */
  @TableField("national_area")
  private String nationalArea;
  /** 
   * 护照号
   */
  @TableField("passport_no")
  private String passportNo;
  /** 
   * 身份证号
   */
  @TableField("id_number")
  private String idNumber;
  /** 
   * 身份证照片-正面
   */
  @TableField("id_card_photo_positive")
  private String idCardPhotoPositive;
  /** 
   * 身份证照片-背面
   */
  @TableField("id_card_photo_back")
  private String idCardPhotoBack;
  /** 
   * 籍贯
   */
  @TableField("native_place")
  private String nativePlace;
  /** 
   * 民族
   */
  @TableField("nation")
  private String nation;
  /** 
   * 英文名
   */
  @TableField("english_name")
  private String englishName;
  /** 
   * 婚姻状况
   */
  @TableField("marital_status")
  private String maritalStatus;
  /** 
   * 员工照片
   */
  @TableField("staff_photo")
  private String staffPhoto;
  /** 
   * 生日
   */
  @TableField("birthday")
  private String birthday;
  /** 
   * 属相
   */
  @TableField("zodiac")
  private String zodiac;
  /** 
   * 年龄
   */
  @TableField("age")
  private String age;
  /** 
   * 星座
   */
  @TableField("constellation")
  private String constellation;
  /** 
   * 血型
   */
  @TableField("blood_type")
  private String bloodType;
  /** 
   * 户籍所在地
   */
  @TableField("domicile")
  private String domicile;
  /** 
   * 政治面貌
   */
  @TableField("political_outlook")
  private String politicalOutlook;
  /** 
   * 入党时间
   */
  @TableField("time_to_join_the_party")
  private String timeToJoinTheParty;
  /** 
   * 存档机构
   */
  @TableField("archiving_organization")
  private String archivingOrganization;
  /** 
   * 子女状态
   */
  @TableField("state_of_children")
  private String stateOfChildren;
  /** 
   * 子女有无商业保险
   */
  @TableField("do_children_have_commercial_insurance")
  private String doChildrenHaveCommercialInsurance;
  /** 
   * 有无违法违纪行为
   */
  @TableField("is_there_any_violation_of_law_or_discipline")
  private String isThereAnyViolationOfLawOrDiscipline;
  /** 
   * 有无重大病史
   */
  @TableField("are_there_any_major_medical_histories")
  private String areThereAnyMajorMedicalHistories;
  /** 
   * QQ
   */
  @TableField("qq")
  private String qq;
  /** 
   * 微信
   */
  @TableField("wechat")
  private String wechat;
  /** 
   * 居住证城市
   */
  @TableField("residence_card_city")
  private String residenceCardCity;
  /** 
   * 居住证办理日期
   */
  @TableField("date_of_residence_permit")
  private String dateOfResidencePermit;
  /** 
   * 居住证截止日期
   */
  @TableField("residence_permit_deadline")
  private String residencePermitDeadline;
  /** 
   * 现居住地
   */
  @TableField("place_of_residence")
  private String placeOfResidence;
  /** 
   * 通讯地址
   */
  @TableField("postal_address")
  private String postalAddress;

  @TableField("contact_the_mobile_phone")
  private String contactTheMobilePhone;

  @TableField("personal_mailbox")
  private String personalMailbox;
  /** 
   * 紧急联系人
   */
  @TableField("emergency_contact")
  private String emergencyContact;
  /** 
   * 紧急联系电话
   */
  @TableField("emergency_contact_number")
  private String emergencyContactNumber;
  /** 
   * 社保电脑号
   */
  @TableField("social_security_computer_number")
  private String socialSecurityComputerNumber;
  /** 
   * 公积金账号
   */
  @TableField("provident_fund_account")
  private String providentFundAccount;
  /** 
   * 银行卡号
   */
  @TableField("bank_card_number")
  private String bankCardNumber;
  /** 
   * 开户行
   */
  @TableField("opening_bank")
  private String openingBank;
  /** 
   * 学历类型
   */
  @TableField("educational_type")
  private String educationalType;
  /** 
   * 毕业学校
   */
  @TableField("graduate_school")
  private String graduateSchool;
  /** 
   * 入学时间
   */
  @TableField("enrolment_time")
  private String enrolmentTime;
  /** 
   * 毕业时间
   */
  @TableField("graduation_time")
  private String graduationTime;
  /** 
   * 专业
   */
  @TableField("major")
  private String major;
  /** 
   * 毕业证书
   */
  @TableField("graduation_certificate")
  private String graduationCertificate;
  /** 
   * 学位证书
   */
  @TableField("certificate_of_academic_degree")
  private String certificateOfAcademicDegree;
  /** 
   * 上家公司
   */
  @TableField("home_company")
  private String homeCompany;
  /** 
   * 职称
   */
  @TableField("title")
  private String title;
  /** 
   * 简历
   */
  @TableField("resume")
  private String resume;
  /** 
   * 有无竞业限制
   */
  @TableField("is_there_any_competition_restriction")
  private String isThereAnyCompetitionRestriction;
  /** 
   * 前公司离职证明
   */
  @TableField("proof_of_departure_of_former_company")
  private String proofOfDepartureOfFormerCompany;
  /** 
   * 备注
   */
  @TableField("remarks")
  private String remarks;

  /**
   * 工号
   */
  @TableField(exist = false)
  private String workNumber;
  /**
   * 在职状态
   */
  @TableField(exist = false)
  private String inServiceStatus;

}

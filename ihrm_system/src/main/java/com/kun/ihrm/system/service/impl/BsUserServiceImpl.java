package com.kun.ihrm.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.common.utils.IdWorker;
import com.kun.ihrm.model.company.CoDepartment;
import com.kun.ihrm.model.system.BsUser;
import com.kun.ihrm.system.client.CompanyFeignClient;
import com.kun.ihrm.system.mapper.BsUserMapper;
import com.kun.ihrm.system.service.BsUserService;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * BsUserService接口实现
 * @packageName system.com.kun.ihrm.copy
 * @fileName BsUserServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月30日 下午7:59:44
 * @lastModify 2023年8月30日 下午7:59:44
 * @version 1.0.0
 */
@Service
public class BsUserServiceImpl 
				extends ServiceImpl<BsUserMapper, BsUser>
				implements BsUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BsUserServiceImpl.class);

	@Resource
	BsUserMapper userMapper;

    @Resource
    private IdWorker idWorker;

    @Resource
    private CompanyFeignClient companyFeignClient;


	@Override
	public BsUser findByMobile(String mobile) {
		return userMapper.selectMobile(mobile);
	}

	@Override
	public PageResult getByPageList(Integer page, Integer pageSize) {
		PageResult pageUtils=new PageResult();
		pageUtils.setRows(userMapper.selectByPageList((page-1)*pageSize,pageSize));
		return pageUtils;
	}

    /**
     *  批量用户保存
     * @param list  用户list
     * @param companyId 用户所属公司id
     * @param companyName   用户所属公司名称
     */
    @Transactional
    public void saveAll(List<BsUser> list, String companyId, String companyName) {
        for (BsUser user : list) {
            //默认密码
            user.setPassword(new Md5Hash("123456" , user.getMobile() , 3).toString());
            //id
            user.setId(idWorker.nextId() + "");
            //基本属性
            user.setCompanyId(companyId);
            user.setCompanyName(companyName);
            user.setInServiceStatus(1);
            user.setEnableState(1);
            user.setCreateTime(new Date());
            user.setLevel("user");


            //填充部门的属性
            CoDepartment department = companyFeignClient.findByCode(user.getDepartmentId(), companyId);
            if (department != null){
                user.setDepartmentId(department.getId());
                user.setDepartmentName(department.getName());
            }

            userMapper.insert(user);
        }
    }

//	@Override
//	public BsUser getByUserInfo(String id) {
//		return userMapper.getByUserInfo(id);
//	}

}

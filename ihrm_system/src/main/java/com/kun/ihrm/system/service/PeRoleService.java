package com.kun.ihrm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.model.system.PeRole;
import com.kun.ihrm.system.vo.PeRolePermissionVO;

import java.util.List;


/**
 * PeRoleService接口
 * @packageName system.com.kun.ihrm
 * @fileName PeRoleService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
public interface PeRoleService extends IService<PeRole>{
    PageResult getRolePage(Integer page, Integer pageSize);

    PeRolePermissionVO getRoleInfo(String sid);

    List<PeRole> findRoleList();

    int updateToRole(PeRole peRole);
}

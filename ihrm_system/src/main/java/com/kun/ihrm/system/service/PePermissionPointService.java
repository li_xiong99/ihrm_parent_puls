package com.kun.ihrm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.system.entity.PePermissionPoint;


/**
 * PePermissionPointService接口
 * @packageName system.com.kun.ihrm
 * @fileName PePermissionPointService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
public interface PePermissionPointService extends IService<PePermissionPoint>{


}

package com.kun.ihrm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.model.system.PePermission;

import java.util.List;
import java.util.Map;

//import com.kun.ihrm.system.entity.PePermission;


/**
 * PePermissionService接口
 * @packageName system.com.kun.ihrm
 * @fileName PePermissionService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
public interface PePermissionService extends IService<PePermission>{

    /**
     * 查询全部用户列表
     * type     :   查询全部权限列表
     *          0 : 菜单 + 按钮(权限点)    1 ： 菜单  2 : 按钮(权限点) 3 ： API接口
     * enVisible :
     *          0 ： 查询SaaS平台的最高权限   1 ： 查询企业的权限
     */
    List<PePermission> findAll(Map<String, Object> map);

    public List<PePermission> getPePermission(String type, String pid);

    List<PePermission> getPermissionInfo(Integer type, Integer enVisible);

//    List<PePermission> getPermissionInfo();
}

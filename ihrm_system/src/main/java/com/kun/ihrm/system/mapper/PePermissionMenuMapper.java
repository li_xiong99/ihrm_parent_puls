package com.kun.ihrm.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.system.entity.PePermissionMenu;


/**
 * BsUserMapper接口
 * @packageName system.com.kun.ihrm.copy
 * @fileName BsUserMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月30日 下午7:59:44
 * @lastModify 2023年8月30日 下午7:59:44
 * @version 1.0.0
 */
public interface PePermissionMenuMapper extends BaseMapper<PePermissionMenu> {

}

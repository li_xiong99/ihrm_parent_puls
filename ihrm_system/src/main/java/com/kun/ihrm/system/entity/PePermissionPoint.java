package com.kun.ihrm.system.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * PePermissionPoint实体类
 * @packageName system.com.kun.ihrm
 * @fileName PePermissionPoint.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Data
@TableName("pe_permission_point")
public class PePermissionPoint {

  /** 
   * 主键ID
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 按钮类型
   */
  @TableField("point_class")
  private String pointClass;
  /** 
   * 按钮icon
   */
  @TableField("point_icon")
  private String pointIcon;
  /** 
   * 状态
   */
  @TableField("point_status")
  private Integer pointStatus;

}

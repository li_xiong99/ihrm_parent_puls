package com.kun.ihrm.system.controller;


import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import com.kun.ihrm.common.utils.IdWorker;
import com.kun.ihrm.model.system.PePermission;
import com.kun.ihrm.system.entity.PePermissionMenu;
import com.kun.ihrm.system.service.PePermissionMenuServer;
import com.kun.ihrm.system.service.PePermissionService;
import com.kun.ihrm.system.vo.PePermissionVO;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/sys")
public class PePermissionController {
    @Autowired
    PePermissionService pePermissionService;

    @Autowired
    PePermissionMenuServer pePermissionMenuServer;

    @Resource
    IdWorker idWorker;


    /**
     * 权限删除
     * DELETE http://localhost:8080/api/sys/permission/1156085524669272064
     * 接口ID：105051715
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105051715
     *
     * @param id
     * @return
     */
    @DeleteMapping("/permission/{id}")
    public Result delete(@PathVariable("id") String id) {
        boolean b = pePermissionService.removeById(id);
        return new Result(ResultCode.SUCCESS, b);
    }


    /**
     * 查看子权限
     * GET http://localhost:8080/api/sys/permission
     * 接口ID：105062766
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-105062766
     *
     * @param type
     * @param pid
     * @return
     */
//    @GetMapping("/permission")
//    public Result getPermission(String type, String pid) {
//        List<PePermission> list = pePermissionService.getPePermission(type, pid);
//        return new Result(ResultCode.SUCCESS, list);
//    }

    /**
     * 添加api权限
     * POST http://localhost:8080/api/sys/permission
     * 接口ID：104903915
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104903915
     *
     * @param permissionp
     * @return
     */
    @PostMapping("/permission")
    public Result addPermission(@RequestBody PePermission permissionp) {
        String id = idWorker.nextId() + "";
        permissionp.setId(id);
        boolean save = pePermissionService.save(permissionp);
        return new Result(ResultCode.SUCCESS, save);
    }

    /**
     * 查看
     * GET http://localhost:8080/api/sys/permission/1
     * 接口ID：104904409
     * 接口地址：https://app.apifox.com/link/project/3177061/apis/api-104904409
     *
     * @param id
     * @return
     */
    @GetMapping("/permission/{id}")
    public Result getPePermissionVO(@PathVariable("id") String id) {
        PePermission byId = pePermissionService.getById(id);
        PePermissionMenu byId1 = pePermissionMenuServer.getById(byId.getId());
        PePermissionVO vo = new PePermissionVO();
        try {
            BeanUtils.copyProperties(vo, byId);
            vo.setMenuIcon(byId1.getMenu_icon());
            return new Result(ResultCode.SUCCESS, vo);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return new Result(ResultCode.SUCCESS, vo);
    }
}

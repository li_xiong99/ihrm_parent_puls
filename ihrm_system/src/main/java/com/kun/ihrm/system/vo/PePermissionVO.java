package com.kun.ihrm.system.vo;

import com.kun.ihrm.model.system.PePermission;




public class PePermissionVO extends PePermission {

   private String  menuIcon ;



   public String getMenuIcon() {
      return menuIcon;
   }

   public void setMenuIcon(String menuIcon) {
      this.menuIcon = menuIcon;
   }
}

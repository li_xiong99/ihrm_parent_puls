package com.kun.ihrm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.model.system.BsUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * BsUserService接口
 * @packageName system.com.kun.ihrm.copy
 * @fileName BsUserService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月30日 下午7:59:44
 * @lastModify 2023年8月30日 下午7:59:44
 * @version 1.0.0
 */
public interface BsUserService extends IService<BsUser> {

    //根据手机号查询用户
    BsUser findByMobile(String mobile);
//查询员工列表
PageResult getByPageList(@Param("page") Integer page, @Param("pageSize") Integer pageSize);

    void saveAll(List<BsUser> list, String companyId, String companyName);

//    BsUser getByUserInfo(@Param("id") String id);

}

package com.kun.ihrm.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.system.entity.EmUserCompanyPersonal;

/**
 * EmuserCompanyPersonalService接口
 * @packageName .main.java
 * @fileName EmUserCompanyPersonalService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:50:21
 * @lastModify 2023年8月25日 上午10:50:21
 * @version 1.0.0
 */
public interface EmUserCompanyPersonalService extends IService<EmUserCompanyPersonal> {

    PageResult getByPageList(Integer page, Integer pageSize);
//    PageResult getByPageList(Integer page, Integer pageSize,String companyId);
}

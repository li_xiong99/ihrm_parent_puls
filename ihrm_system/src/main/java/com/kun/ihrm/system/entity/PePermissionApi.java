package com.kun.ihrm.system.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * PePermissionApi实体类
 * @packageName system.com.kun.ihrm
 * @fileName PePermissionApi.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Data
@TableName("pe_permission_api")
public class PePermissionApi {

  /** 
   * 主键ID
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 权限等级，1为通用接口权限，2为需校验接口权限
   */
  @TableField("api_level")
  private String apiLevel;
  /** 
   * 请求类型
   */
  @TableField("api_method")
  private String apiMethod;
  /** 
   * 链接
   */
  @TableField("api_url")
  private String apiUrl;

}

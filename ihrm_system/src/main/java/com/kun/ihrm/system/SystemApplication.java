package com.kun.ihrm.system;

import com.kun.ihrm.common.utils.IdWorker;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

/**
 * @author: hyl
 * @date: 2020/01/05
 **/
//1.配置springboot的包扫描
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
@MapperScan("com.kun.ihrm.system.mapper")
public class SystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(SystemApplication.class,args);
    }

    @Bean
public IdWorker IdWorker(){
        return new IdWorker();
}
}

package com.kun.ihrm.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.system.entity.PePermissionMenu;
import com.kun.ihrm.system.mapper.PePermissionMenuMapper;
import com.kun.ihrm.system.service.PePermissionMenuServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
public class PePermissionMenuServerImpl
        extends ServiceImpl<PePermissionMenuMapper, PePermissionMenu>
        implements PePermissionMenuServer {



    private static final Logger LOGGER = LoggerFactory.getLogger(PePermissionPointServiceImpl.class);

}

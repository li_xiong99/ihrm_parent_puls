package com.kun.ihrm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.model.system.PePermission;
import com.kun.ihrm.system.mapper.PePermissionMapper;
import com.kun.ihrm.system.service.PePermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * PePermissionService接口实现
 * @packageName system.com.kun.ihrm
 * @fileName PePermissionServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 下午12:06:25
 * @lastModify 2023年8月25日 下午12:06:25
 * @version 1.0.0
 */
@Service
public class PePermissionServiceImpl 
				extends ServiceImpl<PePermissionMapper, PePermission>
				implements PePermissionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PePermissionServiceImpl.class);


	@Resource
	PePermissionMapper pePermissionMapper;


	/**
	 * 查询全部用户列表
	 * type     :   查询全部权限列表
	 *          0 : 菜单 + 按钮(权限点)    1 ： 菜单  2 : 按钮(权限点) 3 ： API接口
	 * enVisible :
	 *          0 ： 查询SaaS平台的最高权限   1 ： 查询企业的权限
	 */
	public List<PePermission> findAll(Map<String, Object> map) {
		QueryWrapper<PePermission> queryWrapper = new QueryWrapper<>();

		// 根据父id查询
		if (!StringUtils.isEmpty(map.get("pid"))) {
			queryWrapper.eq("pid", map.get("pid"));
		}

		// 根据enVisible查询
		if (!StringUtils.isEmpty(map.get("enVisible"))) {
			queryWrapper.eq("en_visible", map.get("enVisible"));
		}

		// 根据类型type进行查询
		if (!StringUtils.isEmpty(map.get("type"))) {
			String type = (String) map.get("type");
			if ("0".equals(type)) {
				queryWrapper.in("type", 1, 2);
			} else {
				queryWrapper.eq("type", Integer.parseInt(type));
			}
		}

		return pePermissionMapper.selectList(queryWrapper);
	}

	@Override
	public List<PePermission> getPePermission(String type, String pid) {

		QueryWrapper<PePermission> wrapper = new QueryWrapper<>();
		if (!com.alibaba.nacos.client.utils.StringUtils.isEmpty(type)) {
			wrapper.and((obj) -> {
				obj.eq("type", type);
			});
		}
		if (com.alibaba.nacos.client.utils.StringUtils.isEmpty(pid)) {
			wrapper.and((obj) -> {
				obj.eq("pid", pid);
			});
		}
		List<PePermission> list = this.list(wrapper);
		return list;
	}

	@Override
	public List<PePermission> getPermissionInfo(Integer type, Integer enVisible) {
		return pePermissionMapper.getPermissionInfo(type,enVisible);
	}

//	@Override
//	public List<PePermission> getPermissionInfo() {
//		return null;
//	}

}

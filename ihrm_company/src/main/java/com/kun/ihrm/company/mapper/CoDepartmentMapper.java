package com.kun.ihrm.company.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.model.company.CoDepartment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * CoDepartmentMapper接口
 * @packageName com.kun.ihrm
 * @fileName CoDepartmentMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:41:04
 * @lastModify 2023年8月25日 上午10:41:04
 * @version 1.0.0
 */
@Mapper
public interface CoDepartmentMapper extends BaseMapper<CoDepartment> {

    CoDepartment selectDepartment(@Param("code") String code, @Param("companyId") String companyId);

    List<CoDepartment> selectDepartmentCompanyId(@Param("companyId") String companyId);
}

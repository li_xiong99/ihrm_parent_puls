package com.kun.ihrm.company.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.model.company.CoCompany;
import org.apache.ibatis.annotations.Mapper;

/**
 * CoCompanyMapper接口
 * @packageName com.kun.ihrm
 * @fileName CoCompanyMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:41:04
 * @lastModify 2023年8月25日 上午10:41:04
 * @version 1.0.0
 */
@Mapper
public interface CoCompanyMapper extends BaseMapper<CoCompany> {

}

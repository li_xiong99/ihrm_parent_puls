package com.kun.ihrm.company.controller;

import com.kun.ihrm.common.controller.BaseController;
import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import com.kun.ihrm.company.service.CoCompanyService;
import com.kun.ihrm.company.service.CoDepartmentService;
import com.kun.ihrm.company.vo.CoDepartmentVo;
import com.kun.ihrm.model.company.CoCompany;
import com.kun.ihrm.model.company.CoDepartment;
import com.kun.ihrm.model.company.response.DeptListResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: hyl
 * @date: 2020/01/04
 **/
//解决跨域
@CrossOrigin
@RestController
@RequestMapping(value = "/company")
public class DepartmentController extends BaseController {

    @Resource
    private CoDepartmentService departmentService;

    @Resource
    private CoCompanyService companyService;


    /**
     *  根据部门编码和公司id查询部门
     */
    @RequestMapping(value = "/department/search" , method = RequestMethod.POST)
    public CoDepartment findByCode(@RequestParam("code") String code, @RequestParam("companyId") String companyId){
        return departmentService.findByCode(code , companyId);
    }

    /**
     * 查询企业的部门列表
     * 指定企业id
     */
    @RequestMapping(value = "/department",method = RequestMethod.GET)
    public Result findDepartment(){
        //公司信息           //从BaseController里获取session中的安全数据 里面的公司id
        CoCompany company = companyService.getById(companyId);
        //公司id查询公司部门
        List<CoDepartment> department = departmentService.selectDepartment(companyId);
        //合并数据返回
        DeptListResult deptListResult = new DeptListResult(company,department);

        return new Result(ResultCode.SUCCESS,deptListResult);
    }

    /**
     * 公司部门添加或添加子部门
     */
    @RequestMapping(value = "/department",method = RequestMethod.POST)
    public Result addDepartment(@RequestBody CoDepartmentVo departmentVo){
        departmentVo.setCompanyId(companyId);
        int save = departmentService.saveDepartment(departmentVo);
        if(save > 0){
            return new Result(ResultCode.SUCCESS);
        }
        return new Result(ResultCode.FAIL);
    }

    /**
     * 删除子部门
     * @param id
     * @return
     */
    @RequestMapping(value = "/department/{id}",method = RequestMethod.DELETE)
    public Result deleteDepartment(@PathVariable("id") String id){
        boolean save = departmentService.removeById(id);
        if(save){
            return new Result(ResultCode.SUCCESS);
        }
        return new Result(ResultCode.FAIL);
    }

    /**
     * 查询部门信息
     */
    @RequestMapping(value = "/department/{id}",method = RequestMethod.GET)
    public Result selectDepartmentId(@PathVariable("id") String id){
        CoDepartment byId = departmentService.getById(id);
        return new Result(ResultCode.SUCCESS,byId);
    }

    /**
     * 修改子部门信息
     */
    @RequestMapping(value = "/department/{id}",method = RequestMethod.PUT)
    public Result updateDepartment(@PathVariable("id") String id,@RequestBody CoDepartmentVo departmentVo){
        int update = departmentService.updDepartment(id,departmentVo);
        if(update > 0){
            return new Result(ResultCode.SUCCESS);
        }
        return new Result(ResultCode.FAIL);
    }



}

package com.kun.ihrm.company.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.model.company.CoCompany;
import org.springframework.stereotype.Service;


/**
 * CoCompanyService接口
 * @packageName com.kun.ihrm
 * @fileName CoCompanyService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:41:04
 * @lastModify 2023年8月25日 上午10:41:04
 * @version 1.0.0
 */
@Service
public interface CoCompanyService extends IService<CoCompany> {


}

package com.kun.ihrm.company.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.common.utils.IdWorker;
import com.kun.ihrm.company.mapper.CoDepartmentMapper;
import com.kun.ihrm.company.service.CoDepartmentService;
import com.kun.ihrm.company.vo.CoDepartmentVo;
import com.kun.ihrm.model.company.CoDepartment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * CoDepartmentService接口实现
 * @packageName com.kun.ihrm
 * @fileName CoDepartmentServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:41:04
 * @lastModify 2023年8月25日 上午10:41:04
 * @version 1.0.0
 */
@Service
public class CoDepartmentServiceImpl 
				extends ServiceImpl<CoDepartmentMapper, CoDepartment>
				implements CoDepartmentService { 

    private static final Logger LOGGER = LoggerFactory.getLogger(CoDepartmentServiceImpl.class);

	@Resource
	CoDepartmentMapper departmentMapper;

	//部门id
	@Resource
	private IdWorker idWorker;

	/**
	 *  根据部门编码和公司id查询部门
	 */
	@Override
	public CoDepartment findByCode(String code, String companyId) {
		return departmentMapper.selectDepartment(code,companyId);
	}

	/**
	 * 初始化查询全部
	 * @param companyId
	 * @return
	 */
	@Override
	public List<CoDepartment> selectDepartment(String companyId) {
		return departmentMapper.selectDepartmentCompanyId(companyId);
	}
	/**
	 * 修改子部门信息
	 */
	@Override
	public int updDepartment(String id,CoDepartmentVo departmentVo) {
		CoDepartment department = new CoDepartment();

		department.setId(id);
		department.setName(departmentVo.getName());
		department.setCode(departmentVo.getCode());
		department.setIntroduce(departmentVo.getIntroduce());
		return departmentMapper.updateById(department);
	}
	/**
	 * 公司部门添加或添加子部门
	 */
	@Override
	public int saveDepartment(CoDepartmentVo departmentVo) {
		        //创建对象 Vo赋值实体
        CoDepartment coDepartment = new CoDepartment();
        //设置主键的值
        String id = idWorker.nextId()+"";
        coDepartment.setId(id);
        //部门编码
        coDepartment.setCode(departmentVo.getCode());
        //企业id
        coDepartment.setCompanyId(departmentVo.getCompanyId());
        //部门介绍
        coDepartment.setIntroduce(departmentVo.getIntroduce());
        //部门创建时间
        coDepartment.setCreateTime(new Date());
        //负责人
        coDepartment.setManager(departmentVo.getManager());
        //负责人id
        coDepartment.setManagerId("");
        //父级部门id
        coDepartment.setPid(departmentVo.getPid());
        //部门名称
        coDepartment.setName(departmentVo.getName());
		return departmentMapper.insert(coDepartment);
	}

}

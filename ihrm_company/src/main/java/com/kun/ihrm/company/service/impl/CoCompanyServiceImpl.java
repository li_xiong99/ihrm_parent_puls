package com.kun.ihrm.company.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.company.mapper.CoCompanyMapper;
import com.kun.ihrm.company.service.CoCompanyService;
import com.kun.ihrm.model.company.CoCompany;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * CoCompanyService接口实现
 * @packageName com.kun.ihrm
 * @fileName CoCompanyServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:41:04
 * @lastModify 2023年8月25日 上午10:41:04
 * @version 1.0.0
 */
@Service
public class CoCompanyServiceImpl 
				extends ServiceImpl<CoCompanyMapper, CoCompany>
				implements CoCompanyService { 

    private static final Logger LOGGER = LoggerFactory.getLogger(CoCompanyServiceImpl.class);


}

//package com.kun.ihrm.company.entity;
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import lombok.Data;
//import java.util.Date;
//
//
///**
// * CoDepartment实体类
// * @packageName com.kun.ihrm
// * @fileName CoDepartment.java
// * @author 科泰教育 (http://www.ktjiaoyu.com)
// * @createTime 2023年8月25日 上午10:41:04
// * @lastModify 2023年8月25日 上午10:41:04
// * @version 1.0.0
// */
//@Data
//@TableName("co_department")
//public class CoDepartment {
//
//
//  @TableId(value = "id", type = IdType.AUTO)
//  private String id;
//  /**
//   * 企业ID
//   */
//  @TableField("company_id")
//  private String companyId;
//  /**
//   * 父级部门ID
//   */
//  @TableField("pid")
//  private String pid;
//  /**
//   * 部门名称
//   */
//  @TableField("name")
//  private String name;
//  /**
//   * 部门编码
//   */
//  @TableField("code")
//  private String code;
//  /**
//   * 部门负责人
//   */
//  @TableField("manager")
//  private String manager;
//  /**
//   * 介绍
//   */
//  @TableField("introduce")
//  private String introduce;
//  /**
//   * 创建时间
//   */
//  @TableField("create_time")
//  private Date createTime;
//  /**
//   * 负责人ID
//   */
//  @TableField("manager_id")
//  private String managerId;
//
//}

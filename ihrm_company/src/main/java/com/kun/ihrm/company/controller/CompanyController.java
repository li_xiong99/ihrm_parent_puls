package com.kun.ihrm.company.controller;

import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import com.kun.ihrm.company.service.CoCompanyService;
import com.kun.ihrm.model.company.CoCompany;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


//解决跨域问题
@CrossOrigin
@RestController
@RequestMapping(value="/company")
public class CompanyController {

    @Resource
    private CoCompanyService companyService;

    //根据id查询企业
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public Result findCompanyById(@PathVariable(value="id") String id){
        CoCompany company = companyService.getById(id);
        Result result = new Result(ResultCode.SUCCESS);
        result.setData(company);
        return result;
    }


}

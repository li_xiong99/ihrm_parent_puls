package com.kun.ihrm.company.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.company.vo.CoDepartmentVo;
import com.kun.ihrm.model.company.CoDepartment;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * CoDepartmentService接口
 * @packageName com.kun.ihrm
 * @fileName CoDepartmentService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:41:04
 * @lastModify 2023年8月25日 上午10:41:04
 * @version 1.0.0
 */
@Service
public interface CoDepartmentService extends IService<CoDepartment> {

    /**
     *  根据部门编码和公司id查询部门
     */
    CoDepartment findByCode(String code, String companyId);

    /**
     * 初始化查询全部
     */
    List<CoDepartment> selectDepartment(String companyId);
    /**
     * 修改子部门信息
     */
    int updDepartment(String id, CoDepartmentVo departmentVo);
    /**
     * 公司部门添加或添加子部门
     */
    int saveDepartment(CoDepartmentVo departmentVo);
}

//package com.kun.ihrm.company.entity;
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import lombok.Data;
//import java.util.Date;
//
//import java.util.Date;
//
//import java.util.Date;
//
//
///**
// * CoCompany实体类
// * @packageName com.kun.ihrm
// * @fileName CoCompany.java
// * @author 科泰教育 (http://www.ktjiaoyu.com)
// * @createTime 2023年8月25日 上午10:41:04
// * @lastModify 2023年8月25日 上午10:41:04
// * @version 1.0.0
// */
//@Data
//@TableName("co_company")
//public class CoCompany {
//
//  /**
//   * ID
//   */
//  @TableField("id")
//  private String id;
//  /**
//   * 公司名称
//   */
//  @TableField("name")
//  private String name;
//  /**
//   * 企业登录账号ID
//   */
//  @TableField("manager_id")
//  private String managerId;
//  /**
//   * 当前版本
//   */
//  @TableField("version")
//  private String version;
//  /**
//   * 续期时间
//   */
//  @TableField("renewal_date")
//  private Date renewalDate;
//  /**
//   * 到期时间
//   */
//  @TableField("expiration_date")
//  private Date expirationDate;
//  /**
//   * 公司地区
//   */
//  @TableField("company_area")
//  private String companyArea;
//  /**
//   * 公司地址
//   */
//  @TableField("company_address")
//  private String companyAddress;
//  /**
//   * 营业执照-图片ID
//   */
//  @TableField("business_license_id")
//  private String businessLicenseId;
//  /**
//   * 法人代表
//   */
//  @TableField("legal_representative")
//  private String legalRepresentative;
//  /**
//   * 公司电话
//   */
//  @TableField("company_phone")
//  private String companyPhone;
//  /**
//   * 邮箱
//   */
//  @TableField("mailbox")
//  private String mailbox;
//  /**
//   * 公司规模
//   */
//  @TableField("company_size")
//  private String companySize;
//  /**
//   * 所属行业
//   */
//  @TableField("industry")
//  private String industry;
//  /**
//   * 备注
//   */
//  @TableField("remarks")
//  private String remarks;
//  /**
//   * 审核状态
//   */
//  @TableField("audit_state")
//  private String auditState;
//  /**
//   * 状态
//   */
//  @TableField("state")
//  private Integer state;
//  /**
//   * 当前余额
//   */
//  @TableField("balance")
//  private Double balance;
//  /**
//   * 创建时间
//   */
//  @TableField("create_time")
//  private Date createTime;
//
//}

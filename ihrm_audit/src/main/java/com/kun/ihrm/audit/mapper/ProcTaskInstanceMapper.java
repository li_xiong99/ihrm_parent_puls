package com.kun.ihrm.audit.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.audit.entity.ProcTaskInstance;

/**
 * ProcTaskInstanceMapper接口
 * @packageName com.kun.ihrm
 * @fileName ProcTaskInstanceMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 * @version 1.0.0
 */
public interface ProcTaskInstanceMapper extends BaseMapper<ProcTaskInstance> {

}

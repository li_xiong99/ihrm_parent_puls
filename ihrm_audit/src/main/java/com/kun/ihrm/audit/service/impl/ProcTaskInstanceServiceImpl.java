package com.kun.ihrm.audit.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.audit.entity.ProcTaskInstance;
import com.kun.ihrm.audit.mapper.ProcTaskInstanceMapper;
import com.kun.ihrm.audit.service.ProcTaskInstanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * ProcTaskInstanceService接口实现
 * @packageName com.kun.ihrm
 * @fileName ProcTaskInstanceServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 * @version 1.0.0common
 */
@Service
public class ProcTaskInstanceServiceImpl 
				extends ServiceImpl<ProcTaskInstanceMapper, ProcTaskInstance> 
				implements ProcTaskInstanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcTaskInstanceServiceImpl.class);


}

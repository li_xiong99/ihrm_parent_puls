package com.kun.ihrm.audit.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;


/**
 * ProcInstance实体类
 * @packageName com.kun.ihrm
 * @fileName ProcInstance.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 * @version 1.0.0
 */
@Data
@TableName("proc_instance")
public class ProcInstance {

  /** 
   * 流程实例ID
   */
  @TableId(value = "process_id", type = IdType.AUTO)
  private String processId;
  /** 
   * 流程标识
   */
  @TableField("process_key")
  private String processKey;
  /** 
   * 流程名称
   */
  @TableField("process_name")
  private String processName;
  /** 
   * 流程定义ID
   */
  @TableField("process_definition_id")
  private String processDefinitionId;
  /** 
   * 流程状态（0已提交；1审批中；2审批
通过；3审批不通过；4撤销）
   */
  @TableField("process_state")
  private String processState;
  /** 
   * 申请人ID
   */
  @TableField("user_id")
  private String userId;
  /** 
   * 申请人
   */
  @TableField("username")
  private String username;
  /** 
   * 申请时间
   */
  @TableField("proc_apply_time")
  private Date procApplyTime;
  /** 
   * 当前节点审批人ID
   */
  @TableField("proc_curr_node_user_id")
  private String procCurrNodeUserId;
  /** 
   * 当前节点审批人
   */
  @TableField("proc_curr_node_user_name")
  private String procCurrNodeUserName;
  /** 
   * 结束流程时间
   */
  @TableField("proc_end_time")
  private Date procEndTime;

  @TableField("proc_data")
  private String procData;

  @TableField("department_id")
  private String departmentId;

  @TableField("department_name")
  private String departmentName;

  @TableField("time_of_entry")
  private Date timeOfEntry;

}

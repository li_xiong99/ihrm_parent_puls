package com.kun.ihrm.audit.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;


/**
 * ProcUserGroup实体类
 * @packageName com.kun.ihrm
 * @fileName ProcUserGroup.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 * @version 1.0.0
 */
@Data
@TableName("proc_user_group")
public class ProcUserGroup {

  /** 
   * 主键
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 组名
   */
  @TableField("name")
  private String name;
  /** 
   * 入参
   */
  @TableField("param")
  private String param;
  /** 
   * 对应sql
   */
  @TableField("isql")
  private String isql;
  /** 
   * 有效标记
   */
  @TableField("isvalid")
  private String isvalid;
  /** 
   * 创建人
   */
  @TableField("create_user")
  private String createUser;
  /** 
   * 创建时间
   */
  @TableField("create_time")
  private Date createTime;
  /** 
   * 最后更新人
   */
  @TableField("update_user")
  private String updateUser;
  /** 
   * 最后更新时间
   */
  @TableField("update_time")
  private Date updateTime;

}

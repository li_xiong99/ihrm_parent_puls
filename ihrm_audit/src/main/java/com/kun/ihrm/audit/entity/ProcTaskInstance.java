package com.kun.ihrm.audit.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;


/**
 * ProcTaskInstance实体类
 * @packageName com.kun.ihrm
 * @fileName ProcTaskInstance.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 * @version 1.0.0
 */
@Data
@TableName("proc_task_instance")
public class ProcTaskInstance {

  /** 
   * 流程实例ID
   */
  @TableId(value = "process_id", type = IdType.AUTO)
  private String processId;
  /** 
   * 任务实例ID
   */
  @TableField("task_id")
  private String taskId;
  /** 
   * 任务节点key
   */
  @TableField("task_key")
  private String taskKey;
  /** 
   * 任务节点
   */
  @TableField("task_name")
  private String taskName;
  /** 
   * 应审批用户ID
   */
  @TableField("should_user_id")
  private String shouldUserId;
  /** 
   * 应审批用户
   */
  @TableField("should_user_name")
  private String shouldUserName;
  /** 
   * 实际处理用户ID
   */
  @TableField("handle_user_id")
  private String handleUserId;
  /** 
   * 实际处理用户
   */
  @TableField("handle_user_name")
  private String handleUserName;
  /** 
   * 处理时间
   */
  @TableField("handle_time")
  private Date handleTime;
  /** 
   * 处理意见
   */
  @TableField("handle_opinion")
  private String handleOpinion;
  /** 
   * 处理类型（2审批通过；3审批不通过；4
撤销）
   */
  @TableField("handle_type")
  private String handleType;

}

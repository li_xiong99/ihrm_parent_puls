package com.kun.ihrm.audit.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.audit.entity.ProcInstance;
import com.kun.ihrm.audit.mapper.ProcInstanceMapper;
import com.kun.ihrm.audit.service.ProcInstanceService;
import com.kun.ihrm.common.entity.PageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * ProcInstanceService接口实现
 *
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @version 1.0.0
 * @packageName com.kun.ihrm
 * @fileName ProcInstanceServiceImpl.java
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 */
@Service
public class ProcInstanceServiceImpl
        extends ServiceImpl<ProcInstanceMapper, ProcInstance>
        implements ProcInstanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcInstanceServiceImpl.class);

    @Resource
    ProcInstanceMapper procInstanceMapper;

    @Override
    public PageResult getInstanceList(Integer page, Integer pageSize) {
        PageResult pageResult = new PageResult<>();
        pageResult.setRows(procInstanceMapper.getInstanceList((page - 1) * pageSize, pageSize));
        return pageResult;
    }

    @Override
    public PageResult getInstanceListAnd(Integer page, Integer pageSize, List<String> approvalTypes) {
        PageResult pageResult = new PageResult<>();
        pageResult.setRows(procInstanceMapper.getInstanceListAnd((page - 1) * pageSize, pageSize, approvalTypes));
        return pageResult;
    }

    @Override
    public PageResult getInstanceListAndStateOfApprovals(Integer page, Integer pageSize, List<String> stateOfApprovals,List<String> approvalTypes) {
        PageResult pageResult = new PageResult<>();
        List<Integer> list = new ArrayList<>();
        for (String stateOfApproval : stateOfApprovals) {
            switch (stateOfApproval) {
                case "已提交":
                    list.add(0);
                    break;
                case "审批中":
                    list.add(1);
                    break;
                case "审批通过":
                    list.add(2);
                    break;
                case "审批驳回":
                    list.add(3);
                    break;
                case "已撤回":
                    list.add(4);
                    break;
            }
        }
        pageResult.setRows(procInstanceMapper.getInstanceListAndStateOfApprovals((page - 1) * pageSize, pageSize, list,approvalTypes));
        return pageResult;
    }
}

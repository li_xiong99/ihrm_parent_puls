package com.kun.ihrm.audit.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.audit.entity.ProcInstance;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * ProcInstanceMapper接口
 * @packageName com.kun.ihrm
 * @fileName ProcInstanceMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 * @version 1.0.0
 */
public interface ProcInstanceMapper extends BaseMapper<ProcInstance> {


    @Select("select * from  proc_instance  limit #{page},#{pageSize}")
    List<ProcInstance> getInstanceList(@Param("page") Integer page, @Param("pageSize") Integer pageSize);



    List<ProcInstance> getInstanceListAnd(@Param("page") int page, @Param("pageSize") Integer pageSize, @Param("list") List<String> list);

    List<ProcInstance> getInstanceListAndStateOfApprovals(@Param("page") int page,
                                                          @Param("pageSize") Integer pageSize,
                                                          @Param("list") List<Integer> list,
                                                          @Param("stateOfApprovals") List<String> stateOfApprovals);

}

package com.kun.ihrm.audit.conetroller;
import com.kun.ihrm.audit.entity.ProcInstance;
import com.kun.ihrm.audit.service.ProcInstanceService;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class ApplyController {

    @Autowired
    ProcInstanceService procInstanceService;


    @PostMapping("/apply/overtime")
    public Result addApply(@RequestParam("ProcInstance") ProcInstance procInstance) {
        procInstanceService.save(procInstance);
        return new Result(ResultCode.SUCCESS);
    }

    /**
     * 根据查询条件查询，对应的审批条件
     * http://localhost:8080/api/user/process/instance/1/10?
     * applicant=1063705989926227968&page=1&pageSize=10&
     * stateOfApprovals[]=%E5%AE%A1%E6%89%B9%E4%B8%AD
     *
     * @param page
     * @param pageSize
     * @param 
     * @param approvalTypes
     * @param stateOfApprovals
     * @return
     */
    @GetMapping("/process/instance/{page}/{pageSize}")
    public Result getInstanceListAnd(
            @PathVariable("page") Integer page,
            @PathVariable("pageSize") Integer pageSize,
//            @RequestParam(value = "ccPeople", required = false) Long ccPeople,
            @RequestParam(value = "approvalTypes[]", required = false) List<String> approvalTypes,
            @RequestParam(value = "stateOfApprovals[]", required = false) List<String> stateOfApprovals) {
        PageResult pages = null;
//        if (approvalTypes != null) {
//            pages = procInstanceService.getInstanceListAnd(page, pageSize, approvalTypes);
//        } else
        if (stateOfApprovals != null) {
            pages = procInstanceService.getInstanceListAndStateOfApprovals(page, pageSize, stateOfApprovals, approvalTypes);
        } else {
            pages = procInstanceService.getInstanceList(page, pageSize);
        }
        return new Result(ResultCode.SUCCESS, pages);
    }
}

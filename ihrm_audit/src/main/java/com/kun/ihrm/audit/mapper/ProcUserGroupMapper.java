package com.kun.ihrm.audit.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.audit.entity.ProcUserGroup;

/**
 * ProcUserGroupMapper接口
 * @packageName com.kun.ihrm
 * @fileName ProcUserGroupMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 * @version 1.0.0
 */
public interface ProcUserGroupMapper extends BaseMapper<ProcUserGroup> {

}

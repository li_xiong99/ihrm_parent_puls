package com.kun.ihrm.audit.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.audit.entity.ProcTaskInstance;


/**
 * ProcTaskInstanceService接口
 * @packageName com.kun.ihrm
 * @fileName ProcTaskInstanceService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 * @version 1.0.0
 */
public interface ProcTaskInstanceService extends IService<ProcTaskInstance>{


}

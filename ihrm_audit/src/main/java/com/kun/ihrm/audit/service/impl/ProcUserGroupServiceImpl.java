package com.kun.ihrm.audit.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.audit.entity.ProcUserGroup;
import com.kun.ihrm.audit.mapper.ProcUserGroupMapper;
import com.kun.ihrm.audit.service.ProcUserGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * ProcUserGroupService接口实现
 * @packageName com.kun.ihrm
 * @fileName ProcUserGroupServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 * @version 1.0.0
 */
@Service
public class ProcUserGroupServiceImpl 
				extends ServiceImpl<ProcUserGroupMapper, ProcUserGroup> 
				implements ProcUserGroupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcUserGroupServiceImpl.class);


}

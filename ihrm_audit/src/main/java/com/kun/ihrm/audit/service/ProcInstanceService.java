package com.kun.ihrm.audit.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.audit.entity.ProcInstance;
import com.kun.ihrm.common.entity.PageResult;

import java.util.List;


/**
 * ProcInstanceService接口
 * @packageName com.kun.ihrm
 * @fileName ProcInstanceService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月25日 上午10:02:08
 * @lastModify 2023年8月25日 上午10:02:08
 * @version 1.0.0
 */
public interface ProcInstanceService extends IService<ProcInstance>{


    PageResult getInstanceList(Integer page, Integer pageSize);

    PageResult getInstanceListAnd(Integer page, Integer pageSize, List<String> approvalTypes);

    PageResult getInstanceListAndStateOfApprovals(Integer page, Integer pageSize, List<String> stateOfApprovals,List<String> approvalTypes);
}

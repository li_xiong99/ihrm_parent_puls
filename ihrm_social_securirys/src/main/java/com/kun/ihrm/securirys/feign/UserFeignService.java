package com.kun.ihrm.securirys.feign;

import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.securirys.config.FeignOAuth2RequestInterceptor;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "ihrm-system", configuration = FeignOAuth2RequestInterceptor.class)
public interface UserFeignService {
    @RequestMapping(value = "/sys/user/{id}", method = RequestMethod.GET)
    public Result findByUserInfo(@PathVariable("id") String id);
}

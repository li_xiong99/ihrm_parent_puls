package com.kun.ihrm.securirys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.securirys.dto.PagingFindConditionDTO;
import com.kun.ihrm.securirys.entity.UserSocialSecurity;
import com.kun.ihrm.securirys.vo.UserSocialSecurityVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 社保-用户社保信息表 Mapper接口
 * @packageName com.kun.ihrm.social
 * @fileName UserSocialSecurityMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
@Mapper
public interface UserSocialSecurityMapper extends BaseMapper<UserSocialSecurity> {

    List<UserSocialSecurityVO> getListPage(PagingFindConditionDTO dto);
}

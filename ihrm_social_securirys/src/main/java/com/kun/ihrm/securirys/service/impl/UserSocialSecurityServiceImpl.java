package com.kun.ihrm.securirys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.securirys.dto.PagingFindConditionDTO;
import com.kun.ihrm.securirys.entity.UserSocialSecurity;
import com.kun.ihrm.securirys.mapper.UserSocialSecurityMapper;
import com.kun.ihrm.securirys.service.UserSocialSecurityService;
import com.kun.ihrm.securirys.vo.UserSocialSecurityVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 社保-用户社保信息表 Service接口实现
 * @packageName com.kun.ihrm.social
 * @fileName UserSocialSecurityServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
@Service
public class UserSocialSecurityServiceImpl 
				extends ServiceImpl<UserSocialSecurityMapper, UserSocialSecurity>
				implements UserSocialSecurityService {


    @Resource
    private UserSocialSecurityMapper userSocialSecurityMapper;

    @Override
    public PageResult<Object> getUserSocialSecurityPage(PagingFindConditionDTO dto) {
        List<UserSocialSecurityVO> listPage = userSocialSecurityMapper.getListPage(dto);
        if (listPage!=null){
            return new PageResult((long)listPage.size(),listPage);
        }
        return new PageResult(0L,null);
    }

    @Override
    public UserSocialSecurity getUserSocialSecurity(String user_id) {
        if (!StringUtils.isEmpty(user_id)){
            return this.getById(user_id);
        }
        return null;
    }

    @Override
    public boolean updateUserSocialSecurity(String userId, UserSocialSecurity user) {
        return this.update(user,new QueryWrapper<UserSocialSecurity>().eq("user_id", userId));
    }
}

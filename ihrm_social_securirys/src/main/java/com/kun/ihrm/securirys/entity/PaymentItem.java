package com.kun.ihrm.securirys.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 社保-缴费项目实体类
 * @packageName com.kun.ihrm.social
 * @fileName PaymentItem.java
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
@Data
@TableName("ss_payment_item")
public class PaymentItem {

  /** 
   * id
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 缴费项目名称
   */
  @TableField("name")
  private String name;
  /** 
   * 企业是否缴纳开关，0为禁用，1为启用
   */
  @TableField("switch_company")
  private Integer switchCompany;
  /** 
   * 企业比例
   */
  @TableField("scale_company")
  private Double scaleCompany;
  /** 
   * 个人是否缴纳开关，0为禁用，1为启用
   */
  @TableField("switch_personal")
  private Integer switchPersonal;
  /** 
   * 个人比例
   */
  @TableField("scale_personal")
  private Double scalePersonal;

}

package com.kun.ihrm.securirys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 社保-企业设置信息实体类
 *
 * @version 1.0.0
 * @packageName com.kun.ihrm.social
 * @fileName CompanySettings.java
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 */
@Data
@TableName("ss_company_settings")
public class CompanySettings {

    /**
     * 企业id
     */
    @TableId(value = "company_id", type = IdType.INPUT)
    private String companyId;
    /**
     * 0是未设置，1是已设置
     */
    @TableField("is_settings")
    private Integer isSettings;
    /**
     * 当前显示报表月份
     */
    @TableField("data_month")
    private String dataMonth;

}

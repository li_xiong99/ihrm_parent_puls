package com.kun.ihrm.securirys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.securirys.entity.CompanySettings;


/**
 * 社保-企业设置信息 Service接口
 * @packageName com.kun.ihrm.social
 * @fileName CompanySettingsService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
public interface CompanySettingsService extends IService<CompanySettings> {
    /**
     * 根据 企业ID 重新 企业是否购买社保
     * @param companyId 企业ID
     * @return
     */
    CompanySettings getCompanySettings(String companyId);

    /**
     * 设置 企业 当月的 社保
     * @param companySettings
     * @return
     */
    boolean saveSettings(CompanySettings companySettings);
}

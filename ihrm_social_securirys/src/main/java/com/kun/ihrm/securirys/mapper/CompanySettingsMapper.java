package com.kun.ihrm.securirys.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.securirys.entity.CompanySettings;

/**
 * 社保-企业设置信息 Mapper接口
 * @packageName com.kun.ihrm.social
 * @fileName CompanySettingsMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
public interface CompanySettingsMapper extends BaseMapper<CompanySettings> {

}

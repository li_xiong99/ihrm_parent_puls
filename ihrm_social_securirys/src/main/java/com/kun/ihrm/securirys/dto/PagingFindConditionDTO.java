package com.kun.ihrm.securirys.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PagingFindConditionDTO {
    /**
     * 当前页数
     */
    private int page;
    /**
     * 每页数据量
     */
    private int pageSize;
    /**
     * 公积金城市
     */
    private String[] providentFundChecks;
    /**
     * 部门
     */
    private String[] departmentChecks;
    /**
     * 社保城市
     */
    private String[] socialSecurityChecks;
    /**
     * 查询框
     */
    private String keyword;

    public void setPage(int page) {
        this.page = page-1;
    }
}

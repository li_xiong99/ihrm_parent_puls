package com.kun.ihrm.securirys.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kun.ihrm.securirys.entity.CityPaymentItem;

/**
 * 社保-城市与缴费项目关联表 Mapper接口
 * @packageName com.kun.ihrm.social
 * @fileName CityPaymentItemMapper.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
public interface CityPaymentItemMapper extends BaseMapper<CityPaymentItem> {
}

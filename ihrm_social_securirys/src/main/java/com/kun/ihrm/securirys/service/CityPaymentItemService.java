package com.kun.ihrm.securirys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.securirys.entity.CityPaymentItem;

import java.util.List;


/**
 * 社保-城市与缴费项目关联表 Service接口
 * @packageName com.kun.ihrm.social
 * @fileName CityPaymentItemService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
public interface CityPaymentItemService extends IService<CityPaymentItem> {
    /**
     * 根据 城市 id 查询 该城市的社保缴纳 项目（医疗、生育、等等、……）
     * 因为每个城市 社保 缴纳项目 有可能 有所不同
     * @param city_id id
     * @return
     */
    List<CityPaymentItem> getCityPaymentItemList(String city_id);


}

package com.kun.ihrm.securirys.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 社保-城市与缴费项目关联表实体类
 * @packageName com.kun.ihrm.social
 * @fileName CityPaymentItem.java
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
@Data
@TableName("ss_city_payment_item")
public class CityPaymentItem {


  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 城市id
   */
  @TableField("city_id")
  private String cityId;
  /** 
   * 缴费项目id
   */
  @TableField("payment_item_id")
  private String paymentItemId;
  /** 
   * 企业是否缴纳开关，0为禁用，1为启用
   */
  @TableField("switch_company")
  private boolean switchCompany;
  /** 
   * 企业比例
   */
  @TableField("scale_company")
  private Double scaleCompany;
  /** 
   * 个人是否缴纳开关，0为禁用，1为启用
   */
  @TableField("switch_personal")
  private boolean switchPersonal;

  /** 
   * 个人比例
   */
  @TableField("scale_personal")
  private Double scalePersonal;
  /** 
   * 缴费项目名称
   */
  @TableField("name")
  private String name;

}

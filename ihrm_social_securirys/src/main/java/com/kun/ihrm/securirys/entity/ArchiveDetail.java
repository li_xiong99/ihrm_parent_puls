package com.kun.ihrm.securirys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 社保-归档详情实体类
 *
 * @version 1.0.0
 * @packageName com.kun.ihrm.social
 * @fileName ArchiveDetail.java
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 */
@Data
@TableName("ss_archive_detail")
public class ArchiveDetail {

    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;
    /**
     * 归档id
     */
    @TableField("archive_id")
    private String archiveId;
    /**
     * 用户id
     */
    @TableField("user_id")
    private String userId;
    /**
     * 用户名称
     */
    @TableField("username")
    private String username;
    /**
     * 入职时间
     */
    @TableField("time_of_entry")
    private String timeOfEntry;
    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;
    /**
     * 身份证号
     */
    @TableField("id_number")
    private String idNumber;
    /**
     * 学历
     */
    @TableField("the_highest_degree_of_education")
    private String theHighestDegreeOfEducation;
    /**
     * 开户行
     */
    @TableField("opening_bank")
    private String openingBank;
    /**
     * 银行卡号
     */
    @TableField("bank_card_number")
    private String bankCardNumber;
    /**
     * 一级部门
     */
    @TableField("first_level_department")
    private String firstLevelDepartment;
    /**
     * 二级部门
     */
    @TableField("two_level_department")
    private String twoLevelDepartment;
    /**
     * 工作城市
     */
    @TableField("working_city")
    private String workingCity;
    /**
     * 社保电脑号
     */
    @TableField("social_security_computer_number")
    private String socialSecurityComputerNumber;
    /**
     * 公积金账号
     */
    @TableField("provident_fund_account")
    private String providentFundAccount;
    /**
     * 离职时间
     */
    @TableField("leave_date")
    private String leaveDate;
    /**
     * 户籍类型
     */
    @TableField("household_registration_type")
    private String householdRegistrationType;
    /**
     * 参保城市
     */
    @TableField("participating_in_the_city")
    private String participatingInTheCity;
    /**
     * 社保月份
     */
    @TableField("social_security_month")
    private String socialSecurityMonth;
    /**
     * 社保基数
     */
    @TableField("social_security_base")
    private Double socialSecurityBase;
    /**
     * 社保合计
     */
    @TableField("social_security")
    private Double socialSecurity;
    /**
     * 社保企业
     */
    @TableField("social_security_enterprise")
    private BigDecimal socialSecurityEnterprise;
    /**
     * 社保个人
     */
    @TableField("social_security_individual")
    private BigDecimal socialSecurityIndividual;
    /**
     * 公积金城市
     */
    @TableField("provident_fund_city")
    private String providentFundCity;
    /**
     * 公积金月份
     */
    @TableField("provident_fund_month")
    private String providentFundMonth;
    /**
     * 公积金基数
     */
    @TableField("provident_fund_base")
    private Double providentFundBase;
    /**
     * 公积金企业基数
     */
    @TableField("accumulation_fund_enterprise_base")
    private Double accumulationFundEnterpriseBase;
    /**
     * 公积金企业比例
     */
    @TableField("proportion_of_provident_fund_enterprises")
    private Double proportionOfProvidentFundEnterprises;
    /**
     * 公积金个人基数
     */
    @TableField("individual_base_of_provident_fund")
    private Double individualBaseOfProvidentFund;
    /**
     * 公积金个人比例
     */
    @TableField("personal_ratio_of_provident_fund")
    private Double personalRatioOfProvidentFund;
    /**
     * 公积金合计
     */
    @TableField("total_provident_fund")
    private Double totalProvidentFund;
    /**
     * 公积金企业
     */
    @TableField("provident_fund_enterprises")
    private Double providentFundEnterprises;
    /**
     * 公积金个人
     */
    @TableField("provident_fund_individual")
    private Double providentFundIndividual;
    /**
     * 养老企业基数
     */
    @TableField("pension_enterprise_base")
    private Double pensionEnterpriseBase;
    /**
     * 养老企业比例
     */
    @TableField("proportion_of_pension_enterprises")
    private Double proportionOfPensionEnterprises;
    /**
     * 养老企业
     */
    @TableField("pension_enterprise")
    private Double pensionEnterprise;
    /**
     * 养老个人基数
     */
    @TableField("personal_pension_base")
    private Double personalPensionBase;
    /**
     * 养老个人比例
     */
    @TableField("personal_pension_ratio")
    private Double personalPensionRatio;
    /**
     * 养老个人
     */
    @TableField("old_age_individual")
    private Double oldAgeIndividual;
    /**
     * 失业企业基数
     */
    @TableField("unemployment_enterprise_base")
    private Double unemploymentEnterpriseBase;
    /**
     * 失业企业比例
     */
    @TableField("proportion_of_unemployed_enterprises")
    private Double proportionOfUnemployedEnterprises;
    /**
     * 失业企业
     */
    @TableField("unemployed_enterprise")
    private Double unemployedEnterprise;
    /**
     * 失业个人基数
     */
    @TableField("the_number_of_unemployed_individuals")
    private Double theNumberOfUnemployedIndividuals;
    /**
     * 失业个人比例
     */
    @TableField("percentage_of_unemployed_individuals")
    private Double percentageOfUnemployedIndividuals;
    /**
     * 失业个人
     */
    @TableField("unemployed_individual")
    private Double unemployedIndividual;
    /**
     * 医疗企业基数
     */
    @TableField("medical_enterprise_base")
    private Double medicalEnterpriseBase;
    /**
     * 医疗企业比例
     */
    @TableField("proportion_of_medical_enterprises")
    private Double proportionOfMedicalEnterprises;
    /**
     * 医疗企业
     */
    @TableField("medical_enterprise")
    private Double medicalEnterprise;
    /**
     * 医疗个人基数
     */
    @TableField("medical_personal_base")
    private Double medicalPersonalBase;
    /**
     * 医疗个人比例
     */
    @TableField("medical_personal_ratio")
    private Double medicalPersonalRatio;
    /**
     * 医疗个人
     */
    @TableField("medical_individual")
    private Double medicalIndividual;
    /**
     * 工伤企业基数
     */
    @TableField("base_of_industrial_injury_enterprises")
    private Double baseOfIndustrialInjuryEnterprises;
    /**
     * 工伤企业比例
     */
    @TableField("proportion_of_industrial_injury_enterprises")
    private Double proportionOfIndustrialInjuryEnterprises;
    /**
     * 工伤企业
     */
    @TableField("industrial_injury_enterprise")
    private Double industrialInjuryEnterprise;
    /**
     * 生育企业基数
     */
    @TableField("fertility_enterprise_base")
    private Double fertilityEnterpriseBase;
    /**
     * 生育企业比例
     */
    @TableField("proportion_of_fertility_enterprises")
    private Double proportionOfFertilityEnterprises;
    /**
     * 生育企业
     */
    @TableField("childbearing_enterprise")
    private Double childbearingEnterprise;
    /**
     * 大病企业基数
     */
    @TableField("base_of_serious_illness")
    private Double baseOfSeriousIllness;
    /**
     * 大病企业比例
     */
    @TableField("proportion_of_seriously_ill_enterprises")
    private Double proportionOfSeriouslyIllEnterprises;
    /**
     * 大病企业
     */
    @TableField("big_disease_enterprise")
    private Double bigDiseaseEnterprise;
    /**
     * 大病个人基数
     */
    @TableField("personal_base_of_serious_illness")
    private Double personalBaseOfSeriousIllness;
    /**
     * 大病个人比例
     */
    @TableField("personal_proportion_of_serious_illness")
    private Double personalProportionOfSeriousIllness;
    /**
     * 大病个人
     */
    @TableField("a_person_of_great_disease")
    private Double aPersonOfGreatDisease;
    /**
     * 公积金备注
     */
    @TableField("provident_fund_notes")
    private String providentFundNotes;
    /**
     * 社保备注
     */
    @TableField("social_security_notes")
    private String socialSecurityNotes;

    @TableField("years_month")
    private String yearsMonth;

}

package com.kun.ihrm.securirys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.securirys.entity.Archive;

import java.util.List;


/**
 * 社保-归档表 Service接口
 * @packageName com.kun.ihrm.social
 * @fileName ArchiveService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
public interface ArchiveService extends IService<Archive> {
    /**
     * 根据 公司Id 和 年份 查询 历史归档 集合
     * @param companyId 公司Id
     * @param year 年份
     * @return 历史社保归档对象 集合
     */
    List<Archive> getArchiveList(String companyId,String year);

    /**
     * 根据 公司Id 和 年月份 查询 历史归档
     * @param companyId 公司Id
     * @param yearMonth 年月份
     * @return 历史归档 对象
     */
    Archive getArchive(String companyId,String yearMonth);

    /**
     * 用于 保存 或修改 历史归档
     * @param archive  历史归档对象
     * @param state 用于表示是新增还是修改 true : 新增 false : 修改
     * @return
     */
    Boolean saveArchive(Archive archive,boolean state);

    /**
     *  更新 或  新增 社保归档
     * @param companyId 公司 ID
     * @param yearMonth
     * @return
     */
    boolean newReport(String companyId, String yearMonth);
}

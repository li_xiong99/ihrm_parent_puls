package com.kun.ihrm.securirys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.securirys.dto.PagingFindConditionDTO;
import com.kun.ihrm.securirys.entity.UserSocialSecurity;


/**
 * 社保-用户社保信息表 Service接口
 * @packageName com.kun.ihrm.social
 * @fileName UserSocialSecurityService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
public interface UserSocialSecurityService extends IService<UserSocialSecurity> {
    /**
     * 根据 条件 分页 查询 用户信息列表
     * @param dto
     * @return
     */
    PageResult<Object> getUserSocialSecurityPage(PagingFindConditionDTO dto);

    /**
     * 根据 user_id 查询 用户社保信息表
     * @param user_id 用户 Id
     * @return 社保信息 对象
     */
    UserSocialSecurity getUserSocialSecurity(String user_id);

    /**
     * 根据 用户Id  修改 用户社保信息
     * @param userId  用户Id
     * @param user 用户社保对象
     * @return
     */
    boolean updateUserSocialSecurity(String userId,UserSocialSecurity user);

}

package com.kun.ihrm.securirys.vo;


import lombok.Data;

@Data
public class UserSocialSecurityVO {
    /**
     * 姓名
     */
    private String  departmentName;
    /**
     *
     */
    private String  id;
    /**
     *
     */
    private String  leaveTime;
    /**
     *
     */
    private String  providentFundCity;
    /**
     *
     */
    private String  timeOfEntry;
    /**
     *
     */
    private String  username;
    /**
     *
     */
    private String  mobile;
    /**
     *
     */
    private String  workNumber;
    /**
     *
     */
    private String  socialSecurityBase;
    /**
     *
     */
    private String  participatingInTheCityId;
    /**
     *
     */
    private String  providentFundCityId;
    /**
     *
     */
    private String  participatingInTheCity;
    /**
     *
     */
    private String  providentFundBase;
}

package com.kun.ihrm.securirys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 社保-用户社保信息表实体类
 *
 * @version 1.0.0
 * @packageName com.kun.ihrm.social
 * @fileName UserSocialSecurity.java
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 */
@Data
@TableName("ss_user_social_security")
public class UserSocialSecurity {

    /**
     * 用户id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private String userId;
    /**
     * 本月是否缴纳社保 0为不缴纳 1为缴纳
     */
    @TableField("enterprises_pay_social_security_this_month")
    private Integer enterprisesPaySocialSecurityThisMonth;
    /**
     * 本月是否缴纳公积金 0为不缴纳 1为缴纳
     */
    @TableField("enterprises_pay_the_provident_fund_this_month")
    private Integer enterprisesPayTheProvidentFundThisMonth;
    /**
     * 参保城市id
     */
    @TableField("participating_in_the_city_id")
    private String participatingInTheCityId;
    /**
     * 参保类型  1为首次开户 2为非首次开户
     */
    @TableField("social_security_type")
    private Integer socialSecurityType;
    /**
     * 户籍类型 1为本市城镇 2为本市农村 3为外埠城镇 4为外埠农村
     */
    @TableField("household_registration_type")
    private Integer householdRegistrationType;
    /**
     * 社保基数
     */
    @TableField("social_security_base")
    private Integer socialSecurityBase;
    /**
     * 工伤比例
     */
    @TableField("industrial_injury_ratio")
    private Double industrialInjuryRatio;
    /**
     * 社保备注
     */
    @TableField("social_security_notes")
    private String socialSecurityNotes;
    /**
     * 公积金城市id
     */
    @TableField("provident_fund_city_id")
    private String providentFundCityId;
    /**
     * 公积金基数
     */
    @TableField("provident_fund_base")
    private Integer providentFundBase;
    /**
     * 公积金企业比例
     */
    @TableField("enterprise_proportion")
    private Double enterpriseProportion;
    /**
     * 公积金个人比例
     */
    @TableField("personal_proportion")
    private Double personalProportion;
    /**
     * 公积金企业缴纳数额
     */
    @TableField("enterprise_provident_fund_payment")
    private Double enterpriseProvidentFundPayment;
    /**
     * 公积金个人缴纳数额
     */
    @TableField("personal_provident_fund_payment")
    private Double personalProvidentFundPayment;
    /**
     * 公积金备注
     */
    @TableField("provident_fund_notes")
    private String providentFundNotes;
    /**
     * 最后修改时间
     */
    @TableField("last_modify_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifyTime;
    /**
     * 社保是否缴纳变更时间
     */
//  @TableField("social_security_switch_update_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date socialSecuritySwitchUpdateTime;
    /**
     * 公积金是否缴纳变更时间
     */
//  @TableField("provident_fund_switch_update_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date providentFundSwitchUpdateTime;

    @TableField("city_name")
    private String cityName;

    @TableField("household_registration")
    private String householdRegistration;

    @TableField("participating_in_the_city")
    private String participatingInTheCity;

    @TableField("provident_fund_city")
    private String providentFundCity;

}

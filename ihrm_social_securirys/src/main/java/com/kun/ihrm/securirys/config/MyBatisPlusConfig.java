package com.kun.ihrm.securirys.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * MyBatis-Plus配置类
 * @packageName com.kun.ihrm.social.config;
 * @fileName MyBatisPlusConfig.java
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
@EnableTransactionManagement //开启事务
@Configuration //声明 为 配置类
@MapperScan("com.kun.ihrm.securirys.mapper") //
public class MyBatisPlusConfig {
   @Bean
   public PaginationInterceptor paginationInterceptor(){
       return new PaginationInterceptor();
   }
}

package com.kun.ihrm.securirys.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.securirys.entity.CompanySettings;
import com.kun.ihrm.securirys.mapper.CompanySettingsMapper;
import com.kun.ihrm.securirys.service.CompanySettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 社保-企业设置信息 Service接口实现
 * @packageName com.kun.ihrm.social
 * @fileName CompanySettingsServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
@Service
public class CompanySettingsServiceImpl 
				extends ServiceImpl<CompanySettingsMapper, CompanySettings>
				implements CompanySettingsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanySettingsServiceImpl.class);


    @Override
    public CompanySettings getCompanySettings(String companyId) {
        return this.getById(companyId);
    }

    @Override
    public boolean saveSettings(CompanySettings companySettings) {
        return this.save(companySettings);
    }
}

package com.kun.ihrm.securirys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 社保-归档表实体类
 * @packageName com.kun.ihrm.social
 * @fileName Archive.java
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
@Data
@TableName("ss_archive")
public class Archive {

  /** 
   * id
   */
  @TableId(value = "id", type = IdType.INPUT)
  private String id;
  /** 
   * 企业id
   */
  @TableField("company_id")
  private String companyId;
  /** 
   * 年月
   */
  @TableField("years_month")
  private String yearsMonth;
  /** 
   * 创建时间
   */
  @TableField("creation_time")
  private Date creationTime;
  /** 
   * 企业缴纳
   */
  @TableField("enterprise_payment")
  private BigDecimal enterprisePayment;
  /** 
   * 个人缴纳
   */
  @TableField("personal_payment")
  private BigDecimal personalPayment;
  /** 
   * 合计
   */
  @TableField("total")
  private BigDecimal total;

}

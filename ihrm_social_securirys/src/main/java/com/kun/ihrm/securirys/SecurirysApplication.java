package com.kun.ihrm.securirys;

import com.kun.ihrm.common.utils.IdWorker;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@MapperScan("com.kun.ihrm.securirys.mapper")
//@EnableFeignClients(basePackages = "com.kun.ihrm.securirys.feign")
@EnableFeignClients
public class SecurirysApplication {
    public static void main(String[] args) {
        SpringApplication.run(SecurirysApplication.class, args);
    }

    @Bean
    public IdWorker idWorker() {
        return new IdWorker();
    }
}

package com.kun.ihrm.securirys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.securirys.entity.ArchiveDetail;

import java.util.List;

/**
 * 社保-归档详情 Service接口
 * @packageName com.kun.ihrm.social
 * @fileName ArchiveDetailService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
public interface ArchiveDetailService extends IService<ArchiveDetail> {

    /**
     * 根据 公司Id 和 年月份 查询 当月社保信息 集合
     * @param companyId 公司Id
     * @param yearMonth 年月份
     * @return 当月 社保详情信息 集合
     */
    List<ArchiveDetail> getUserArchiveDetailList(String companyId, String yearMonth);

    /**
     * 根据 社保Id 社保详情信息
     * @param id  社保Id
     * @return 当月 社保详情信息
     */
    List<ArchiveDetail> getUserArchiveDetailList(String id);

    /**
     * 新增
     * @param archiveDetail
     * @return
     */
    boolean addArchiveDetail(ArchiveDetail archiveDetail);

}

package com.kun.ihrm.securirys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.securirys.entity.Archive;
import com.kun.ihrm.securirys.entity.ArchiveDetail;
import com.kun.ihrm.securirys.mapper.ArchiveDetailMapper;
import com.kun.ihrm.securirys.service.ArchiveDetailService;
import com.kun.ihrm.securirys.service.ArchiveService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 社保-归档详情 Service接口实现
 * @packageName com.kun.ihrm.social
 * @fileName ArchiveDetailServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
@Service
public class ArchiveDetailServiceImpl
				extends ServiceImpl<ArchiveDetailMapper, ArchiveDetail>
				implements ArchiveDetailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchiveDetailServiceImpl.class);

    @Autowired
    private ArchiveService archiveService;




    @Override
    public List<ArchiveDetail> getUserArchiveDetailList(String companyId, String yearMonth) {
        List<ArchiveDetail> archive_id = null;
        //先根据 公司Id 和 社保日期 查询到当月 社保对象
        Archive one = archiveService.getOne(new QueryWrapper<Archive>().eq("company_id", companyId).eq("years_month", yearMonth));
        if (one!=null){
            //根据社保对象的 id 查询社保详情信息
           archive_id = this.list(new QueryWrapper<ArchiveDetail>().eq("archive_id", one.getId()));
        }

        return archive_id;
    }

    @Override
    public List<ArchiveDetail>  getUserArchiveDetailList(String id) {
       return this.list(new QueryWrapper<ArchiveDetail>().eq("archive_id", id));
    }

    @Override
    public boolean addArchiveDetail(ArchiveDetail archiveDetail) {
        return this.save(archiveDetail);
    }

}

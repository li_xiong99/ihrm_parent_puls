package com.kun.ihrm.securirys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.securirys.entity.PaymentItem;
import com.kun.ihrm.securirys.mapper.PaymentItemMapper;
import com.kun.ihrm.securirys.service.PaymentItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 社保-缴费项目 Service接口实现
 * @packageName com.kun.ihrm.social
 * @fileName PaymentItemServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
@Service
public class PaymentItemServiceImpl 
				extends ServiceImpl<PaymentItemMapper, PaymentItem>
				implements PaymentItemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentItemServiceImpl.class);


}

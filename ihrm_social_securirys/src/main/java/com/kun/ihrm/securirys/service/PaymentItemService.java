package com.kun.ihrm.securirys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kun.ihrm.securirys.entity.PaymentItem;


/**
 * 社保-缴费项目 Service接口
 * @packageName com.kun.ihrm.social
 * @fileName PaymentItemService.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
public interface PaymentItemService extends IService<PaymentItem> {


}

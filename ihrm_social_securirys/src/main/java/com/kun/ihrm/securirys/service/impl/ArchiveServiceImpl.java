package com.kun.ihrm.securirys.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.common.utils.IdWorker;
import com.kun.ihrm.securirys.entity.Archive;
import com.kun.ihrm.securirys.entity.ArchiveDetail;
import com.kun.ihrm.securirys.mapper.ArchiveMapper;
import com.kun.ihrm.securirys.service.ArchiveDetailService;
import com.kun.ihrm.securirys.service.ArchiveService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 社保-归档表 Service接口实现
 *
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @version 1.0.0
 * @packageName com.kun.ihrm.social
 * @fileName ArchiveServiceImpl.java
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 */
@Service
public class ArchiveServiceImpl
        extends ServiceImpl<ArchiveMapper, Archive>
        implements ArchiveService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchiveServiceImpl.class);

    @Resource
    private ArchiveDetailService archiveDetailService;

    @Resource
    private IdWorker idWorker;


    @Transactional//开启事务
    @Override
    public boolean newReport(String companyId, String yearMonth) {
        List<ArchiveDetail> list = archiveDetailService.getUserArchiveDetailList(companyId, yearMonth);
        BigDecimal firmSum=new BigDecimal("0");
        BigDecimal personageSum = new BigDecimal("0");
        if (list != null) {
            for (ArchiveDetail archiveDetail : list) {
                //社保企业
                BigDecimal securityIndividual = archiveDetail.getSocialSecurityIndividual();
                if (securityIndividual!=null){
                    firmSum = firmSum.add(securityIndividual);
                }
                //社保个人
                BigDecimal enterprise = archiveDetail.getSocialSecurityEnterprise();
                if (enterprise!=null){
                    personageSum = personageSum.add(enterprise);
                }
            }
        }
        //查询 出历史归档
        Archive archive = this.getArchive(companyId, yearMonth);
        //如果 不存在 就新建 并保存
        boolean state = false;
        if (archive == null){
            archive = new Archive();
            archive.setId(idWorker.nextId() + "");
            archive.setCompanyId(companyId);
            archive.setYearsMonth(yearMonth);
            archive.setCreationTime(new Date());
            state = true;
        }
        //存在就 从新归档
        archive.setEnterprisePayment(firmSum);
        archive.setPersonalPayment(personageSum);
        archive.setTotal(
                firmSum.add(personageSum)
        );
        Boolean aBoolean = this.saveArchive(archive,state);
        //todo 循环保存到归档信息详情1
        boolean save = false;
        if (list!=null){
            for (ArchiveDetail archiveDetail : list) {
                archiveDetail.setId(idWorker.nextId() + "");
                try {
                    save = archiveDetailService.save(archiveDetail);
                    if (!save){
                        break;
                    }
                } catch (Exception e) {
                    log.error("归档信息详情保存失败!!!!");
                    e.printStackTrace();
                    break;
                }
            }
        }
        return aBoolean&&save ? true:false;
    }

    @Override
    public Boolean saveArchive(Archive archive, boolean state) {
        if (state){
            return this.save(archive);
        }else {
            return this.update(archive,
                    new QueryWrapper<Archive>().eq("id", archive.getId())
            );
        }
    }

    @Override
    public Archive getArchive(String companyId, String yearMonth) {
        return this.getOne(new QueryWrapper<Archive>().eq("company_id", companyId).eq("years_month", yearMonth));
    }

    @Override
    public List<Archive> getArchiveList(String companyId, String year) {
        return this.list(new QueryWrapper<Archive>().eq("company_id", companyId).likeRight("years_month", year));
    }


}

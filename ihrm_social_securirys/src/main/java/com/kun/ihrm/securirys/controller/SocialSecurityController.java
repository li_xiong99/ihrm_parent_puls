package com.kun.ihrm.securirys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kun.ihrm.common.controller.BaseController;
import com.kun.ihrm.common.entity.PageResult;
import com.kun.ihrm.common.entity.Result;
import com.kun.ihrm.common.entity.ResultCode;
import com.kun.ihrm.securirys.dto.PagingFindConditionDTO;
import com.kun.ihrm.securirys.entity.*;
import com.kun.ihrm.securirys.feign.UserFeignService;
import com.kun.ihrm.securirys.service.ArchiveService;
import com.kun.ihrm.securirys.service.CityPaymentItemService;
import com.kun.ihrm.securirys.service.CompanySettingsService;
import com.kun.ihrm.securirys.service.UserSocialSecurityService;
import com.kun.ihrm.securirys.service.impl.ArchiveDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("social_securitys")
public class SocialSecurityController extends BaseController {

    @Autowired
    private CompanySettingsService cs;//  社保-企业设置信息 Service 对象
    @Autowired
    private UserSocialSecurityService uss;//  社保-用户社保信息表 Service 对象

    @Autowired
    private CityPaymentItemService cp;//社保-城市与缴费项目关联表 Service 对象

    @Autowired
    private UserSocialSecurityService us;//社保-用户社保信息 对象

    @Autowired
    private ArchiveService as;// 社保-归档 信息 对象

    @Autowired
    private ArchiveDetailServiceImpl ad;// 社保-归档 详情信息 对象

    @Resource
    private UserFeignService userFeignService;

    /**
     * 查询企业 是否设置过社保
     * @return
     */
    @GetMapping("/settings")
    public Result settings() {
        CompanySettings companySettings = cs.getCompanySettings(companyId);
        return new Result(ResultCode.SUCCESS, companySettings);
    }

    /**
     * 保存企业设置
     */
    @RequestMapping(value = "/settings", method = RequestMethod.POST)
    public Result saveSettings(@RequestBody CompanySettings companySettings) {
        companySettings.setCompanyId(companyId);
        companySettings.setIsSettings(1);
        if (cs.saveSettings(companySettings)) {
            return new Result(ResultCode.SUCCESS);
        }
        return new Result(ResultCode.FAIL);
    }

    /**
     * 根据 条件 分页 查询 用户信息列表
     *
     * @param dto 中的提交 筛选条件 有
     *            providentFundChecks 公积金城市
     *            departmentChecks 部门
     *            socialSecurityChecks 社保城市
     *            keyword 搜索框
     * @return
     */
    @PostMapping("/list")
    public Result getList(@RequestBody PagingFindConditionDTO dto) {
        PageResult<Object> page = uss.getUserSocialSecurityPage(dto);
        return new Result(ResultCode.SUCCESS, page);
    }

    /**
     * 根据 城市 id 查询 该城市的社保缴纳 项目（医疗、生育、等等、……）
     * 因为每个城市 社保 缴纳项目 有可能 有所不同
     *
     * @param city_id 城市id
     * @return
     */
    @GetMapping("/payment_item/{id}")
    public Result getCityPaymentItem(@PathVariable(name = "id") String city_id) {
        List<CityPaymentItem> list = cp.getCityPaymentItemList(city_id);
        return new Result(ResultCode.SUCCESS, list);
    }

    /**
     * 根据  用户 id 保存或更新用户社保
     *
     * @param city_id 用户 id
     * @param user    用户社保实体
     * @return
     */
    @PutMapping("/{id}")
    public Result updateCityPaymentItem(@PathVariable(name = "id") String city_id,
                                        @RequestBody UserSocialSecurity user) {
        if (us.updateUserSocialSecurity(city_id, user)) {
            return new Result(ResultCode.SUCCESS);
        }
        return new Result(ResultCode.FAIL);
    }

    /**
     * 根据 用户Id 查询 用户的社保信息 和 用户的详情信息
     *
     * @param user_id 用户 Id
     * @return
     */
    @GetMapping("/{id}")
    public Result getUserSocialSecurity(@PathVariable(name = "id") String user_id) {
        UserSocialSecurity user = us.getUserSocialSecurity(user_id);
        Map<String, Object> map = new HashMap<>();
        map.put("userSocialSecurity", user);
        // 远程调用 根据用户 Id 查询 用户 详情信息
        Result userResult = userFeignService.findByUserInfo(user.getUserId());
        map.put("user",userResult.getData());
        return new Result(ResultCode.SUCCESS, map);
    }


    /**
     * 根据 登入的 公司id 和 年份 查询  历史归档列表
     * @param year
     * @return
     */
    @GetMapping("/historys/{year}/list")
    public Result getArchive(@PathVariable String year) {
        List<Archive> archiveList = as.getArchiveList("1", year);
        return new Result(ResultCode.SUCCESS, archiveList);
    }

    /**
     * 查询 月份 报表
     *
     * @param yearMonth
     * @param opType
     * @return
     */
    @GetMapping("/historys/{yearMonth}")
    public Result historysDetail(@PathVariable String yearMonth, int opType) {
        List<ArchiveDetail> list = null;
        //TODO 搞清楚 归档和未归档的 数据
        //如果 是 未归档
        if (opType == 1) {
            //TODO 未归档查询当月数据
            list = ad.getUserArchiveDetailList(companyId, yearMonth);
        } else {
            //根据 企业Id 和 年月份查询到 当月 的 社保信息对象
            Archive archive = as.getArchive(companyId, yearMonth);
            if (archive != null) {
                //根据 社保信息对象 的Id 查询到 社保详情信息
                list = ad.getUserArchiveDetailList(archive.getId());
            }
        }
        return new Result(ResultCode.SUCCESS, list);
    }

    @PostMapping("/historys/{yearMonth}/archive")
    public Result archive(@PathVariable String yearMonth) {
        if (as.newReport(companyId, yearMonth)) {
            return new Result(ResultCode.SUCCESS);
        }
        return new Result(ResultCode.FAIL);

    }

    @PutMapping("/historys/{yearMonth}/newReport")
    public Result newReport(@PathVariable String yearMonth) {
        CompanySettings companySettings = cs.getById(companyId);
        if (companySettings == null) {
            companySettings = new CompanySettings();
        }
        companySettings.setCompanyId(companyId);
        companySettings.setDataMonth(yearMonth);
        if (cs.update(companySettings, new QueryWrapper<CompanySettings>().eq("company_id", companyId))) {
            return new Result(ResultCode.SUCCESS);
        }
        return new Result(ResultCode.FAIL);

    }

}

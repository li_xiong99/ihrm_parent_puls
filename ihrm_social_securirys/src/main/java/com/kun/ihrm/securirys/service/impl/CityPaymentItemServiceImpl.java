package com.kun.ihrm.securirys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kun.ihrm.securirys.entity.CityPaymentItem;
import com.kun.ihrm.securirys.mapper.CityPaymentItemMapper;
import com.kun.ihrm.securirys.service.CityPaymentItemService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 社保-城市与缴费项目关联表 Service接口实现
 * @packageName com.kun.ihrm.social
 * @fileName CityPaymentItemServiceImpl.java
 * @author 科泰教育 (http://www.ktjiaoyu.com)
 * @createTime 2023年8月28日 下午3:18:09
 * @lastModify 2023年8月28日 下午3:18:09
 * @version 1.0.0
 */
@Service
public class CityPaymentItemServiceImpl 
				extends ServiceImpl<CityPaymentItemMapper, CityPaymentItem>
				implements CityPaymentItemService {

    @Override
    public List<CityPaymentItem> getCityPaymentItemList(String city_id) {
        if (!StringUtils.isEmpty(city_id)){
            return this.list(new QueryWrapper<CityPaymentItem>().eq("city_id", city_id));
        }
        return null;
    }


}
